package co.sodimac.vedigital.reportes;


import co.sodimac.vedigital.reportes.domain.Problem;

import javax.ws.rs.core.Response;

public class VEException extends Exception {
    private Response.Status status;

    public VEException(int statusCode, String msg, Throwable t) {
        super(msg, t);
        this.status = Response.Status.fromStatusCode(statusCode);
    }

    public Problem getProblem() {
        return Problem.builder().
                detail(this.getMessage()).
                title(this.status.getReasonPhrase()).
                type("https://sodimac.com.co/venta-empresa/error/request").build();
    }

    public void setStatus(Response.Status status) {
        this.status = status;
    }

    public Response.Status getStatus() {
        return status;
    }
}
