package co.sodimac.vedigital.reportes;

import io.opentracing.Tracer;
import io.vertx.core.http.HttpServerRequest;
import org.slf4j.MDC;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;

@Provider
public class LoggingFilter implements ContainerRequestFilter, ContainerResponseFilter {

    private static String EMPLOYEE_STORE = "employeeStore";
    private static String DEFAULT_STORE = "default";

    @Context
    HttpServerRequest request;

    @Inject
    Tracer configuredTracer;

    @Override
    public void filter(ContainerRequestContext context) {

        var headerEmployeeStore = request.getHeader(EMPLOYEE_STORE);
        if (headerEmployeeStore==null){
            headerEmployeeStore= DEFAULT_STORE;
        }
        MDC.remove(EMPLOYEE_STORE);
        MDC.put(EMPLOYEE_STORE, headerEmployeeStore);
        configuredTracer.activeSpan().setTag(EMPLOYEE_STORE,headerEmployeeStore);
    }

    @Override
    public void filter(ContainerRequestContext req, ContainerResponseContext resp) {

        MDC.remove(EMPLOYEE_STORE);
    }
}