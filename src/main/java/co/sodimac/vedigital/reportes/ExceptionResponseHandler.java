package co.sodimac.vedigital.reportes;

import co.sodimac.vedigital.reportes.domain.Problem;
import org.jboss.logging.Logger;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/*************
 ** Proveedores jax-rs para VCSOFT
 ************/
@Provider
public class ExceptionResponseHandler implements ExceptionMapper<Throwable> {
    private static final Logger LOG = Logger.getLogger(ExceptionResponseHandler.class);

    @Override
    @Produces(MediaType.APPLICATION_JSON)
    public Response toResponse(Throwable exception) {
        if (VEException.class.isInstance(exception)) {
            VEException ex = (VEException) exception;
            Problem prlm = ex.getProblem();
            LOG.warnf("Se reporta Problema en respuesta REST para excepcion VE %s ", prlm);
            return Response.status(ex.getStatus()).entity(prlm).build();
        }
        // Agregar I/O Exception

        Problem prlm = new Problem();
        prlm.setDetail(exception.getMessage());
        prlm.setTitle("Error en microservicio: ve-reportes");
        prlm.setType("https://sodimac.com.co/venta-empresa/error/unknown");
        prlm.setInstance("Instancia ve-reportes");

        LOG.errorf(exception,
                "Se reporta Problema en respuesta REST para excepcion no definida por VE %s ",
                prlm);
        LOG.error("Existe un error en el microservicio ve-reportes");
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(prlm)
                .build();
    }
}