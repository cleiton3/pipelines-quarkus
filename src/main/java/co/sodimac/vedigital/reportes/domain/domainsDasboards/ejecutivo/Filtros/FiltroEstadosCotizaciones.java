package co.sodimac.vedigital.reportes.domain.domainsDasboards.ejecutivo.Filtros;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FiltroEstadosCotizaciones {
    private String ejecutivo;
    private int anioSeleccionado;
    private int mesSeleccionado;
    private boolean fechaActual;
}
