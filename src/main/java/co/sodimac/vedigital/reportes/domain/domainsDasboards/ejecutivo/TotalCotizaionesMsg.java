package co.sodimac.vedigital.reportes.domain.domainsDasboards.ejecutivo;

import co.sodimac.vedigital.reportes.domain.domainsDasboards.MetaDashboardMsg;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.Document;

import java.util.ArrayList;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TotalCotizaionesMsg {
    private ArrayList<Document> data;
    private MetaDashboardMsg meta;
}
