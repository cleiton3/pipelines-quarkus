package co.sodimac.vedigital.reportes.domain.domains;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TotalRango {
    private Double rangoTotalMin;
    private Double rangoTotalMax;
}
