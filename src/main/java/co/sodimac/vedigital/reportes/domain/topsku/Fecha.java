package co.sodimac.vedigital.reportes.domain.topsku;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Fecha {
    private Date fechaInicial;
    private Date fechaFinal;
}
