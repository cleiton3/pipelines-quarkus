package co.sodimac.vedigital.reportes.domain.domains;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ContribucionRango {
    private Double contribucionMin;
    private Double contribucionMax;
}
