package co.sodimac.vedigital.reportes.domain.domainsDasboards;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MetaDashboardMsg {
    private long cantidadDocumentos;
}
