package co.sodimac.vedigital.reportes.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;
import java.util.Objects;


/**
 * Mensaje de Error que puede generar el servicio REST.
 Representa un error o problema en la ejecución del servicio
 Defición planteada por [Problem Details for HTTP APIs](https://tools.ietf.org/html/rfc7807)

 **/
public class Problem {
  private  @Valid  String type;
  private  @Valid  String title;
  private  @Valid  String detail;
  private  @Valid  String instance;

  public Problem(){}

  /***
   * Constructor Problem
   ****/
  public Problem(String type,
                 String title,
                 String detail,
                 String instance ) {
    this.type = type;
    this.title = title;
    this.detail = detail;
    this.instance = instance;
  }

  /**
   * Atributo type: getter, setter y method chaining
   **/
  @JsonProperty("type")
  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public Problem type(String type) {
    this.type = type;
    return this;
  }


  /**
   * Atributo title: getter, setter y method chaining
   **/
  @JsonProperty("title")
  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Problem title(String title) {
    this.title = title;
    return this;
  }


  /**
   * Atributo detail: getter, setter y method chaining
   **/
  @JsonProperty("detail")
  public String getDetail() {
    return detail;
  }

  public void setDetail(String detail) {
    this.detail = detail;
  }

  public Problem detail(String detail) {
    this.detail = detail;
    return this;
  }


  /**
   * Atributo instance: getter, setter y method chaining
   **/
  @JsonProperty("instance")
  public String getInstance() {
    return instance;
  }

  public void setInstance(String instance) {
    this.instance = instance;
  }

  public Problem instance(String instance) {
    this.instance = instance;
    return this;
  }



  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Problem problem = (Problem) o;
    return Objects.equals(this.type, problem.type) &&
            Objects.equals(this.title, problem.title) &&
            Objects.equals(this.detail, problem.detail) &&
            Objects.equals(this.instance, problem.instance);
  }

  @Override
  public int hashCode() {
    return Objects.hash(type, title, detail, instance);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Problem {\n");

    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("    title: ").append(toIndentedString(title)).append("\n");
    sb.append("    detail: ").append(toIndentedString(detail)).append("\n");
    sb.append("    instance: ").append(toIndentedString(instance)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

  /********
   *  Creation Design Pattern: Builder
   **********/
  public static Builder builder() {
    return new Builder();
  }

  public static class Builder {
    //Atributos de la entidad
    private String type;
    private String title;
    private String detail;
    private String instance;

    public Builder type(String type) {
      this.type = type;
      return this;
    }
    public Builder title(String title) {
      this.title = title;
      return this;
    }
    public Builder detail(String detail) {
      this.detail = detail;
      return this;
    }
    public Builder instance(String instance) {
      this.instance = instance;
      return this;
    }

    public Problem build() {
      return new Problem(type, title, detail, instance);
    }
  }
}

