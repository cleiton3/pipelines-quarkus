package co.sodimac.vedigital.reportes.domain.contribucion;

import co.sodimac.vedigital.reportes.domain.domains.MetaReporteMsg;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.Document;

import java.util.ArrayList;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReporteContribucionMsg {
    private ArrayList<Document> data;
    private MetaReporteMsg meta;
}
