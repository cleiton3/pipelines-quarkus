package co.sodimac.vedigital.reportes.domain.domainsDasboards.ejecutivo.Filtros;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FiltroTotalCotizaciones {
    private String ejecutivo;
}
