package co.sodimac.vedigital.reportes.domain.efectividad;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RespuestaEfectividad {
    private String zona;
    private String ejecutivoId;
    private String ejecutivoNombre;
    private String codigoTienda;
    private String descripcionTienda;
    private String numeroCotizacion;
    private String numeroNegociacion;
    private String stickerNotaPedido;
    private String fechaCreacion;
    private String estadoNotaPedido;
    private String oc;
    private String estadoOc;
    private Date fechaDevolucion;
    private String numeroDevolucion;
    private String consecutivoNP;
    private String factura;
    private String clienteNitConsecutivo;
    private String clienteNombre;
}
