package co.sodimac.vedigital.reportes.domain.efectividad;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FiltrosEfectividad {
    private List<Integer> zonas;
    private List<Integer> tiendas;
    private List<Integer> cotizaciones;
    private List<String> stickers;
    private List<String> negociaciones;
//    Ejemplos para estadosNP: ["SIN PAGO REGISTRADO", "EN ALISTAMIENTO", "ENTREGADO COMPLETO PENDIENTE DE PAGO",
//    "MERCANCIA COMPLETA PENDIENTE POR CARGAR EN VEHICIULO", "EN ALISTAMIENTO MICROBLEND", "MERCANCIA PARCIAL
//    GRABADA Y ALISTADA", MERCANCIA COMPLETA PENDIENTE POR CARGAR EN VEHICIULO]
    private List<String> estadosNP;
    private List<String> oc;
//    Ejemplo para estadoOC = ["On Order", "Fully Received", "Cancel"]
    private List<String> estadosOC;
    private List<String> numDevoluciones;
    private List<String> consecutivosNP;
    private List<String> facturas;
    private List<String> ejecutivos;
    private List<String> clientes;
    private Fecha fechaCreacion;
}
