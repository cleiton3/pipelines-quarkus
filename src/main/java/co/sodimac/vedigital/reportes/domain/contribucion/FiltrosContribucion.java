package co.sodimac.vedigital.reportes.domain.contribucion;

import co.sodimac.vedigital.reportes.domain.domains.ContribucionRango;
import co.sodimac.vedigital.reportes.domain.domains.MargenRango;
import co.sodimac.vedigital.reportes.domain.domains.TotalRango;
import co.sodimac.vedigital.reportes.domain.topsku.Fecha;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FiltrosContribucion {
    private List<String> ejecutivo;
    private List<String> avance;
    private List<String> zonas;
    private List<String> tiendas;

    private Fecha fechaVigencia;

    private Fecha fechaCreacion;

    private String tipoTotal;
    private Double total;
    private TotalRango totalRango;

    private String tipoTotalContribucion;
    private Double totalContribucion;
    private ContribucionRango contribucionRango;

    private String tipoTotalMargen;
    private Double totalMargen;
    private MargenRango margenRango;
}
