package co.sodimac.vedigital.reportes.domain.tiempos;

import co.sodimac.vedigital.reportes.domain.domains.MetaReporteMsg;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.Document;

import java.util.ArrayList;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReporteTiemposResMsg {
    private ArrayList<Document> data;
    private MetaReporteMsg meta;
}
