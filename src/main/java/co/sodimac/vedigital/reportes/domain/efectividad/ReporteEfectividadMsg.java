package co.sodimac.vedigital.reportes.domain.efectividad;

import co.sodimac.vedigital.reportes.domain.domains.MetaReporteMsg;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReporteEfectividadMsg {
    private List<RespuestaEfectividad> data;
    private MetaReporteMsg meta;
}
