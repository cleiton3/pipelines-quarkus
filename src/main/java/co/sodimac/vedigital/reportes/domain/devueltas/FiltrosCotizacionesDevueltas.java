package co.sodimac.vedigital.reportes.domain.devueltas;

import co.sodimac.vedigital.reportes.domain.domains.ContribucionRango;
import co.sodimac.vedigital.reportes.domain.domains.MargenRango;
import co.sodimac.vedigital.reportes.domain.domains.TotalRango;
import co.sodimac.vedigital.reportes.domain.topsku.Fecha;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FiltrosCotizacionesDevueltas {
    private List<String> cotizaciones;
    private List<String> ejecutivo;
    private List<String> tiendas;
    private List<String> avances;

    private Fecha fechaCreacion;

    private String tipoTotalContribucion;
    private Double totalContribucion;
    private ContribucionRango contribucionRango;

    private String tipoTotal;
    private Double total;
    private TotalRango totalRango;

    private String tipoTotalMargen;
    private Double totalMargen;
    private MargenRango margenRango;

    private List<String> estado;
    private List<String> zonas;
    private Fecha fechaVigencia;

}
