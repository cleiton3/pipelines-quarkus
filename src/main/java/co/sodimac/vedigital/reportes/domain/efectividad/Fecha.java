package co.sodimac.vedigital.reportes.domain.efectividad;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Fecha {
    private String fechaInicial;
    private String fechaFinal;
}
