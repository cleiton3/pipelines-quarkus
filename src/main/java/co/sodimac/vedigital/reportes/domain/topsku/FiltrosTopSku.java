package co.sodimac.vedigital.reportes.domain.topsku;

import co.sodimac.vedigital.reportes.domain.domains.ContribucionRango;
import co.sodimac.vedigital.reportes.domain.domains.MargenRango;
import co.sodimac.vedigital.reportes.domain.domains.TotalRango;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FiltrosTopSku {
//    private Fecha fecha;
    private List<String> familias;
    private List<Integer> tiendas;
    private List<Integer> productos;

    private String tipoTotal;
    private Double total;
    private TotalRango totalRango;

    private String tipoTotalContribucion;
    private Double totalContribucion;
    private ContribucionRango contribucionRango;

    private String tipoTotalMargen;
    private Double totalMargen;
    private MargenRango margenRango;

    private List<String> proveedores;
}
