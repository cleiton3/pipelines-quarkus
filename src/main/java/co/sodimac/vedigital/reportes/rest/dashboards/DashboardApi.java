package co.sodimac.vedigital.reportes.rest.dashboards;

import co.sodimac.vedigital.reportes.domain.domainsDasboards.ejecutivo.EstadosCotizacionesMsg;
import co.sodimac.vedigital.reportes.domain.domainsDasboards.ejecutivo.Filtros.FiltroEstadosCotizaciones;
import co.sodimac.vedigital.reportes.domain.domainsDasboards.ejecutivo.Filtros.FiltroMetaCotizaciones;
import co.sodimac.vedigital.reportes.domain.domainsDasboards.ejecutivo.Filtros.FiltroTotalCotizaciones;
import co.sodimac.vedigital.reportes.domain.domainsDasboards.ejecutivo.MetaCotizacionesMsg;
import co.sodimac.vedigital.reportes.domain.domainsDasboards.ejecutivo.ResDashboardsMsg;
import co.sodimac.vedigital.reportes.domain.domainsDasboards.ejecutivo.TotalCotizaionesMsg;
import co.sodimac.vedigital.reportes.service.dashboards.ejecutivo.*;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.jboss.logging.Logger;
import org.jboss.resteasy.annotations.jaxrs.QueryParam;

import javax.inject.Inject;
import javax.ws.rs.Path;


import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/reportes")
public class DashboardApi {
    private static final Logger LOG = Logger.getLogger(DashboardApi.class);

    @Inject
    TotalCotizacionesService totalCotizacionesService;

    @Inject
    EstadosCotizacionesService estadosCotizacionesService;

    @Inject
    TopEjecutivosService topEjecutivosService;

    @Inject
    TopClientesService topClientesService;

    @Inject
    EjecutivosService ejecutivosService;

    @Inject
    AnalistasService analistasService;

    @Inject
    TopZonasService topZonasService;

    @Inject
    MetaCotizacionesService metaCotizacionesService;


    /**
     * Consulta dashboard total cotizaciones - ejecutivo
     * @param filtro - Filtro para realizar la busqueda
     * @return
     */
    @PUT
    @Path("/dashboard/total-cotizaciones")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Tag(name="Dashboards")
    @APIResponses(
            value = {
                    @APIResponse(
                            responseCode = "200",
                            description = "",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = TotalCotizaionesMsg.class))
                    ),
                    @APIResponse(
                            responseCode = "500",
                            description = "",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    @Operation(
            summary = "",
            description = ""
    )
    public Response getTotalCotizacionesService(
            @RequestBody(
                    description = "",
                    required = true,
                    content = @Content(mediaType = "application/json",schema = @Schema(implementation = FiltroTotalCotizaciones.class))
            )
            FiltroTotalCotizaciones filtro) {
        LOG.infof("@getTotalCotizacionesService, Consulta dashboard ejecutivo - total cotizaciones");
        LOG.infof("@getTotalCotizacionesService, Consulta dashboard ejecutivo - el ejecutivo es el siguiente: " + filtro.getEjecutivo());

        return Response.
                status(200).
                entity(totalCotizacionesService.getTotalCotizacionesService(filtro)).
                build();
    }

    /**
     * Consulta dashboard top ejecutivos - tienda
     * @param tienda - Filtro de tienda para realizar la búsqueda
     * @return
     */
    @GET
    @Path("/dashboard/top-ejecutivos")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Tag(name="Dashboards")
    @APIResponses(
            value = {
                    @APIResponse(
                            responseCode = "200",
                            description = "",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResDashboardsMsg.class))
                    ),
                    @APIResponse(
                            responseCode = "500",
                            description = "",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    @Operation(
            summary = "",
            description = ""
    )
    public Response getTopEjecutivos(
            @Parameter(
                    description = "",
                    example = ""
            )
            @QueryParam("tienda") @DefaultValue("0") String tienda) {
        LOG.infof("@getTopEjecutivos, Consulta dashboard top ejecutivos por tienda: %s", tienda);
        return Response.
                status(200).
                entity(topEjecutivosService.getTopEjecutivos(tienda)).
                build();
    }

    /**
     * Consulta dashboard top clientes - ejecutivo
     * @param ejecutivo - Filtro de ejecutivo para realizar la búsqueda
     * @return
     */
    @GET
    @Path("/dashboard/top-clientes")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Tag(name="Dashboards")
    @APIResponses(
            value = {
                    @APIResponse(
                            responseCode = "200",
                            description = "",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResDashboardsMsg.class))
                    ),
                    @APIResponse(
                            responseCode = "500",
                            description = "",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    @Operation(
            summary = "",
            description = ""
    )
    public Response getTopClientes(
            @Parameter(
                    description = "",
                    example = ""
            )
            @QueryParam("ejecutivo") @DefaultValue("0") String ejecutivo) {
        LOG.infof("@getTopClientes, Consulta dashboard top clientes por ejecutivo: %s", ejecutivo);
        return Response.
                status(200).
                entity(topClientesService.getTopClientes(ejecutivo)).
                build();
    }

    /**
     * Consulta lista ejecutivos
     * @return
     */
    @GET
    @Path("/dashboard/ejecutivos")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Tag(name="Dashboards")
    @APIResponses(
            value = {
                    @APIResponse(
                            responseCode = "200",
                            description = "",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResDashboardsMsg.class))
                    ),
                    @APIResponse(
                            responseCode = "500",
                            description = "",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    @Operation(
            summary = "",
            description = ""
    )
    public Response getEjecutivos() {
        LOG.infof("@getEjecutivos, Consulta dashboard ejecutivos");
        return Response.
                status(200).
                entity(ejecutivosService.getEjecutivos()).
                build();
    }

    /**
     * Consulta lista analistas
     * @return
     */
    @GET
    @Path("/dashboard/analistas")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Tag(name="Dashboards")
    @APIResponses(
            value = {
                    @APIResponse(
                            responseCode = "200",
                            description = "",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResDashboardsMsg.class))
                    ),
                    @APIResponse(
                            responseCode = "500",
                            description = "",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    @Operation(
            summary = "",
            description = ""
    )
    public Response getAnalistas() {
        LOG.infof("@getEjecutivos, Consulta dashboard ejecutivos");
        return Response.
                status(200).
                entity(analistasService.getAnalistas()).
                build();
    }

    /**
     * Consulta dashboard top zonas
     * @return
     */
    @GET
    @Path("/dashboard/zonas-tiendas")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Tag(name="Dashboards")
    @APIResponses(
            value = {
                    @APIResponse(
                            responseCode = "200",
                            description = "",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResDashboardsMsg.class))
                    ),
                    @APIResponse(
                            responseCode = "500",
                            description = "",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    @Operation(
            summary = "",
            description = ""
    )
    public Response getZonasTiendas() {
        LOG.infof("@getZonasTiendas, Consulta dashboard zonas tiendas");
        return Response.
                status(200).
                entity(topZonasService.getZonasTiendas()).
                build();
    }

    /**
     * Consulta dashboard top zonas
     * @return
     */
    @GET
    @Path("/dashboard/top-zonas")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Tag(name="Dashboards")
    @APIResponses(
            value = {
                    @APIResponse(
                            responseCode = "200",
                            description = "",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ResDashboardsMsg.class))
                    ),
                    @APIResponse(
                            responseCode = "500",
                            description = "",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    @Operation(
            summary = "",
            description = ""
    )
    public Response getTopZonas() {
        LOG.infof("@getTopZonas, Consulta dashboard top zonas");
        return Response.
                status(200).
                entity(topZonasService.getTopZonas()).
                build();
    }

    /**
     * Consulta dashboard estados cotizaciones - ejecutivo
     * @param filtro - Filtro para realizar la busqueda
     * @return
     */
    @PUT
    @Path("/dashboard/estados-cotizaciones")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Tag(name="Dashboards")
    @APIResponses(
            value = {
                    @APIResponse(
                            responseCode = "200",
                            description = "",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = EstadosCotizacionesMsg.class))
                    ),
                    @APIResponse(
                            responseCode = "500",
                            description = "",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    @Operation(
            summary = "",
            description = ""
    )
    public Response getEstadosCotizacionesService(
            @RequestBody(
                    description = "",
                    required = true,
                    content = @Content(mediaType = "application/json",schema = @Schema(implementation = FiltroEstadosCotizaciones.class))
            )
            FiltroEstadosCotizaciones filtro) {
        LOG.infof("@getEstadosCotizacionesService, Consulta dashboard ejecutivo - estados cotizaciones");
        LOG.infof("@getEstadosCotizacionesService, Consulta dashboard ejecutivo - el ejecutivo es el siguiente: " + filtro.getEjecutivo());

        return Response.
                status(200).
                entity(estadosCotizacionesService.getEstadosCotizacionesService(filtro)).
                build();
    }

    /**
     * Consulta dashboard meta cotizaciones - ejecutivo
     * @param filtro - Filtro para realizar la busqueda
     * @return
     */
    @PUT
    @Path("/dashboard/meta")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Tag(name="Dashboards")
    @APIResponses(
            value = {
                    @APIResponse(
                            responseCode = "200",
                            description = "",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = MetaCotizacionesMsg.class))
                    ),
                    @APIResponse(
                            responseCode = "500",
                            description = "",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    @Operation(
            summary = "",
            description = ""
    )
    public Response getMetaCotizacionesService(
            @RequestBody(
                    description = "",
                    required = true,
                    content = @Content(mediaType = "application/json",schema = @Schema(implementation = FiltroMetaCotizaciones.class))
            )
            FiltroMetaCotizaciones filtro) {
        LOG.infof("@getMetaCotizacionesService, Consulta dashboard ejecutivo - meta cotizaciones");
        LOG.infof("@getMetaCotizacionesService, Consulta dashboard ejecutivo - el ejecutivo es el siguiente: " + filtro.getEjecutivo());

        return Response.
                status(200).
                entity(metaCotizacionesService.getMetaCotizacionesService(filtro)).
                build();
    }
}
