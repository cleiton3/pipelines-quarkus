package co.sodimac.vedigital.reportes.rest.reportes;

import co.sodimac.vedigital.reportes.domain.Problem;
import co.sodimac.vedigital.reportes.domain.contribucion.FiltrosContribucion;
import co.sodimac.vedigital.reportes.domain.contribucion.ReporteContribucionMsg;
import co.sodimac.vedigital.reportes.domain.devueltas.FiltrosCotizacionesDevueltas;
import co.sodimac.vedigital.reportes.domain.devueltas.ReporteCotizacionesDevueltasMsg;
import co.sodimac.vedigital.reportes.domain.efectividad.FiltrosEfectividad;
import co.sodimac.vedigital.reportes.domain.historial.FiltrosHistorial;
import co.sodimac.vedigital.reportes.domain.historial.ReporteHistorialMsg;
import co.sodimac.vedigital.reportes.domain.tiempos.FiltrosTiemposRes;
import co.sodimac.vedigital.reportes.domain.tiempos.ReporteTiemposResMsg;
import co.sodimac.vedigital.reportes.domain.topsku.FiltrosTopSku;
import co.sodimac.vedigital.reportes.domain.topsku.ReporteTopSkuMsg;
import co.sodimac.vedigital.reportes.service.reportes.*;
import co.sodimac.vedigital.reportes.service.reportes.refactor.*;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.openapi.annotations.media.Content;
import org.eclipse.microprofile.openapi.annotations.media.Schema;
import org.eclipse.microprofile.openapi.annotations.parameters.Parameter;
import org.eclipse.microprofile.openapi.annotations.parameters.RequestBody;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponse;
import org.eclipse.microprofile.openapi.annotations.responses.APIResponses;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;
import org.jboss.logging.Logger;
import org.jboss.resteasy.annotations.jaxrs.QueryParam;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.SQLException;

@Path("/reportes")
public class ReportesApi {

    private static final Logger LOG = Logger.getLogger(ReportesApi.class);

    @Inject
    ReporteContribucionService reporteContribucionService;

    @Inject
    ReporteTopSkuService reporteTopSkuService;

    @Inject
    ReporteCotizacionesDevueltasService reporteCotizacionesDevueltasService;

    @Inject
    ReporteHistorialService reporteHistorialService;

    @Inject
    ReporteTiemposResService reporteTiemposResService;

    @Inject
    ReporteEfectividadService reporteEfectividadService;

    @Inject
    ReporteRefactorTopSkuService reporteRefactorTopSkuService;

    @Inject
    ReporteRefactorContribucionService reporteRefactorContribucionService;

    @Inject
    ReporteRefactorCotizacionesDevueltasService reporteRefactorCotizacionesDevueltasService;

    @Inject
    ReporteRefactorHistorialService reporteRefactorHistorialService;

    @Inject
    ReportesRefactorTiemposService reportesRefactorTiemposService;


    /**
     *  Reportes
     */

    // Reporte top sku

    /**
     * Consulta reporte top-sku
     * @param page
     * @param cantidad
     * @param columna
     * @param orden
     * @return
     */
    @GET
    @Path("/top-sku")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Tag(name="Reportes")
    @APIResponses(
            value = {
                    @APIResponse(
                            responseCode = "200",
                            description = "Se obtiene satisfactoriamente el reporte de top sku",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ReporteTopSkuMsg.class))
                    ),
                    @APIResponse(
                            responseCode = "500",
                            description = "Hubo un error al consultar el reporte de top sku",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    @Operation(
            summary = "Reporte top sku",
            description = "Reporte de los productos más cotizados en el sistema venta empresa"
    )
    public Response getListadoTopSku(
            @Parameter(
                    name= "page",
                    description = "Número de página que se lista en reporte top sku",
                    example = "1"
            )
            @QueryParam("page") @DefaultValue("1") int page,
            @Parameter(
                    name= "size",
                    description = "Tamaño de registros que se listan por página en reporte top sku",
                    example = "20"
            )
            @QueryParam("size") @DefaultValue("20") int cantidad,
            @Parameter(
                    name= "columna",
                    description = "Nombre de la columna para hacer el ordenamiento de top sku",
                    example = "cantidad"
            )
            @QueryParam("columna") @DefaultValue("cantidad") String columna,
            @Parameter(
                    name= "orden",
                    description = "Si es 0 se ordena la columna de mayor a menor en reporte top sku",
                    example = "0"
            )
            @QueryParam("orden") @DefaultValue("0") String orden) {
        LOG.infof("@listadoTopSku, Consulta de top sku");

        return Response.
                status(200).
                entity(reporteRefactorTopSkuService.getListadoTopSku(page, cantidad, orden, columna)).
                build();
    }


    /**
     * Consulta top-sku, filtros
     *
     * @param page
     * @param cantidad
     * @param columna
     * @param orden
     * @param filtros
     * @return
     */
    @PUT
    @Path("/top-sku")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Tag(name="Reportes")
    @APIResponses(
            value = {
                    @APIResponse(
                            responseCode = "200",
                            description = "Se obtiene satisfactoriamente el reporte de top sku con sus filtros",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ReporteTopSkuMsg.class))
                    ),
                    @APIResponse(
                            responseCode = "500",
                            description = "Hubo un error al consultar el reporte de top sku con sus filtros",
                            content = @Content(mediaType = "application/json")
                    )})
    @Operation(
            summary = "Reporte top sku con filtros",
            description = "Reporte de los productos más cotizados con filtros en el sistema venta empresa"
    )
    public Response getListadoTopSkuFiltros(
            @Parameter(
                    name= "page",
                    description = "Número de página que se lista en reporte top sku con filtros",
                    example = "1"
            )
            @QueryParam("page") @DefaultValue("1") int page,
            @Parameter(
                    name= "size",
                    description = "Tamaño de registros de top sku con filtros que se listan por página",
                    example = "20"
            )
            @QueryParam("size") @DefaultValue("20") int cantidad,
            @Parameter(
                    name= "columna",
                    description = "Nombre de la columna para hacer el ordenamiento de top sku con filtros",
                    example = "cantidad"
            )
            @QueryParam("columna") @DefaultValue("cantidad") String columna,
            @Parameter(
                    name= "orden",
                    description = "Si es 0 se ordena la columna de mayor a menor en reporte top sku con filtros",
                    example = "0"
            )
            @QueryParam("orden") @DefaultValue("0") String orden,
            @RequestBody(
                    name = "filtros",
                    description = "Filtros que se aplicarán al reporte top sku",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = FiltrosTopSku.class)),
                    required = true
            )
            FiltrosTopSku filtros) {
        LOG.infof("@getListadoTopSkuFiltros, Consulta de top sku, con el siguiente filtro, Filtro: %s", filtros);

        return Response.
                status(200).
                entity(reporteRefactorTopSkuService.getListadoTopSkuFiltros(page, cantidad, orden, columna, filtros)).
                build();
    }

    // Reporte contribuciones

    /**
     * Consulta contribución - margen
     * @param page
     * @param cantidad
     * @param columna
     * @param orden
     * @return
     */
    @GET
    @Path("/contribucion")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Tag(name="Reportes")
    @APIResponses(
            value = {
                    @APIResponse(
                            responseCode = "200",
                            description = "Se obtiene satisfactoriamente el reporte de contribución",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ReporteContribucionMsg.class))
                    ),
                    @APIResponse(
                            responseCode = "500",
                            description = "Hubo un error al consultar el reporte de contribución",
                            content = @Content(mediaType = "application/json")
                    )})
    @Operation(
            summary = "Reporte de contribución",
            description = "Reporte de contribución que se realiza en el sistema venta empresa"
    )
    public Response getListadoContribucion(
            @Parameter(
                    name= "page",
                    description = "Número de página que se lista en reporte de contribución",
                    example = "1"
            )
            @QueryParam("page") @DefaultValue("1") int page,
            @Parameter(
                    name= "size",
                    description = "Tamaño de registros que se listan por página en reporte de contribución",
                    example = "20"
            )
            @QueryParam("size") @DefaultValue("20") int cantidad,
            @Parameter(
                    name= "columna",
                    description = "Nombre de la columna para hacer el ordenamiento de reporte de contribución",
                    example = "totalContribucion"
            )
            @QueryParam("columna") @DefaultValue("totalContribucion") String columna,
            @Parameter(
                    name= "orden",
                    description = "Si es 0 se ordena la columna de mayor a menor en reporte de contribución",
                    example = "0"
            )
            @QueryParam("orden") @DefaultValue("0") String orden) {
        LOG.infof("@getListadoContribucion, Consulta reporte contribución");

        return Response.
                status(200).
                entity(reporteRefactorContribucionService.getListadoContribucion(page, cantidad, orden, columna)).
                build();
    }


    /**
     * Consulta contribución - margen (filtros)
     *
     * @param page
     * @param cantidad
     * @param columna
     * @param orden
     * @param filtros
     * @return
     */
    @PUT
    @Path("/contribucion")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Tag(name="Reportes")
    @APIResponses(
            value = {
                    @APIResponse(
                            responseCode = "200",
                            description = "Se obtiene satisfactoriamente el reporte de contribución con filtros",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ReporteContribucionMsg.class))
                    ),
                    @APIResponse(
                            responseCode = "500",
                            description = "Hubo un error al consultar el reporte de contribución con filtros",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    @Operation(
            summary = "Reporte de contribución con filtros",
            description = "Reporte de contribución con filtros que se realiza en el sistema venta empresa"
    )
    public Response getListadoContribucionFiltros(
            @Parameter(
                    name= "page",
                    description = "Número de página que se lista en reporte de contribución con filtros",
                    example = "1"
            )
            @QueryParam("page") @DefaultValue("1") int page,
            @Parameter(
                    name= "size",
                    description = "Tamaño de registros que se listan por página en reporte de contribución",
                    example = "20"
            )
            @QueryParam("size") @DefaultValue("20") int cantidad,
            @Parameter(
                    name= "columna",
                    description = "Nombre de la columna para hacer el ordenamiento de reporte de contribución",
                    example = "totalContribucion"
            )
            @QueryParam("columna") @DefaultValue("totalContribucion") String columna,
            @Parameter(
                    name= "orden",
                    description = "Si es 0 se ordena la columna de mayor a menor en reporte de contribución",
                    example = "0"
            )
            @QueryParam("orden") @DefaultValue("0") String orden,
            @RequestBody(
                    name = "filtros",
                    description = "Filtros que se aplicarán al reporte de contribución",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = FiltrosContribucion.class)),
                    required = true
            )
            FiltrosContribucion filtros) {
        LOG.infof("@getListadoTopSkuFiltros, Consulta de contribución y margen, con el siguiente filtro, Filtro: %s",
                filtros);

        return Response.
                status(200).
                entity(reporteRefactorContribucionService.getListadoContribucionFiltros(page, cantidad, orden, columna, filtros)).
                build();
    }


    // Reporte contribuciones

    /**
     * Consulta cotizaciones devueltas
     * @param page
     * @param cantidad
     * @param columna
     * @param orden
     * @return
     */
    @GET
    @Path("/cotizaciones-devueltas")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Tag(name="Reportes")
    @APIResponses(
            value = {
                    @APIResponse(
                            responseCode = "200",
                            description = "Se obtiene satisfactoriamente el reporte de cotizaciones devueltas",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ReporteCotizacionesDevueltasMsg.class))
                    ),
                    @APIResponse(
                            responseCode = "500",
                            description = "Hubo un error al consultar el reporte de cotizaciones devueltas",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    @Operation(
            summary = "Reporte de cotizaciones devueltas",
            description = "Reporte de cotizaciones devueltas en el sistema venta empresa"

    )
    public Response getListadoCotDevueltas(
            @Parameter(
                    name= "page",
                    description = "Número de página que se lista en reporte de cotizaciones devueltas",
                    example = "1"
            )
            @QueryParam("page") @DefaultValue("1") int page,
            @Parameter(
                    name= "size",
                    description = "Tamaño de registros que se listan por página en reporte de cotizaciones devueltas",
                    example = "20"
            )
            @QueryParam("size") @DefaultValue("20") int cantidad,
            @Parameter(
                    name= "columna",
                    description = "Nombre de la columna para hacer el ordenamiento de cotizaciones devueltas",
                    example = "numeroCotizaciones"
            )
            @QueryParam("columna") @DefaultValue("numeroCotizaciones") String columna,
            @Parameter(
                    name= "orden",
                    description = "Si es 0 se ordena la columna de mayor a menor en reporte de cotizaciones devueltas",
                    example = "0"
            )
            @QueryParam("orden") @DefaultValue("0") String orden) {
        LOG.infof("@getListadoCotDevueltas, Consulta reporte cotizaciones devueltas");

        return Response.
                status(200).
                entity(reporteRefactorCotizacionesDevueltasService.getListadoCotizacionesDevueltas(page, cantidad, orden,
                        columna)).
                build();
    }


    /**
     * Consulta cotizaciones devueltas (filtros)
     *
     * @param page
     * @param cantidad
     * @param columna
     * @param orden
     * @param filtros
     * @return
     */
    @PUT
    @Path("/cotizaciones-devueltas")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Tag(name="Reportes")
    @APIResponses(
            value = {
                    @APIResponse(
                            responseCode = "200",
                            description = "Se obtiene satisfactoriamente el reporte de cotizaciones devueltas con " +
                                    "filtros",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ReporteCotizacionesDevueltasMsg.class))
                    ),
                    @APIResponse(
                            responseCode = "500",
                            description = "Hubo un error al consultar el reporte de cotizaciones devueltas con filtros",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    @Operation(
            summary = "Reporte de cotizaciones devueltas con filtros",
            description = "Reporte de cotizaciones devueltas con filtros en el sistema venta empresa"

    )
    public Response getListadoCotDevueltasFiltros(
            @Parameter(
                    name= "page",
                    description = "Número de página que se lista en reporte de cotizaciones devueltas con filtros",
                    example = "1"
            )
            @QueryParam("page") @DefaultValue("1") int page,
            @Parameter(
                    name= "size",
                    description = "Tamaño de registros que se listan por página en reporte de cotizaciones devuelas " +
                            "con filtros",
                    example = "20"
            )
            @QueryParam("size") @DefaultValue("20") int cantidad,
            @Parameter(
                    name= "columna",
                    description = "Nombre de la columna para hacer el ordenamiento de cotizaciones devueltas con " +
                            "filtros",
                    example = "numeroCotizaciones"
            )
            @QueryParam("columna") @DefaultValue("numeroCotizaciones") String columna,
            @Parameter(
                    name= "orden",
                    description = "Si es 0 se ordena la columna de mayor a menor en reporte de cotizaciones devueltas" +
                            " con filtros",
                    example = "0"
            )
            @QueryParam("orden") @DefaultValue("0") String orden,
            @RequestBody(
                    name = "filtros",
                    description = "Filtros que se aplicarán al reporte de cotizaciones devueltas",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = FiltrosCotizacionesDevueltas.class)),
                    required = true
            )
            FiltrosCotizacionesDevueltas filtros) {
        LOG.infof("@getListadoCotDevueltasFiltros, Consulta de cotizaciones devueltas, con el siguiente filtro, " +
                        "Filtro: %s",
                filtros);

        return Response.
                status(200).
                entity(reporteRefactorCotizacionesDevueltasService.getListadoCotizacionesDevueltasFiltros(page, cantidad, orden, columna, filtros)).
                build();
    }

    // Reporte Historial

    /**
     * Consulta historial cotizaciones
     * @param page
     * @param cantidad
     * @param columna
     * @param orden
     * @return
     */
    @GET
    @Path("/historial")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Tag(name="Reportes")
    @APIResponses(
            value = {
                    @APIResponse(
                            responseCode = "200",
                            description = "Se obtiene satisfactoriamente el reporte de historial de cotización",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ReporteHistorialMsg.class))
                    ),
                    @APIResponse(
                            responseCode = "500",
                            description = "Hubo un error al consultar el reporte de historial de cotización",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    @Operation(
            summary = "Reporte de historial de cotización",
            description = "Reporte de historial de cotizaciones en el sistema venta empresa"

    )
    public Response getListadoHistorial(
            @Parameter(
                    name= "page",
                    description = "Número de página que se lista en reporte de historial de cotización",
                    example = "1"
            )
            @QueryParam("page") @DefaultValue("1") int page,
            @Parameter(
                    name= "size",
                    description = "Tamaño de registros que se listan por página en reporte de historial de cotización",
                    example = "20"
            )
            @QueryParam("size") @DefaultValue("20") int cantidad,
            @Parameter(
                    name= "columna",
                    description = "Nombre de la columna para hacer el ordenamiento de historial cotización",
                    example = "numeroCotizaciones"
            )
            @QueryParam("columna") @DefaultValue("numeroCotizaciones") String columna,
            @Parameter(
                    name= "orden",
                    description = "Si es 0 se ordena la columna de mayor a menor en reporte de historial de cotización",
                    example = "0"
            )
            @QueryParam("orden") @DefaultValue("0") String orden) {
        LOG.infof("@getListadoHistorial, Consulta reporte cotizaciones historial");

        return Response.
                status(200).
                entity(reporteRefactorHistorialService.getListadoHistorialCotizacion(page, cantidad, orden,
                        columna)).
                build();
    }


    /**
     * Consulta historial cotizaciones (filtros)
     *
     * @param page
     * @param cantidad
     * @param columna
     * @param orden
     * @param filtros
     * @return
     */
    @PUT
    @Path("/historial")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Tag(name="Reportes")
    @APIResponses(
            value = {
                    @APIResponse(
                            responseCode = "200",
                            description = "Se obtiene satisfactoriamente el reporte de historial de cotización con " +
                                    "filtros",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ReporteHistorialMsg.class))
                    ),
                    @APIResponse(
                            responseCode = "500",
                            description = "Hubo un error al consultar el reporte de historial de cotización con " +
                                    "filtros",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    @Operation(
            summary = "Reporte de historial de cotización con filtros",
            description = "Reporte de historial de cotizaciones con filtros en el sistema venta empresa"

    )
    public Response getListadoHistorialFiltros(
            @Parameter(
                    name= "page",
                    description = "Número de página que se lista en reporte de historial de cotización con filtros",
                    example = "1"
            )
            @QueryParam("page") @DefaultValue("1") int page,
            @Parameter(
                    name= "size",
                    description = "Tamaño de registros que se listan por página en reporte de historial de cotización" +
                            " con filtros",
                    example = "20"
            )
            @QueryParam("size") @DefaultValue("20") int cantidad,
            @Parameter(
                    name= "columna",
                    description = "Tamaño de registros que se listan por página en reporte de historial de cotización" +
                            " con filtros",
                    example = "creacion"
            )
            @QueryParam("columna") @DefaultValue("creacion") String columna,
            @Parameter(
                    name= "orden",
                    description = "Si es 0 se ordena la columna de mayor a menor en reporte de historial de " +
                            "cotización con filtros",
                    example = "0"
            )
            @QueryParam("orden") @DefaultValue("0") String orden,
            @RequestBody(
                    name = "filtros",
                    description = "Filtros que se aplicarán al reporte de historial de cotización con filtros",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = FiltrosHistorial.class)),
                    required = true
            )
            FiltrosHistorial filtros) {
        LOG.infof("@getListadoHistorialFiltros, Consulta de historial cotizaciones, con el siguiente filtro, " +
                        "Filtro: %s",
                filtros);

        return Response.
                status(200).
                entity(reporteRefactorHistorialService.getListadoHistorialCotizacionFiltros(page, cantidad, orden, columna,
                        filtros)).
                build();
    }

    // Reporte tiempos de respuesta

    /**
     * Consulta tiempos de respuesta
     * @param page
     * @param cantidad
     * @param columna
     * @param orden
     * @return
     */
    @GET
    @Path("/tiempos")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Tag(name="Reportes")
    @APIResponses(
            value = {
                    @APIResponse(
                            responseCode = "200",
                            description = "",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ReporteTiemposResMsg.class))
                    ),
                    @APIResponse(
                            responseCode = "500",
                            description = "",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    @Operation(
            summary = "",
            description = ""
    )
    public Response getListadoTiempos(
            @Parameter(
                    name= "page",
                    description = "",
                    example = ""
            )
            @QueryParam("page") @DefaultValue("1") int page,
            @Parameter(
                    name= "size",
                    description = "",
                    example = ""
            )
            @QueryParam("size") @DefaultValue("20") int cantidad,
            @Parameter(
                    name= "columna",
                    description = "",
                    example = ""
            )
            @QueryParam("columna") @DefaultValue("numeroCotizaciones") String columna,
            @Parameter(
                    name= "orden",
                    description = "",
                    example = ""
            )
            @QueryParam("orden") @DefaultValue("0") String orden) {
        LOG.infof("@getListadoTiempos, Consulta reporte tiempos de respuesta");

        return Response.
                status(200).
                entity(reportesRefactorTiemposService.getListadoTiemposRes(page, cantidad, orden,
                        columna)).
                build();
    }


    /**
     * Consulta reportes tiempos de respuesta (filtros)
     *
     * @param page
     * @param cantidad
     * @param columna
     * @param orden
     * @param filtros
     * @return
     */
    @PUT
    @Path("/tiempos")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Tag(name="Reportes")
    @APIResponses(
            value = {
                    @APIResponse(
                            responseCode = "200",
                            description = "Se obtiene satisfactoriamente el reporte de tiempos de respuesta con " +
                                    "filtros",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ReporteTiemposResMsg.class))
                    ),
                    @APIResponse(
                            responseCode = "500",
                            description = "Hubo un error al consultar el reporte de tiempos de respuesta con filtros",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    @Operation(
            summary = "Reporte de tiempos de respuesta con filtros",
            description = "Reporte de tiempos de respuesta con filtros en el sistema venta empresa"

    )
    public Response getListadoTiemposFiltros(
            @Parameter(
                    name= "page",
                    description = "Número de página que se lista en reporte de tiempos de respuesta con filtros",
                    example = "1"
            )
            @QueryParam("page") @DefaultValue("1") int page,
            @Parameter(
                    name= "size",
                    description = "Tamaño de registros que se listan por página en reporte de tiempos de respuesta " +
                            "con filtros",
                    example = "20"
            )
            @QueryParam("size") @DefaultValue("20") int cantidad,
            @Parameter(
                    name= "columna",
                    description = "Nombre de la columna para hacer el ordenamiento de reporte de tiempos de respuesta" +
                            " con filtros",
                    example = "numeroCotizaciones"
            )
            @QueryParam("columna") @DefaultValue("numeroCotizaciones") String columna,
            @Parameter(
                    name= "orden",
                    description = "Si es 0 se ordena la columna de mayor a menor en reporte de tiempos de respuesta" +
                            " con filtros",
                    example = "0"
            )
            @QueryParam("orden") @DefaultValue("0") String orden,
            @RequestBody(
                    name = "filtros",
                    description = "Filtros que se aplicarán al reporte de tiempos de respuesta",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation =
                            FiltrosTiemposRes.class)),
                    required = true
            )
                    FiltrosTiemposRes filtros) {
        LOG.infof("@getListadoTiemposFiltros, Consulta de tiempos de respuesta, con el siguiente filtro, " +
                        "Filtro: %s",
                filtros);

        return Response.
                status(200).
                entity(reportesRefactorTiemposService.getListadoTiemposResFiltros(page, cantidad, orden,
                columna, filtros)).
                build();
    }

    @GET
    @Path("/efectividad")
    @Produces(MediaType.APPLICATION_JSON)
    @Tag(name="Reportes")
    @APIResponses(
            value = {
                    @APIResponse(
                            responseCode = "500",
                            description = "Falla en consulta de reporte de efectividad",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = Problem.class))
                    ),
                    @APIResponse(
                            responseCode = "200",
                            description = "Obtiene reporte de efectividad")})
    @Operation(
            summary = "Crea un reporte de efectividad")
    public Response getListadoEfectividad(
            @Parameter(
                    name= "rInicial",
                    description = "",
                    example = "0"
            )
            @QueryParam("rInicial") @DefaultValue("0") int rInicial,
            @Parameter(
                    name= "rFinal",
                    description = "",
                    example = "20"
            )
            @QueryParam("rFinal") @DefaultValue("20") int rFinal
    ) throws SQLException {
        LOG.infof("@getListadoEfectividad, Consulta de reporte de efectividad");
        LOG.infof("@getListadoEfectividad, Consulta de reporte de efectividad: Inicial: " + rInicial);
        LOG.infof("@getListadoEfectividad, Consulta de reporte de efectividad: FInal: " + rFinal);

        return Response.
                status(200).
                entity(reporteEfectividadService.getReporteEfectividad(rInicial, rFinal)).
                build();
    }


    @PUT
    @Path("/efectividad")
    @Produces(MediaType.APPLICATION_JSON)
    @Tag(name = "Reportes")
    @APIResponses(
            value = {
                    @APIResponse(
                            responseCode = "500",
                            description = "Falla en consulta de reporte de efectividad con filtros",
                            content = @Content(mediaType = "application/json",
                                    schema = @Schema(implementation = Problem.class))
                    ),
                    @APIResponse(
                            responseCode = "200",
                            description = "Obtiene reporte de efectividad con filtros")})
    @Operation(
            summary = "Crea un reporte de efectividad con filtros")
    public Response getListadoEfectividadFiltros(@QueryParam("rInicial") @DefaultValue("0") int rInicial,
                                                 @QueryParam("rFinal") @DefaultValue("20") int rFinal,
                                                 @RequestBody(
                                                         name = "filtros",
                                                         description = "Filtros que se aplicarán al reporte de " +
                                                                 "efectividad",
                                                         content = @Content(mediaType = "application/json", schema =
                                                         @Schema(implementation =
                                                                 FiltrosEfectividad.class)),
                                                         required = true
                                                 )
                                                             FiltrosEfectividad filtros
    ) throws SQLException {
        LOG.infof("@getListadoEfectividadFiltros, Consulta de reporte de efectividad con filtros");

        return Response.
                status(200).
                entity(reporteEfectividadService.getReporteEfectividadFiltros(rInicial, rFinal, filtros)).
                build();
    }

    // Reporte top sku

    /**
     * Consulta reporte top-sku
     * @param page
     * @param cantidad
     * @param columna
     * @param orden
     * @return
     */
    @GET
    @Path("/refactor")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Tag(name="Reportes")
    @APIResponses(
            value = {
                    @APIResponse(
                            responseCode = "200",
                            description = "Se obtiene satisfactoriamente el reporte de top sku",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ReporteTopSkuMsg.class))
                    ),
                    @APIResponse(
                            responseCode = "500",
                            description = "Hubo un error al consultar el reporte de top sku",
                            content = @Content(mediaType = "application/json")
                    )
            }
    )
    @Operation(
            summary = "Reporte top sku",
            description = "Reporte de los productos más cotizados en el sistema venta empresa"
    )
    public Response getListadoTopSkuRefactor(
            @Parameter(
                    name= "page",
                    description = "Número de página que se lista en reporte top sku",
                    example = "1"
            )
            @QueryParam("page") @DefaultValue("1") int page,
            @Parameter(
                    name= "size",
                    description = "Tamaño de registros que se listan por página en reporte top sku",
                    example = "20"
            )
            @QueryParam("size") @DefaultValue("20") int cantidad,
            @Parameter(
                    name= "columna",
                    description = "Nombre de la columna para hacer el ordenamiento de top sku",
                    example = "cantidad"
            )
            @QueryParam("columna") @DefaultValue("cantidad") String columna,
            @Parameter(
                    name= "orden",
                    description = "Si es 0 se ordena la columna de mayor a menor en reporte top sku",
                    example = "0"
            )
            @QueryParam("orden") @DefaultValue("0") String orden) {
        LOG.infof("@listadoTopSku, Consulta de top sku");

        return Response.
                status(200).
                entity(reportesRefactorTiemposService.getListadoTiemposRes(page, cantidad, orden, columna)).
                build();
    }



    /**
     * Consulta top-sku, filtros
     *
     * @param page
     * @param cantidad
     * @param columna
     * @param orden
     * @param filtros
     * @return
     */
    @PUT
    @Path("/refactor")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Tag(name="Reportes")
    @APIResponses(
            value = {
                    @APIResponse(
                            responseCode = "200",
                            description = "Se obtiene satisfactoriamente el reporte de top sku con sus filtros",
                            content = @Content(mediaType = "application/json", schema = @Schema(implementation = ReporteTopSkuMsg.class))
                    ),
                    @APIResponse(
                            responseCode = "500",
                            description = "Hubo un error al consultar el reporte de top sku con sus filtros",
                            content = @Content(mediaType = "application/json")
                    )})
    @Operation(
            summary = "Reporte top sku con filtros",
            description = "Reporte de los productos más cotizados con filtros en el sistema venta empresa"
    )
    public Response getListadoTopSkuFiltrosRefactor(
            @Parameter(
                    name= "page",
                    description = "Número de página que se lista en reporte top sku con filtros",
                    example = "1"
            )
            @QueryParam("page") @DefaultValue("1") int page,
            @Parameter(
                    name= "size",
                    description = "Tamaño de registros de top sku con filtros que se listan por página",
                    example = "20"
            )
            @QueryParam("size") @DefaultValue("20") int cantidad,
            @Parameter(
                    name= "columna",
                    description = "Nombre de la columna para hacer el ordenamiento de top sku con filtros",
                    example = "cantidad"
            )
            @QueryParam("columna") @DefaultValue("cantidad") String columna,
            @Parameter(
                    name= "orden",
                    description = "Si es 0 se ordena la columna de mayor a menor en reporte top sku con filtros",
                    example = "0"
            )
            @QueryParam("orden") @DefaultValue("0") String orden,
            @RequestBody(
                    name = "filtros",
                    description = "Filtros que se aplicarán al reporte top sku",
                    content = @Content(mediaType = "application/json", schema = @Schema(implementation = FiltrosTopSku.class)),
                    required = true
            )
                    FiltrosTiemposRes filtros) {
        LOG.infof("@getListadoTopSkuFiltros, Consulta de top sku, con el siguiente filtro, Filtro: %s", filtros);

        return Response.
                status(200).
                entity(reportesRefactorTiemposService.getListadoTiemposResFiltros(page, cantidad, orden, columna, filtros)).
                build();
    }

}
