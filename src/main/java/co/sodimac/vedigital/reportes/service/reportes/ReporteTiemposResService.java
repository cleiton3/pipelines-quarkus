package co.sodimac.vedigital.reportes.service.reportes;

import co.sodimac.vedigital.reportes.domain.domains.MetaReporteMsg;
import co.sodimac.vedigital.reportes.domain.tiempos.FiltrosTiemposRes;
import co.sodimac.vedigital.reportes.domain.tiempos.ReporteTiemposResMsg;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import io.quarkus.cache.CacheKey;
import io.quarkus.cache.CacheResult;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Aggregates.limit;
import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Sorts.*;

@ApplicationScoped
public class ReporteTiemposResService {

    private static final Logger LOG = Logger.getLogger(ReporteTiemposResService.class);

    @Inject
    MongoClient mongoClient;

    private MongoCollection getCollectionReporteTiemposRes() {
        LOG.infof("@getCollectionReporteTiemposRes, Conexion a la base de datos ReporteTiemposRespuesta");
        return mongoClient.getDatabase("datosLinea").getCollection("ReporteTiemposRespuesta");
    }

    public void actualizacionVistas() {
        getCollectionReporteTiemposRes().drop();
        mongoClient.getDatabase("datosLinea").createView("ReporteTiemposRespuesta", "CotizacionMsg", Arrays.asList(
                new Document("$lookup",
                        new Document("from", "LineaMsg").
                                append("localField", "contexto._id").
                                append("foreignField", "_id.cotizacionId").
                                append("as", "lineas")
                ),
                new Document("$project",
                        new Document("_id", new Document("ejecutivo", "$meta.dataauthor").
                                append("referencia", "$contexto.referencia").
                                append("idCotizacion", "$contexto._id").
                                append("estadoCotizacion", "$contexto.estado").
                                append("avance", "$contexto.avance").
                                append("idTiendaCreacion", "$data.idTiendaCreacion").
                                append("idTiendaDespacho", "$data.idTiendaDespacho").
                                append("idZonaTiendaCreacion", "$data.idZonaTiendaCreacion").
                                append("idZonaTiendaDespacho", "$data.idZonaTiendaDespacho").
                                append("subclienteConsecutivo", "$data.clienteSubcliente.subclienteConsecutivo").
                                append("subclienteNombre", "$data.clienteSubcliente.subclienteNombre").
                                append("clienteNumeroDoc", "$data.clienteSubcliente.clienteNumeroDoc").
                                append("clienteNombre", "$data.clienteSubcliente.clienteNombre").
                                append("remisionada", "$contexto.remisionada").
                                append("creacion", "$meta.time").
                                append("vigenciaDesdeFecha", "$data.vigenciaDesdeFecha").
                                append("vigenciaHastaFecha", "$data.vigenciaHastaFecha").
                                // Falta agregar la categoría de cliente, Juan se encarga de agregarlo en
                                // clienteSubcliente de CotizacionMsg
                                // append("categoriaCliente", "$data.clienteSubcliente.categoriaCliente").
                                        append("lineas", "$lineas")).
                                append("totalContribucion", new Document("$sum", "$lineas.data.contribucion")).
                                append("totalVenta", new Document("$sum", "$lineas.data.ventaTotal")).
                                append("tiempoRespuestaAprobacion", new Document("$sum", "$lineas.data.aprobaciones" +
                                        ".revisionesAnalista.fechaRevision")).
                                append("totalMargen", new Document("$avg", "$lineas.data.margenPorcentaje"))
                ),
                match(ne("totalContribucion", null)),
                match(ne("totalContribucion", Float.NaN)),
                match(ne("totalVenta", null)),
                match(ne("totalVenta", Float.NaN)),
                match(ne("totalMargen", null)),
                match(ne("totalMargen", Float.NaN)),
                match(or(eq("_id.avance", "_6_APROBACION"), eq("_id.avance", "_7_APROBADO"), eq("_id.avance",
                        "_8_RESULTADOS")))
        ));
    }

    // Listados
    private String getColumna(String columna) {
        switch (columna) {
            case "ejecutivo":
                return "_id.ejecutivo";
            default:
                return "_id.creacion";
        }
    }

    /**
     * Obtiene el reporte de historial
     *
     * @param page
     * @param cantidad
     * @param orden
     * @param columna
     * @return
     */
    @CacheResult(cacheName = "reporte-tiempos-cache")
    public ReporteTiemposResMsg getListadoTiemposRes(@CacheKey int page, @CacheKey int cantidad,
                                                     @CacheKey String orden, @CacheKey String columna) {
        LOG.infof("@getListadoTiemposRes, Consulta a MONGO, para obtener tiempos de respuesta");

        columna = getColumna(columna);
        LOG.debugf("@getListadoTiemposRes - La columna es: %s", columna);

        Bson direction = descending(columna);
        if (orden.matches("1")) {
            direction = ascending(columna);
        }

        // Aqui crea la vista
        actualizacionVistas();

        var collection = getCollectionReporteTiemposRes();
        AggregateIterable<Document> output = collection.
                aggregate(Arrays.asList(
                        sort(orderBy(direction)),
                        skip((page - 1) * cantidad),
                        limit(cantidad)
                ));

        var listaTiempos = output.into(new ArrayList<>());

        long cantidadDocumentos = collection.countDocuments();
        LOG.infof("Lista respuesta: %s", listaTiempos.toString());
        LOG.infof("Cantidad lista respuesta: %s", cantidadDocumentos);
        return ReporteTiemposResMsg.builder().
                data(listaTiempos).
                meta(
                        MetaReporteMsg.builder().
                                cantidadDocumentos(cantidadDocumentos)
                                .build()).
                build();
    }


    /**
     * Devuelve listado de tiempos de respuesta, según los filtros, si no se ingresa un filtro, devuelve la lista
     * completa
     *
     * @param page
     * @param cantidad
     * @param orden
     * @param columna
     * @param filtros
     * @return
     */
    @CacheResult(cacheName = "reporte-tiempos-cache")
    public ReporteTiemposResMsg getListadoTiemposResFiltros(@CacheKey int page, @CacheKey int cantidad,
                                                            @CacheKey String orden,
                                                            @CacheKey String columna,
                                                            @CacheKey FiltrosTiemposRes filtros) {
        LOG.infof("@getListadoTopSkuFiltros, Consulta a MONGO, para obtener top sku");

        var collection = getCollectionReporteTiemposRes();
        List<Bson> queries = new ArrayList<>();
        List<Bson> stepsAggregate = new ArrayList<>();

        columna = getColumna(columna);

        Bson direction = descending(columna);
        if (orden.matches("1")) {
            direction = ascending(columna);
        }
        if(filtros.getCotizaciones() != null && !filtros.getCotizaciones().isEmpty()){
            queries.add(
                    or(
                            in("_id.idCotizacion", filtros.getCotizaciones()),
                            in("_id.referencia", filtros.getCotizaciones())
                    ));
        }
        if(queries.isEmpty()){
            if (filtros.getEjecutivos() != null && !filtros.getEjecutivos().isEmpty()) {
                queries.add(in("_id.ejecutivo", filtros.getEjecutivos()));
            }
            if(filtros.getAnalistas() != null && !filtros.getAnalistas().isEmpty()){
                queries.add(
                        in("lineas.data.aprobaciones.revisionesAnalista.idAnalista", filtros.getAnalistas())
                );
            }
            if (filtros.getAvances() != null && !filtros.getAvances().isEmpty()) {
                queries.add(
                        in("_id.avance", filtros.getAvances())
                );
            }
            if (filtros.getTiendas() != null && !filtros.getTiendas().isEmpty()) {
                queries.add(
                        or(
                                in("_id.idTiendaCreacion",filtros.getTiendas()),
                                in("_id.idTiendaDespacho", filtros.getTiendas())
                        )
                );
            }
            if (filtros.getZonas() != null && !filtros.getZonas().isEmpty()) {
                queries.add(
                        or(
                                in("_id.idZonaTiendaCreacion",filtros.getZonas()),
                                in("_id.idZonaTiendaDespacho", filtros.getZonas())
                        )
                );
            }
            if (filtros.getFechaVigencia() != null) {
                queries.add(
                        and(
                                gte("_id.vigenciaDesdeFecha", filtros.getFechaVigencia().getFechaInicial()),
                                lte("_id.vigenciaHastaFecha", filtros.getFechaVigencia().getFechaFinal())
                        )
                );
            }
            if (filtros.getFechaCreacion() != null) {
                queries.add(
                        and(
                                gte("_id.creacion", filtros.getFechaCreacion().getFechaInicial()),
                                lte("_id.creacion", filtros.getFechaCreacion().getFechaFinal())
                        )
                );
            }
            if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals("igual")) {
                queries.add(in("totalVenta", filtros.getTotal()));
            }
            if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals(
                    "mayor")) {
                queries.add(gte("totalVenta", filtros.getTotal()));
            }
            if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals(
                    "menor")) {
                queries.add(lte("totalVenta", filtros.getTotal()));
            }
            if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals(
                    "rango")) {
                queries.add(
                        and(
                                gte("totalVenta", filtros.getTotalRango().getRangoTotalMin()),
                                lte("totalVenta", filtros.getTotalRango().getRangoTotalMax())
                        )
                );
            }
            if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals("igual")) {
                queries.add(in("totalContribucion", filtros.getTotalContribucion()));
            }
            if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals(
                    "mayor")) {
                queries.add(gte("totalContribucion", filtros.getTotalContribucion()));
            }
            if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals(
                    "menor")) {
                queries.add(lte("totalContribucion", filtros.getTotalContribucion()));
            }
            if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals(
                    "rango")) {
                queries.add(
                        and(
                                gte("totalContribucion", filtros.getContribucionRango().getContribucionMin()),
                                lte("totalContribucion", filtros.getContribucionRango().getContribucionMax())
                        )
                );
            }
            if (filtros.getTipoTotalMargen() != null && !filtros.getTipoTotalMargen().isEmpty() && filtros.getTipoTotalMargen().equals(
                    "rango")) {
                queries.add(
                        and(
                                gte("totalMargen", filtros.getMargenRango().getMargenMin()),
                                lte("totalMargen", filtros.getMargenRango().getMargenMax())
                        )
                );
            }
        }
        if (!queries.isEmpty()) {
            stepsAggregate.add(match(and(queries)));
        }
        //Cuenta la cantidad de documentos
        stepsAggregate.add(count());
        AggregateIterable<Document> countAggregateIterable = collection.aggregate(stepsAggregate);

        var countList = countAggregateIterable.into(new ArrayList<>());
        var cantidadDocumentos = 0;
        if (countList != null && !countList.isEmpty()) {
            cantidadDocumentos = (Integer) countList.get(0).get("count");
        }

        //Consulta los documentos que cumplen con el filtro
        stepsAggregate.remove(stepsAggregate.size() - 1);
        stepsAggregate.add(sort(orderBy(direction)));
        stepsAggregate.add(skip((page - 1) * cantidad));
        stepsAggregate.add(limit(cantidad));
        AggregateIterable<Document> output = collection.aggregate(stepsAggregate);

        var listaTiempos = output.into(new ArrayList<>());
        LOG.infof("Lista respuesta: %s", listaTiempos);
        LOG.infof("Cantidad lista respuesta: %s", cantidadDocumentos);
        return ReporteTiemposResMsg.builder().
                data(listaTiempos).
                meta(
                        MetaReporteMsg.builder().
                                cantidadDocumentos(cantidadDocumentos)
                                .build()).
                build();
    }


}
