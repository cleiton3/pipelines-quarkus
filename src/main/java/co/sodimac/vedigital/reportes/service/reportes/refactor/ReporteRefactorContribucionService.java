package co.sodimac.vedigital.reportes.service.reportes.refactor;

import co.sodimac.vedigital.reportes.domain.contribucion.FiltrosContribucion;
import co.sodimac.vedigital.reportes.domain.contribucion.ReporteContribucionMsg;
import co.sodimac.vedigital.reportes.domain.domains.MetaReporteMsg;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import io.quarkus.cache.CacheKey;
import io.quarkus.cache.CacheResult;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Sorts.ascending;
import static com.mongodb.client.model.Sorts.descending;

@ApplicationScoped
public class ReporteRefactorContribucionService {

    private static final Logger LOG = Logger.getLogger(ReporteRefactorContribucionService.class);

    @Inject
    MongoClient mongoClient;

    private String getColumna(String columna) {
        switch (columna) {
            case "ejecutivo":
                return "_id.ejecutivo";
            case "avance":
                return "_id.avance";
            case "tiendaCreacion":
                return "_id.tiendaCreacion";
            case "tiendaDespacho":
                return "_id.tiendaDespacho";
            case "zonaCreacion":
                return "_id.zonaCreacion";
            case "fechaCreacion":
                return "_id.fechaCreacion";
            case "vigenciaDesdeFecha":
                return "_id.vigenciaDesdeFecha";
            case "vigenciaHastaFecha":
                return "_id.vigenciaHastaFecha";
            case "zonaDespacho":
                return "_id.zonaDespacho";
            case "numeroCotizaciones":
                return "numeroCotizaciones";
            case "totalContribucion":
                return "totalContribucion";
            case "totalVenta":
                return "totalVenta";
            case "totalMargen":
                return "totalMargen";
            default:
                return "totalContribucion";
        }
    }


    /**
     * Obtiene el reporte de contribución y margen
     *
     * @param page
     * @param cantidad
     * @param orden
     * @param columna
     * @return
     */
    @CacheResult(cacheName = "reporte-contribucion-cache")
    public ReporteContribucionMsg getListadoContribucion(@CacheKey int page, @CacheKey int cantidad,
                                                         @CacheKey String orden,
                                                         @CacheKey String columna) {
        LOG.infof("@getListadoContribucion, Consulta a MONGO, para obtener reporte contribuciones");

        MongoDatabase db = mongoClient.getDatabase("datosLinea");
        MongoCollection<Document> lineasMsg = db.getCollection("CotizacionMsg");

        columna = getColumna(columna);
        LOG.debugf("La columna es: " + columna);

        Bson sort = sort(descending(columna));
        if (orden.matches("1")) {
            sort = sort(ascending(columna));
        }
        Bson limit = limit(cantidad);
        Bson skip = skip((page - 1) * cantidad);

        Bson lookup = new Document("$lookup",
                new Document("from", "LineaMsg").
                        append("localField", "contexto._id").
                        append("foreignField", "_id.cotizacionId").
                        append("as", "lineas"));

        Bson project = new Document("$project",
                new Document("_id", new Document("ejecutivo", "$meta.dataauthor").
                        append("avance", "$contexto.avance").
                        append("tiendaCreacion", "$data.idTiendaCreacion").
                        append("tiendaDespacho", "$data.idTiendaDespacho").
                        append("zonaCreacion", "$data.idZonaTiendaCreacion").
                        append("fechaCreacion", "$meta.time").
                        append("vigenciaDesdeFecha", "$data.vigenciaDesdeFecha").
                        append("vigenciaHastaFecha", "$data.vigenciaHastaFecha").
                        append("zonaDespacho", "$data.idZonaTiendaDespacho")).
                        append("totalContribucion", new Document("$sum", "$lineas.data.contribucion")).
                        append("totalVenta", new Document("$sum", "$lineas.data.ventaTotal")).
                        append("totalMargen", new Document("$avg", "$lineas.data.margenPorcentaje")));

        Bson matchNotTotalMargen = match(ne("totalMargen", 0.0));
        Bson matchNullTotalMargen = match(ne("totalMargen", null));
        Bson matchNaNTotalMargen = match(ne("totalMargen", Float.NaN));

        Bson matchNotTotalContribucion = match(ne("totalContribucion", 0.0));
        Bson matchNullTotalContribucion = match(ne("totalContribucion", null));
        Bson matchNaNTotalContribucion = match(ne("totalContribucion", Float.NaN));

        Bson matchNotTotalVenta = match(ne("totalVenta", 0.0));
        Bson matchNullTotalVenta = match(ne("totalVenta", null));
        Bson matchNaNTotalVenta = match(ne("totalVenta", Float.NaN));

        Bson group = new Document("$group",
                new Document("_id", new Document("ejecutivo", "$_id.ejecutivo").
                        append("avance", "$_id.avance").
                        append("tiendaCreacion", "$_id.tiendaCreacion").
                        append("tiendaDespacho", "$_id.tiendaDespacho").
                        append("zonaCreacion", "$_id.zonaCreacion").
                        append("fechaCreacion", "$_id.fechaCreacion").
                        append("vigenciaDesdeFecha", "$_id.vigenciaDesdeFecha").
                        append("vigenciaHastaFecha", "$_id.vigenciaHastaFecha").
                        append("zonaDespacho", "$_id.zonaDespacho")).
                        append("numeroCotizaciones", new Document("$sum", 1)).
                        append("totalContribucion", new Document("$sum", "$totalContribucion")).
                        append("totalVenta", new Document("$sum", "$totalVenta")).
                        append("totalMargen", new Document("$avg", "$totalMargen")));

        List<Document> results =
                lineasMsg.aggregate(Arrays.asList(
                        lookup,
                        project,
                        matchNotTotalMargen,
                        matchNullTotalMargen, matchNaNTotalMargen,
                        matchNotTotalContribucion,
                        matchNullTotalContribucion, matchNaNTotalContribucion,
                        matchNotTotalVenta,
                        matchNullTotalVenta, matchNaNTotalVenta
                        , group
                        , sort, skip, limit
                )).into(new ArrayList<>());

        List<Document> cantidadDocumentosContribucion = lineasMsg.aggregate(Arrays.asList(
                lookup,
                project,
                matchNotTotalMargen,
                matchNullTotalMargen, matchNaNTotalMargen,
                matchNotTotalContribucion,
                matchNullTotalContribucion, matchNaNTotalContribucion,
                matchNotTotalVenta,
                matchNullTotalVenta, matchNaNTotalVenta
                , group)).into(new ArrayList<>());

        long cantidadDocumentos = cantidadDocumentosContribucion.size();

        return ReporteContribucionMsg.builder().
                data(new ArrayList<>(results)).
                meta(
                        MetaReporteMsg.builder().
                                cantidadDocumentos(cantidadDocumentos)
                                .build()).
                build();
    }


    /**
     * Devuelve listado de contribución y margen, según los filtros, si no se ingresa un filtro, devuelve la lista
     * completa
     *
     * @param page
     * @param cantidad
     * @param orden
     * @param columna
     * @param filtros
     * @return
     */
    @CacheResult(cacheName = "reporte-contribucion-filtros-cache")
    public ReporteContribucionMsg getListadoContribucionFiltros(@CacheKey int page, @CacheKey int cantidad,
                                                                @CacheKey String orden, @CacheKey String columna,
                                                                @CacheKey FiltrosContribucion filtros) {
        LOG.infof("@getListadoContribucionFiltros, Consulta a MONGO, para obtener reporte de contribucion con filtros");

        MongoDatabase db = mongoClient.getDatabase("datosLinea");
        MongoCollection<Document> lineasMsg = db.getCollection("CotizacionMsg");

        List<Bson> queries = new ArrayList<>();

        columna = getColumna(columna);
        LOG.debugf("La columna es: " + columna);

        Bson sort = sort(descending(columna));
        if (orden.matches("1")) {
            sort = sort(ascending(columna));
        }

        Bson limit = limit(cantidad);
        Bson skip = skip((page - 1) * cantidad);


        Bson lookup = new Document("$lookup",
                new Document("from", "LineaMsg").
                        append("localField", "contexto._id").
                        append("foreignField", "_id.cotizacionId").
                        append("as", "lineas"));

        Bson project = new Document("$project",
                new Document("_id", new Document("ejecutivo", "$meta.dataauthor").
                        append("avance", "$contexto.avance").
                        append("tiendaCreacion", "$data.idTiendaCreacion").
                        append("tiendaDespacho", "$data.idTiendaDespacho").
                        append("zonaCreacion", "$data.idZonaTiendaCreacion").
                        append("fechaCreacion", "$meta.time").
                        append("vigenciaDesdeFecha", "$data.vigenciaDesdeFecha").
                        append("vigenciaHastaFecha", "$data.vigenciaHastaFecha").
                        append("zonaDespacho", "$data.idZonaTiendaDespacho")).
                        append("totalContribucion", new Document("$sum", "$lineas.data.contribucion")).
                        append("totalVenta", new Document("$sum", "$lineas.data.ventaTotal")).
                        append("totalMargen", new Document("$avg", "$lineas.data.margenPorcentaje")));

        Bson matchNotTotalMargen = match(ne("totalMargen", 0.0));
        Bson matchNullTotalMargen = match(ne("totalMargen", null));
        Bson matchNaNTotalMargen = match(ne("totalMargen", Float.NaN));

        Bson matchNotTotalContribucion = match(ne("totalContribucion", 0.0));
        Bson matchNullTotalContribucion = match(ne("totalContribucion", null));
        Bson matchNaNTotalContribucion = match(ne("totalContribucion", Float.NaN));

        Bson matchNotTotalVenta = match(ne("totalVenta", 0.0));
        Bson matchNullTotalVenta = match(ne("totalVenta", null));
        Bson matchNaNTotalVenta = match(ne("totalVenta", Float.NaN));

        Bson group = new Document("$group",
                new Document("_id", new Document("ejecutivo", "$_id.ejecutivo").
                        append("avance", "$_id.avance").
                        append("tiendaCreacion", "$_id.tiendaCreacion").
                        append("tiendaDespacho", "$_id.tiendaDespacho").
                        append("zonaCreacion", "$_id.zonaCreacion").
                        append("fechaCreacion", "$_id.fechaCreacion").
                        append("vigenciaDesdeFecha", "$_id.vigenciaDesdeFecha").
                        append("vigenciaHastaFecha", "$_id.vigenciaHastaFecha").
                        append("zonaDespacho", "$_id.zonaDespacho")).
                        append("numeroCotizaciones", new Document("$sum", 1)).
                        append("totalContribucion", new Document("$sum", "$totalContribucion")).
                        append("totalVenta", new Document("$sum", "$totalVenta")).
                        append("totalMargen", new Document("$avg", "$totalMargen")));

        queries.add(lookup);
        queries.add(project);
        queries.add(matchNotTotalMargen);
        queries.add(matchNullTotalMargen);
        queries.add(matchNaNTotalMargen);

        queries.add(matchNotTotalContribucion);
        queries.add(matchNullTotalContribucion);
        queries.add(matchNaNTotalContribucion);

        queries.add(matchNotTotalVenta);
        queries.add(matchNullTotalVenta);
        queries.add(matchNaNTotalVenta);
        queries.add(group);


        // Filtros

        if (filtros.getEjecutivo() != null && !filtros.getEjecutivo().isEmpty()) {
            queries.add(match(in("_id.ejecutivo", filtros.getEjecutivo())));
        }
        if (filtros.getAvance() != null && !filtros.getAvance().isEmpty()) {
            queries.add(match(
                    in("_id.avance", filtros.getAvance()))
            );
        }
        if (filtros.getFechaCreacion() != null) {
            queries.add(match(
                    and(
                            gte("_id.fechaCreacion", filtros.getFechaCreacion().getFechaInicial()),
                            lte("_id.fechaCreacion", filtros.getFechaCreacion().getFechaFinal())
                    ))
            );
        }
        if (filtros.getFechaVigencia() != null) {
            queries.add(match(
                    and(
                            gte("_id.vigenciaDesdeFecha", filtros.getFechaVigencia().getFechaInicial()),
                            lte("_id.vigenciaHastaFecha", filtros.getFechaVigencia().getFechaFinal())
                    ))
            );
        }
        if (filtros.getZonas() != null && !filtros.getZonas().isEmpty()) {
            queries.add(match(
                    or(
                            in("_id.zonaCreacion", filtros.getZonas()),
                            in("_id.zonaDespacho", filtros.getZonas())
                    ))
            );
        }

        if (filtros.getTiendas() != null && !filtros.getTiendas().isEmpty()) {
            queries.add(match(
                    or(
                            in("_id.tiendaCreacion", filtros.getTiendas()),
                            in("_id.tiendaDespacho", filtros.getTiendas())
                    ))
            );
        }

        if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals(
                "igual")) {
            queries.add(match(in("totalVenta", filtros.getTotal())));
        }
        if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals(
                "mayor")) {
            queries.add(match(gte("totalVenta", filtros.getTotal())));
        }
        if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals(
                "menor")) {
            queries.add(match(lte("totalVenta", filtros.getTotal())));
        }
        if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals(
                "rango")) {
            queries.add(match(
                    and(
                            gte("totalVenta", filtros.getTotalRango().getRangoTotalMin()),
                            lte("totalVenta", filtros.getTotalRango().getRangoTotalMax())
                    ))
            );
        }
        if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals("igual")) {
            queries.add(match(in("totalContribucion", filtros.getTotalContribucion())));
        }
        if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals(
                "mayor")) {
            queries.add(match(gte("totalContribucion", filtros.getTotalContribucion())));
        }
        if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals(
                "menor")) {
            queries.add(match(lte("totalContribucion", filtros.getTotalContribucion())));
        }
        if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals(
                "rango")) {
            queries.add(match(
                    and(
                            gte("totalContribucion", filtros.getContribucionRango().getContribucionMin()),
                            lte("totalContribucion", filtros.getContribucionRango().getContribucionMax())
                    ))
            );
        }
        if (filtros.getTipoTotalMargen() != null && !filtros.getTipoTotalMargen().isEmpty() && filtros.getTipoTotalMargen().equals(
                "rango")) {
            queries.add(match(
                    and(
                            gte("totalMargen", filtros.getMargenRango().getMargenMin()),
                            lte("totalMargen", filtros.getMargenRango().getMargenMax())
                    ))
            );
        }

        queries.add(sort);
        queries.add(skip);
        queries.add(limit);


        // Data
        List<Document> results = lineasMsg.aggregate(queries).into(new ArrayList<>());

        // Contador de documentos
        // se elimina limit
        queries.remove(queries.size() - 1);
        // se elimina skip
        queries.remove(queries.size() - 1);
        // se elimina sort
        queries.remove(queries.size() - 1);

        List<Document> resultsContador = lineasMsg.aggregate(queries).into(new ArrayList<>());
        long cantidadDocumentos = resultsContador.size();

        return ReporteContribucionMsg.builder().
                data(new ArrayList<>(results)).
                meta(
                        MetaReporteMsg.builder().
                                cantidadDocumentos(cantidadDocumentos)
                                .build()).
                build();
    }
}
