package co.sodimac.vedigital.reportes.service.reportes;

import co.sodimac.vedigital.reportes.domain.domains.MetaReporteMsg;

import co.sodimac.vedigital.reportes.domain.devueltas.FiltrosCotizacionesDevueltas;
import co.sodimac.vedigital.reportes.domain.devueltas.ReporteCotizacionesDevueltasMsg;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;

import io.quarkus.cache.CacheKey;
import io.quarkus.cache.CacheResult;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Sorts.*;


@ApplicationScoped
public class ReporteCotizacionesDevueltasService {

    private static final Logger LOG = Logger.getLogger(ReporteCotizacionesDevueltasService.class);

    @Inject
    MongoClient mongoClient;

    private MongoCollection getCollectionReporteCotizacionesDevueltas() {
        LOG.infof("@getCollectionReporteCotizacionesDevueltas, Conexion a la base de datos ReporteCotizacionesDevueltasService");
        return mongoClient.getDatabase("datosLinea").getCollection("ReporteCotizacionesDevueltasService");
    }

    public void actualizacionVistas(){
        getCollectionReporteCotizacionesDevueltas().drop();
        mongoClient.getDatabase("datosLinea").createView("ReporteCotizacionesDevueltasService","CotizacionMsg", Arrays.asList(
                new Document("$lookup",
                        new Document("from","LineaMsg").
                                append("localField","contexto._id").
                                append("foreignField","_id.cotizacionId").
                                append("as","lineas")
                ),
                new Document("$project",
                        new Document("_id", new Document("ejecutivo","$meta.dataauthor").
                                append("referencia", "$contexto.referencia").
                                append("idCotizacion", "$contexto._id").
                                append("estadoCotizacion", "$contexto.estado").
                                append("avance", "$contexto.avance").
                                append("idTiendaCreacion", "$data.idTiendaCreacion").
                                append("idTiendaDespacho", "$data.idTiendaDespacho").
                                append("idZonaTiendaCreacion", "$data.idZonaTiendaCreacion").
                                append("idZonaTiendaDespacho", "$data.idZonaTiendaDespacho").
                                append("subclienteConsecutivo", "$data.clienteSubcliente.subclienteConsecutivo").
                                append("subclienteNombre", "$data.clienteSubcliente.subclienteNombre").
                                append("clienteNumeroDoc", "$data.clienteSubcliente.clienteNumeroDoc").
                                append("clienteNombre", "$data.clienteSubcliente.clienteNombre").
                                append("remisionada", "$contexto.remisionada").
                                append("creacion", "$meta.time").
                                append("vigenciaDesdeFecha", "$data.vigenciaDesdeFecha").
                                append("vigenciaHastaFecha", "$data.vigenciaHastaFecha").

                                // Falta agregar la categoría de cliente, Juan se encarga de agregarlo en
                                // clienteSubcliente de CotizacionMsg
                                // append("categoriaCliente", "$data.clienteSubcliente.categoriaCliente").
                                        append("lineas", "$lineas").
                                        append("aprobaciones", "$lineas.data.aprobaciones")).
                                append("totalContribucion", new Document("$sum", "$lineas.data.contribucion")).
                                append("totalVenta", new Document("$sum", "$lineas.data.ventaTotal")).
                                append("totalMargen", new Document("$avg", "$lineas.data.margenPorcentaje"))
//                                .append("ultimaLineaAprobacion", new Document("$lineas.data.aprobaciones", new Document("$slice",-1)))
                ),
                match(ne("totalContribucion", null)),
                match(ne("totalContribucion", Float.NaN)),
                match(ne("totalVenta", null)),
                match(ne("totalVenta", Float.NaN)),
                match(ne("totalMargen", null)),
                match(ne("totalMargen", Float.NaN)),
                match(or(eq("_id.avance", "_6_APROBACION"), eq("_id.avance", "_7_APROBADO"), eq("_id.avance",
                        "_8_RESULTADOS")))
                , match(eq("_id.lineas.data.aprobaciones.estadoLinea","NEGACION_ANALISTA"))
                , match(ne("_id.lineas.data.flete", true))

//                match(eq("_id.lineas.data.aprobaciones.estadoLinea","NEGACION_ANALISTA")),
//                project(Projections.slice("_id.lineas.data.aprobaciones", 1,1)),

//                append("ultima aproba", new Document("$arrayElemAt", new Document("_id.lineas.data.aprobaciones",-1))).
//                match(eq("_id.lineas.data.aprobaciones.1.estadoLinea","PENDIENTE"))
        ));
    }


    // Listados
    private String getColumna(String columna) {
        switch (columna) {
            case "ejecutivo":
                return "_id.ejecutivo";
            case "avance":
                return "_id.avance";
            case "referencia":
                return "_id.referencia";
            case "clienteNumeroDoc":
                return "_id.clienteNumeroDoc";
            case "idZonaTiendaCreacion":
                return "_id.idZonaTiendaCreacion";
            case "idZonaTiendaDespacho":
                return "_id.idZonaTiendaDespacho";
            case "idTiendaCreacion":
                return "_id.idTiendaCreacion";
            case "idTiendaDespacho":
                return "_id.idTiendaDespacho";
            case "vigenciaDesdeFecha":
                return "_id.vigenciaDesdeFecha";
            case "vigenciaHastaFecha":
                return "_id.vigenciaHastaFecha";
            case "numeroCotizaciones":
                return "numeroCotizaciones";
            case "totalContribucion":
                return "totalContribucion";
            case "totalMargen":
                return "totalMargen";
            case "totalVenta":
                return "totalVenta";
            default:
                return "numeroCotizaciones";
        }
    }

    /**
     * Obtiene el reporte de cotizaciones devueltas
     *
     * @param page
     * @param cantidad
     * @param orden
     * @param columna
     * @return
     */
    @CacheResult(cacheName = "reporte-devueltas-cache")
    public ReporteCotizacionesDevueltasMsg getListadoCotizacionesDevueltas(@CacheKey int page, @CacheKey int cantidad,
                                                                           @CacheKey String orden,
                                                                           @CacheKey String columna) {
        LOG.infof("@getListadoCotizacionesDevueltas, Consulta a MONGO, para obtener cotizaciones devueltas");

        columna = getColumna(columna);
        LOG.debugf("@getListadoCotizacionesDevueltas La columna es: %s", columna);

        Bson direction = descending(columna);
        if (orden.matches("1")) {
            direction = ascending(columna);
        }

        // Aqui crea la vista
        actualizacionVistas();

        var collection = getCollectionReporteCotizacionesDevueltas();
        AggregateIterable<Document> output = collection.
                aggregate(Arrays.asList(
                        sort(orderBy(direction)),
                        skip((page - 1) * cantidad),
                        limit(cantidad)
                ));

        ArrayList<Document> listaCotDevueltas = output.into(new ArrayList<>());
        long cantidadDocumentos = collection.countDocuments();
        LOG.infof("Lista respuesta: %s", listaCotDevueltas);
        LOG.infof("Cantidad lista respuesta: %s", cantidadDocumentos);
        return ReporteCotizacionesDevueltasMsg.builder().
                data(listaCotDevueltas).
                meta(
                        MetaReporteMsg.builder().
                                cantidadDocumentos(cantidadDocumentos)
                                .build()).
                build();
    }

    /**
     * Devuelve listado de cotizaciones devueltas, según los filtros, si no se ingresa un filtro, devuelve la lista
     * completa
     *
     * @param page
     * @param cantidad
     * @param orden
     * @param columna
     * @param filtros
     * @return
     */
    @CacheResult(cacheName = "reporte-devueltas-cache")
    public ReporteCotizacionesDevueltasMsg getListadoCotizacionesDevueltasFiltros(@CacheKey int page,
                                                                                  @CacheKey int cantidad,
                                                                                  @CacheKey String orden,
                                                                                  @CacheKey String columna,
                                                                                  @CacheKey FiltrosCotizacionesDevueltas filtros) {
        LOG.infof("@getListadoCotizacionesDevueltasFiltros, Consulta a MONGO, para obtener cotizaciones devueltas");

        var collection = getCollectionReporteCotizacionesDevueltas();
        List<Bson> queries = new ArrayList<>();
        List<Bson> stepsAggregate = new ArrayList<>();

        columna = getColumna(columna);

        Bson direction = descending(columna);
        if (orden.matches("1")) {
            direction = ascending(columna);
        }

        if(filtros.getCotizaciones() != null && !filtros.getCotizaciones().isEmpty()){
            queries.add(
                    or(
                            in("_id.idCotizacion", filtros.getCotizaciones()),
                            in("_id.referencia", filtros.getCotizaciones())
                    ));
        }
        if(queries.isEmpty()){
            if (filtros.getEjecutivo() != null && !filtros.getEjecutivo().isEmpty()) {
                queries.add(in("_id.ejecutivo", filtros.getEjecutivo()));
            }
            if (filtros.getEstado() != null && !filtros.getEstado().isEmpty()) {
                queries.add(
                        in("_id.estadoCotizacion", filtros.getEstado())
                );
            }
            //aqui
            if (filtros.getAvances() != null && !filtros.getAvances().isEmpty()) {
                queries.add(
                        in("_id.avance", filtros.getAvances())
                );
            }
            if (filtros.getTiendas() != null && !filtros.getTiendas().isEmpty()) {
                queries.add(
                        or(
                                in("_id.idTiendaCreacion",filtros.getTiendas()),
                                in("_id.idTiendaDespacho", filtros.getTiendas())
                        )
                );
            }
            if (filtros.getZonas() != null && !filtros.getZonas().isEmpty()) {
                queries.add(
                        or(
                                in("_id.idZonaTiendaCreacion",filtros.getZonas()),
                                in("_id.idZonaTiendaDespacho", filtros.getZonas())
                        )
                );
            }
            if (filtros.getFechaVigencia() != null) {
                queries.add(
                        and(
                                gte("_id.vigenciaDesdeFecha", filtros.getFechaVigencia().getFechaInicial()),
                                lte("_id.vigenciaHastaFecha", filtros.getFechaVigencia().getFechaFinal())
                        )
                );
            }
            if (filtros.getFechaCreacion() != null) {
                queries.add(
                        and(
                                gte("_id.creacion", filtros.getFechaCreacion().getFechaInicial()),
                                lte("_id.creacion", filtros.getFechaCreacion().getFechaFinal())
                        )
                );
            }
            if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals("igual")) {
                queries.add(in("totalVenta", filtros.getTotal()));
            }
            if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals(
                    "mayor")) {
                queries.add(gte("totalVenta", filtros.getTotal()));
            }
            if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals(
                    "menor")) {
                queries.add(lte("totalVenta", filtros.getTotal()));
            }
            if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals(
                    "rango")) {
                queries.add(
                        and(
                                gte("totalVenta", filtros.getTotalRango().getRangoTotalMin()),
                                lte("totalVenta", filtros.getTotalRango().getRangoTotalMax())
                        )
                );
            }
            if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals("igual")) {
                queries.add(in("totalContribucion", filtros.getTotalContribucion()));
            }
            if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals(
                    "mayor")) {
                queries.add(gte("totalContribucion", filtros.getTotalContribucion()));
            }
            if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals(
                    "menor")) {
                queries.add(lte("totalContribucion", filtros.getTotalContribucion()));
            }
            if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals(
                    "rango")) {
                queries.add(
                        and(
                                gte("totalContribucion", filtros.getContribucionRango().getContribucionMin()),
                                lte("totalContribucion", filtros.getContribucionRango().getContribucionMax())
                        )
                );
            }
            if (filtros.getTipoTotalMargen() != null && !filtros.getTipoTotalMargen().isEmpty() && filtros.getTipoTotalMargen().equals(
                    "rango")) {
                queries.add(
                        and(
                                gte("totalMargen", filtros.getMargenRango().getMargenMin()),
                                lte("totalMargen", filtros.getMargenRango().getMargenMax())
                        )
                );
            }
        }

        if (!queries.isEmpty()) {
            stepsAggregate.add(match(and(queries)));
        }

        //Cuenta la cantidad de documentos
        stepsAggregate.add(count());
        AggregateIterable<Document> countAggregateIterable = collection.aggregate(stepsAggregate);

        var countList = countAggregateIterable.into(new ArrayList<>());
        var cantidadDocumentos = 0;
        if (countList != null && !countList.isEmpty()) {
            cantidadDocumentos = (Integer) countList.get(0).get("count");
        }

        //Consulta los documentos que cumplen con el filtro
        stepsAggregate.remove(stepsAggregate.size() - 1);
        stepsAggregate.add(sort(orderBy(direction)));
        stepsAggregate.add(skip((page - 1) * cantidad));
        stepsAggregate.add(limit(cantidad));
        AggregateIterable<Document> output = collection.aggregate(stepsAggregate);

        var listaCotDevueltas = output.into(new ArrayList<>());
        LOG.infof("Lista respuesta: %s", listaCotDevueltas);
        LOG.infof("Cantidad lista respuesta: %s", cantidadDocumentos);
        return ReporteCotizacionesDevueltasMsg.builder().
                data(listaCotDevueltas).
                meta(
                        MetaReporteMsg.builder().
                                cantidadDocumentos(cantidadDocumentos)
                                .build()).
                build();
    }

}
