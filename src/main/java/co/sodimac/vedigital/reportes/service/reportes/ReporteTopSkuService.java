package co.sodimac.vedigital.reportes.service.reportes;

import co.sodimac.vedigital.reportes.domain.topsku.FiltrosTopSku;
import co.sodimac.vedigital.reportes.domain.domains.MetaReporteMsg;
import co.sodimac.vedigital.reportes.domain.topsku.ReporteTopSkuMsg;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;

import io.quarkus.cache.CacheKey;
import io.quarkus.cache.CacheResult;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Sorts.*;

@ApplicationScoped
public class ReporteTopSkuService {

    private static final Logger LOG = Logger.getLogger(ReporteTopSkuService.class);

    @Inject
    MongoClient mongoClient;

    private MongoCollection getCollectionReporteTopSku() {
        LOG.infof("@getCollectionReporteTopSku, Conexion a la base de datos ReporteTopSku");
        return mongoClient.getDatabase("datosLinea").getCollection("ReporteTopSku");
    }

    public void actualizacionVistas(){
        getCollectionReporteTopSku().drop();
        mongoClient.getDatabase("datosLinea").createView("ReporteTopSku","LineaMsg", Arrays.asList(
                new Document("$group",
                        new Document("_id", new Document("sku", "$data.sku").
                                append("descripcion", "$data.descripcion").
                                append("proveedor", "$data.proveedor").
                                append("proveedores", "$data.proveedores").
                                append("jerarquia", "$data.jerarquia").
                                append("tienda", "$data.tiendaDespacho").
                                append("flete", "$data.esFlete")).
//                                append("fecha", "$meta.time")).
                                append("totalVenta", new Document("$sum", "$data.ventaTotal")).
                                append("totalContribucion", new Document("$sum", "$data.contribucion")).
                                append("totalMargen", new Document("$avg", "$data.margenPorcentaje")).
                                append("cantidad", new Document("$sum", 1))
                ),
                match(ne("_id.flete", true)),
                match(ne("totalMargen", null)),
                match(ne("totalMargen", Float.NaN)),
                match(ne("totalMargen", 0)),
                match(ne("totalContribucion", null)),
                match(ne("totalContribucion", Float.NaN)),
                match(ne("totalContribucion", 0)),
                match(ne("totalVenta", null)),
                match(ne("totalVenta", Float.NaN)),
                match(ne("totalVenta", 0))
        ));
    }

    // Listados
    private String getColumna(String columna) {
        switch (columna) {
            case "sku":
                return "_id.sku";
            case "descripcion":
                return "_id.descripcion";
            case "proveedor":
                return "_id.proveedor";
            case "departamento":
                return "_id.jerarquia.departmentName";
            case "familia":
                return "_id.jerarquia.familyName";
            case "subfamilia":
                return "_id.jerarquia.subfamilyName";
            case "grupo":
                return "_id.jerarquia.groupName";
            case "conjunto":
                return "_id.jerarquia.conjunctName";
            case "tienda":
                return "_id.tienda";
//            case "fecha":
//                return "_id.fecha";
            case "totalVenta":
                return "totalVenta";
            case "totalMargen":
                return "totalMargen";
            case "totalContribucion":
                return "totalContribucion";
            case "cantidad":
                return "cantidad";
            default:
                return "cantidad";
        }
    }


    /**
     * Obtiene el reporte de top sku
     *
     * @param page
     * @param cantidad
     * @param orden
     * @param columna
     * @return
     */
    @CacheResult(cacheName = "reporte-top-cache")
    public ReporteTopSkuMsg getListadoTopSku(@CacheKey int page, @CacheKey int cantidad, @CacheKey String orden,
                                             @CacheKey String columna) {
        LOG.infof("@getListadoTopSku, Consulta a MONGO, para obtener top sku");

        columna = getColumna(columna);
        LOG.debugf("getListadoTopSku La columna es: %s", columna);

        Bson direction = descending(columna);
        if (orden.matches("1")) {
            direction = ascending(columna);
        }

        // Aqui crea la vista
        actualizacionVistas();

        var collection = getCollectionReporteTopSku();
        AggregateIterable<Document> output = collection.
                aggregate(Arrays.asList(
                        sort(orderBy(direction)),
                        skip((page - 1) * cantidad),
                        limit(cantidad)
                ));

        var listaTopSku = output.into(new ArrayList<>());

        long cantidadDocumentos = collection.countDocuments();
        LOG.infof("Lista respuesta: %s", listaTopSku);
        LOG.infof("Cantidad lista respuesta: %s", cantidadDocumentos);
        return ReporteTopSkuMsg.builder().
                data(listaTopSku).
                meta(
                        MetaReporteMsg.builder().
                                cantidadDocumentos(cantidadDocumentos)
                                .build()).
                build();
    }


    /**
     * Devuelve listado de top sku, según los filtros, si no se ingresa un filtro, devuelve la lista completa
     *
     * @param page
     * @param cantidad
     * @param orden
     * @param columna
     * @param filtros
     * @return
     */
    @CacheResult(cacheName = "reporte-top-cache")
    public ReporteTopSkuMsg getListadoTopSkuFiltros(@CacheKey int page, @CacheKey int cantidad,
                                                    @CacheKey String orden, @CacheKey String columna,
                                                    @CacheKey FiltrosTopSku filtros) {
        LOG.infof("@getListadoTopSkuFiltros, Consulta a MONGO, para obtener top sku");

        var collection = getCollectionReporteTopSku();
        List<Bson> queries = new ArrayList<>();
        List<Bson> stepsAggregate = new ArrayList<>();

        columna = getColumna(columna);

        Bson direction = descending(columna);
        if (orden.matches("1")) {
            direction = ascending(columna);
        }

        if (filtros.getTiendas() != null && !filtros.getTiendas().isEmpty()) {
            queries.add(in("_id.tienda", filtros.getTiendas()));
        }
        if (filtros.getFamilias() != null && !filtros.getFamilias().isEmpty()) {
            queries.add(
                    in("_id.jerarquia.familia", filtros.getFamilias())
            );
        }
//        if (filtros.getFecha() != null) {
//            queries.add(
//                    and(
//                            gte("_id.fecha", filtros.getFecha().getFechaInicial()),
//                            lte("_id.fecha", filtros.getFecha().getFechaFinal())
//                    )
//            );
//        }
        if (filtros.getProductos() != null && !filtros.getProductos().isEmpty()) {
            queries.add(
                    in("_id.sku", filtros.getProductos())
            );
        }

        if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals("igual")) {
            queries.add(in("totalVenta", filtros.getTotal()));
        }
        if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals(
                "mayor")) {
            queries.add(gte("totalVenta", filtros.getTotal()));
        }
        if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals(
                "menor")) {
            queries.add(lte("totalVenta", filtros.getTotal()));
        }
        if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals(
                "rango")) {
            queries.add(
                    and(
                            gte("totalVenta", filtros.getTotalRango().getRangoTotalMin()),
                            lte("totalVenta", filtros.getTotalRango().getRangoTotalMax())
                    )
            );
        }
        if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals("igual")) {
            queries.add(in("totalContribucion", filtros.getTotalContribucion()));
        }
        if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals(
                "mayor")) {
            queries.add(gte("totalContribucion", filtros.getTotalContribucion()));
        }
        if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals(
                "menor")) {
            queries.add(lte("totalContribucion", filtros.getTotalContribucion()));
        }
        if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals(
                "rango")) {
            queries.add(
                    and(
                            gte("totalContribucion", filtros.getContribucionRango().getContribucionMin()),
                            lte("totalContribucion", filtros.getContribucionRango().getContribucionMax())
                    )
            );
        }
        if (filtros.getTipoTotalMargen() != null && !filtros.getTipoTotalMargen().isEmpty() && filtros.getTipoTotalMargen().equals(
                "rango")) {
            queries.add(
                    and(
                            gte("totalMargen", filtros.getMargenRango().getMargenMin()),
                            lte("totalMargen", filtros.getMargenRango().getMargenMax())
                    )
            );
        }
        if (filtros.getProveedores() != null && !filtros.getProveedores().isEmpty()) {
            queries.add(
                    in("_id.proveedor", filtros.getProveedores())
            );
        }

        if (!queries.isEmpty()) {
            stepsAggregate.add(match(and(queries)));
        }

        //Cuenta la cantidad de documentos
        stepsAggregate.add(count());
        AggregateIterable<Document> countAggregateIterable = collection.aggregate(stepsAggregate);

        var countList = countAggregateIterable.into(new ArrayList<>());
        var cantidadDocumentos = 0;
        if (countList != null && !countList.isEmpty()) {
            cantidadDocumentos = (Integer) countList.get(0).get("count");
        }

        //Consulta los documentos que cumplen con el filtro
        stepsAggregate.remove(stepsAggregate.size() - 1);
        stepsAggregate.add(sort(orderBy(direction)));
        stepsAggregate.add(skip((page - 1) * cantidad));
        stepsAggregate.add(limit(cantidad));
        AggregateIterable<Document> output = collection.aggregate(stepsAggregate);

        var listaTop = output.into(new ArrayList<>());
        LOG.infof("Lista respuesta: %s", listaTop);
        LOG.infof("Cantidad lista respuesta: %s", cantidadDocumentos);
        return ReporteTopSkuMsg.builder().
                data(listaTop).
                meta(
                        MetaReporteMsg.builder().
                                cantidadDocumentos(cantidadDocumentos)
                                .build()).
                build();
    }


}
