package co.sodimac.vedigital.reportes.service.dashboards.ejecutivo;

import co.sodimac.vedigital.reportes.domain.domainsDasboards.MetaDashboardMsg;
import co.sodimac.vedigital.reportes.domain.domainsDasboards.ejecutivo.ResDashboardsMsg;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.in;
import static com.mongodb.client.model.Sorts.descending;
import static com.mongodb.client.model.Sorts.orderBy;

@ApplicationScoped
public class TopZonasService {

    private static final Logger LOG = Logger.getLogger(TopZonasService.class);
    @Inject
    MongoClient mongoClient;

    private MongoCollection getCollectionTopZonas() {
        LOG.infof("@getCollectionTopZonas, Conexion a la base de datos DashTopZonas");
        return mongoClient.getDatabase("datosLinea").getCollection("DashTopZonas");
    }

    private MongoCollection getCollectionZonasTiendas() {
        LOG.infof("@getCollectionZonasTiendas, Conexion a la base de datos ZonasTiendas");
        return mongoClient.getDatabase("datosLinea").getCollection("ZonasTiendas");
    }

    public void actualizacionVistaTopZonas(){
        getCollectionTopZonas().drop();
        mongoClient.getDatabase("datosLinea").createView("DashTopZonas","CotizacionMsg", Arrays.asList(
                new Document("$group",
                        new Document("_id", new Document("zona", "$data.idZonaTiendaCreacion")).
                                append("cantidad", new Document("$sum", 1))
                )
        ));
    }

    public void actualizacionVistaZonasTiendas(){
        getCollectionZonasTiendas().drop();
        mongoClient.getDatabase("datosLinea").createView("ZonasTiendas","CotizacionMsg", Arrays.asList(
                new Document("$group",
                        new Document("_id",
                                new Document("zona", "$data.idZonaTiendaCreacion").
                                        append("tienda", "$data.idTiendaCreacion")
                        ).
                                append("cantidad", new Document("$sum", 1))
                )
        ));
    }


    public ResDashboardsMsg getTopZonas() {
        LOG.infof("@getTopZonas, Consulta a MONGO, para obtener TopZonas");

        actualizacionVistaTopZonas();

        var collection = getCollectionTopZonas();

        String columna = "cantidad";
        Integer cantidad = 10;


        Bson direction = descending(columna);

        List<Bson> queries = new ArrayList<>();
        List<Bson> stepsAggregate = new ArrayList<>();

        if (!queries.isEmpty()) {
            stepsAggregate.add(match(and(queries)));
        }

        //Cuenta la cantidad de documentos
        stepsAggregate.add(count());
        AggregateIterable<Document> countAggregateIterable = collection.aggregate(stepsAggregate);

        var countList = countAggregateIterable.into(new ArrayList<>());
        var cantidadDocumentos = 0;
        if (countList != null && !countList.isEmpty()) {
            cantidadDocumentos = (Integer) countList.get(0).get("count");
        }

        //Consulta los documentos que cumplen con el filtro
        stepsAggregate.remove(stepsAggregate.size() - 1);
        stepsAggregate.add(sort(orderBy(direction)));
        stepsAggregate.add(limit(cantidad));
        AggregateIterable<Document> output = collection.aggregate(stepsAggregate);

        var listaTop = output.into(new ArrayList<>());
        LOG.infof("Lista respuesta: %s", listaTop);
        LOG.infof("Cantidad lista respuesta: %s", cantidadDocumentos);

        return ResDashboardsMsg.builder().
                data(listaTop).
                meta(
                        MetaDashboardMsg.builder().
                                cantidadDocumentos(cantidadDocumentos)
                                .build()).
                build();
    }

    public ResDashboardsMsg getZonasTiendas() {
        LOG.infof("@getZonasTiendas, Consulta a MONGO, para obtener ZonasTiendas");

        actualizacionVistaZonasTiendas();

        var collection = getCollectionZonasTiendas();

        String columna = "_id.zona";
        Integer cantidad = 30;

        Bson direction = descending(columna);

        List<Bson> queries = new ArrayList<>();
        List<Bson> stepsAggregate = new ArrayList<>();

        if (!queries.isEmpty()) {
            stepsAggregate.add(match(and(queries)));
        }

        //Cuenta la cantidad de documentos
        stepsAggregate.add(count());
        AggregateIterable<Document> countAggregateIterable = collection.aggregate(stepsAggregate);

        var countList = countAggregateIterable.into(new ArrayList<>());
        var cantidadDocumentos = 0;
        if (countList != null && !countList.isEmpty()) {
            cantidadDocumentos = (Integer) countList.get(0).get("count");
        }

        //Consulta los documentos que cumplen con el filtro
        stepsAggregate.remove(stepsAggregate.size() - 1);
        stepsAggregate.add(sort(orderBy(direction)));
        stepsAggregate.add(limit(cantidad));
        AggregateIterable<Document> output = collection.aggregate(stepsAggregate);

        var listaTop = output.into(new ArrayList<>());
        LOG.infof("Lista respuesta: %s", listaTop);
        LOG.infof("Cantidad lista respuesta: %s", cantidadDocumentos);

        return ResDashboardsMsg.builder().
                data(listaTop).
                meta(
                        MetaDashboardMsg.builder().
                                cantidadDocumentos(cantidadDocumentos)
                                .build()).
                build();
    }


}
