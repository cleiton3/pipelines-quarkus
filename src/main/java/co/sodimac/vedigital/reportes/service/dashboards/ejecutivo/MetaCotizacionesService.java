package co.sodimac.vedigital.reportes.service.dashboards.ejecutivo;

import co.sodimac.vedigital.reportes.domain.domainsDasboards.MetaDashboardMsg;
import co.sodimac.vedigital.reportes.domain.domainsDasboards.ejecutivo.Filtros.FiltroMetaCotizaciones;
import co.sodimac.vedigital.reportes.domain.domainsDasboards.ejecutivo.MetaCotizacionesMsg;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Filters.*;

@ApplicationScoped
public class MetaCotizacionesService {
    private static final Logger LOG = Logger.getLogger(MetaCotizacionesService.class);

    @Inject
    MongoClient mongoClient;

    private MongoCollection getCollectionDashboardMetaCotizaciones() {
        LOG.infof("@getCollectionDashboardMetaCotizaciones, Conexion a la base de datos DashEjecMetaCotizaciones");
        return mongoClient.getDatabase("datosLinea").getCollection("DashEjecMetaCotizaciones");
    }

    public void actualizacionVistas(FiltroMetaCotizaciones filtro) {
        getCollectionDashboardMetaCotizaciones().drop();
        int anio = 0;
        int mes = 0;

        Calendar cal = Calendar.getInstance();
        anio = cal.get(Calendar.YEAR);
        Date fecha = new Date();
        mes = fecha.getMonth() + 1;

        LOG.infof("El año a filtrar es: " + anio);
        LOG.infof("El mes a filtrar es: " + mes);

        mongoClient.getDatabase("datosLinea").createView("DashEjecMetaCotizaciones", "CotizacionMsg", Arrays.asList(
                new Document("$lookup",
                        new Document("from","LineaMsg").
                                append("localField","contexto._id").
                                append("foreignField","_id.cotizacionId").
                                append("as","lineas")
                ),
                new Document("$project",
                        new Document("_id", new Document("ejecutivo","$meta.dataauthor").
                                append("avance","$contexto.avance").
                                append("anio","$meta.time").
                                append("mes","$meta.time")).
                                append("totalContribucion", new Document("$sum", "$lineas.data.contribucion"))
                ),
                match(ne("totalContribucion", null)),
                match(ne("totalContribucion", Float.NaN)),
                match(ne("_id.ejecutivo", null)),
                match(ne("_id.ejecutivo", "")),
                match(eq("_id.ejecutivo", filtro.getEjecutivo())),
                match(ne("_id.avance", null)),
                match(ne("_id.avance", "")),
                match(eq("_id.avance", "_8_RESULTADOS")),
                new Document("$group",
                        new Document("_id", new Document("ejecutivo", "$_id.ejecutivo").
                                append("avance", "$_id.avance").
                                append("anio", new Document("$year", "$_id.anio")).
                                append("mes", new Document("$month", "$_id.mes"))).
                                append("totalContribucion", new Document("$sum", "$totalContribucion"))
                ),
                match(eq("_id.anio", anio)),
                match(eq("_id.mes", mes)),
                match(gte("_id.totalContribucion", 0))
        ));
    }

    public MetaCotizacionesMsg getMetaCotizacionesService(FiltroMetaCotizaciones filtro) {
        LOG.infof("@getMetaCotizacionesService, Consulta a MONGO, para obtener DashEjecMetaCotizaciones");

        // Aqui crea la vista
        actualizacionVistas(filtro);

        var collection = getCollectionDashboardMetaCotizaciones();
        AggregateIterable<Document> output = collection.aggregate(Arrays.asList());

        var lista = output.into(new ArrayList<>());

        long cantidadDocumentos = collection.countDocuments();
        LOG.infof("Lista respuesta: %s", lista);
        LOG.infof("Cantidad lista respuesta: %s", cantidadDocumentos);

        return MetaCotizacionesMsg.builder().
                data(lista).
                meta(
                        MetaDashboardMsg.builder().
                                cantidadDocumentos(cantidadDocumentos)
                                .build()).
                build();
    }
}
