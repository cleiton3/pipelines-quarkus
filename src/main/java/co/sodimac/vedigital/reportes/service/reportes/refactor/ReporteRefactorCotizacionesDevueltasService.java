package co.sodimac.vedigital.reportes.service.reportes.refactor;

import co.sodimac.vedigital.reportes.domain.contribucion.FiltrosContribucion;
import co.sodimac.vedigital.reportes.domain.contribucion.ReporteContribucionMsg;
import co.sodimac.vedigital.reportes.domain.devueltas.FiltrosCotizacionesDevueltas;
import co.sodimac.vedigital.reportes.domain.devueltas.ReporteCotizacionesDevueltasMsg;
import co.sodimac.vedigital.reportes.domain.domains.MetaReporteMsg;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import io.quarkus.cache.CacheKey;
import io.quarkus.cache.CacheResult;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Sorts.*;

@ApplicationScoped
public class ReporteRefactorCotizacionesDevueltasService {
    private static final Logger LOG = Logger.getLogger(ReporteRefactorCotizacionesDevueltasService.class);

    @Inject
    MongoClient mongoClient;

    private String getColumna(String columna) {
        switch (columna) {
            case "ejecutivo":
                return "_id.ejecutivo";
            case "avance":
                return "_id.avance";
            case "referencia":
                return "_id.referencia";
            case "clienteNumeroDoc":
                return "_id.clienteNumeroDoc";
            case "idZonaTiendaCreacion":
                return "_id.idZonaTiendaCreacion";
            case "idZonaTiendaDespacho":
                return "_id.idZonaTiendaDespacho";
            case "idTiendaCreacion":
                return "_id.idTiendaCreacion";
            case "idTiendaDespacho":
                return "_id.idTiendaDespacho";
            case "vigenciaDesdeFecha":
                return "_id.vigenciaDesdeFecha";
            case "vigenciaHastaFecha":
                return "_id.vigenciaHastaFecha";
            case "numeroCotizaciones":
                return "numeroCotizaciones";
            case "totalContribucion":
                return "totalContribucion";
            case "totalMargen":
                return "totalMargen";
            case "totalVenta":
                return "totalVenta";
            default:
                return "numeroCotizaciones";
        }
    }

    /**
     * Obtiene el reporte de cotizaciones devueltas
     *
     * @param page
     * @param cantidad
     * @param orden
     * @param columna
     * @return
     */
    @CacheResult(cacheName = "reporte-devueltas-cache")
    public ReporteCotizacionesDevueltasMsg getListadoCotizacionesDevueltas(@CacheKey int page, @CacheKey int cantidad,
                                                                           @CacheKey String orden,
                                                                           @CacheKey String columna) {
        LOG.infof("@getListadoCotizacionesDevueltas, Consulta a MONGO, para obtener cotizaciones devueltas");

        MongoDatabase db = mongoClient.getDatabase("datosLinea");
        MongoCollection<Document> lineasMsg = db.getCollection("CotizacionMsg");

        columna = getColumna(columna);
        LOG.debugf("La columna es: " + columna);

        Bson sort = sort(descending(columna));
        if (orden.matches("1")) {
            sort = sort(ascending(columna));
        }
        Bson limit = limit(cantidad);
        Bson skip = skip((page - 1) * cantidad);

        Bson lookup = new Document("$lookup",
                new Document("from", "LineaMsg").
                        append("localField", "contexto._id").
                        append("foreignField", "_id.cotizacionId").
                        append("as", "lineas")
        );

        Bson project = new Document("$project",
                new Document("_id", new Document("ejecutivo", "$meta.dataauthor").
                        append("referencia", "$contexto.referencia").
                        append("idCotizacion", "$contexto._id").
                        append("estadoCotizacion", "$contexto.estado").
                        append("avance", "$contexto.avance").
                        append("idTiendaCreacion", "$data.idTiendaCreacion").
                        append("idTiendaDespacho", "$data.idTiendaDespacho").
                        append("idZonaTiendaCreacion", "$data.idZonaTiendaCreacion").
                        append("idZonaTiendaDespacho", "$data.idZonaTiendaDespacho").
                        append("subclienteConsecutivo", "$data.clienteSubcliente.subclienteConsecutivo").
                        append("subclienteNombre", "$data.clienteSubcliente.subclienteNombre").
                        append("clienteNumeroDoc", "$data.clienteSubcliente.clienteNumeroDoc").
                        append("clienteNombre", "$data.clienteSubcliente.clienteNombre").
                        append("remisionada", "$contexto.remisionada").
                        append("creacion", "$meta.time").
                        append("vigenciaDesdeFecha", "$data.vigenciaDesdeFecha").
                        append("vigenciaHastaFecha", "$data.vigenciaHastaFecha").

                        // Falta agregar la categoría de cliente, Juan se encarga de agregarlo en
                        // clienteSubcliente de CotizacionMsg
                        // append("categoriaCliente", "$data.clienteSubcliente.categoriaCliente").
                                append("lineas", "$lineas").
                                append("aprobaciones", "$lineas.data.aprobaciones")).
                        append("totalContribucion", new Document("$sum", "$lineas.data.contribucion")).
                        append("totalVenta", new Document("$sum", "$lineas.data.ventaTotal")).
                        append("totalMargen", new Document("$avg", "$lineas.data.margenPorcentaje"))
//                                .append("ultimaLineaAprobacion", new Document("$lineas.data.aprobaciones", new
//                                Document("$slice",-1)))
        );

        Bson matchNullTotalMargen = match(ne("totalMargen", null));
        Bson matchNaNTotalMargen = match(ne("totalMargen", Float.NaN));

        Bson matchNullTotalContribucion = match(ne("totalContribucion", null));
        Bson matchNaNTotalContribucion = match(ne("totalContribucion", Float.NaN));

        Bson matchNullTotalVenta = match(ne("totalVenta", null));
        Bson matchNaNTotalVenta = match(ne("totalVenta", Float.NaN));

        Bson matchEstados = match(or(eq("_id.avance", "_6_APROBACION"), eq("_id.avance", "_7_APROBADO"), eq("_id" +
                        ".avance",
                "_8_RESULTADOS")));
        Bson matchNegacion = match(eq("_id.lineas.data.aprobaciones.estadoLinea", "NEGACION_ANALISTA"));

        Bson matchFlete = match(ne("_id.lineas.data.flete", true));

        List<Document> results =
                lineasMsg.aggregate(Arrays.asList(
                        lookup,
                        project,
                        matchNullTotalMargen, matchNaNTotalMargen,
                        matchNullTotalContribucion, matchNaNTotalContribucion,
                        matchNullTotalVenta, matchNaNTotalVenta, matchEstados, matchNegacion, matchFlete
                        , sort, skip, limit
                )).into(new ArrayList<>());

        List<Document> cantidadDocumentosDevueltas = lineasMsg.aggregate(Arrays.asList(
                lookup,
                project,
                matchNullTotalMargen, matchNaNTotalMargen,
                matchNullTotalContribucion, matchNaNTotalContribucion,
                matchNullTotalVenta, matchNaNTotalVenta, matchEstados, matchNegacion, matchFlete)).into(new ArrayList<>());

        long cantidadDocumentos = cantidadDocumentosDevueltas.size();

        return ReporteCotizacionesDevueltasMsg.builder().
                data(new ArrayList<>(results)).
                meta(
                        MetaReporteMsg.builder().
                                cantidadDocumentos(cantidadDocumentos)
                                .build()).
                build();
    }


    /**
     * Devuelve listado de cotizaciones devueltas, según los filtros, si no se ingresa un filtro, devuelve la lista
     * completa
     *
     * @param page
     * @param cantidad
     * @param orden
     * @param columna
     * @param filtros
     * @return
     */
    @CacheResult(cacheName = "reporte-devueltas-filtros-cache")
    public ReporteContribucionMsg getListadoCotizacionesDevueltasFiltros(@CacheKey int page, @CacheKey int cantidad,
                                                                         @CacheKey String orden,
                                                                         @CacheKey String columna,
                                                                         @CacheKey FiltrosCotizacionesDevueltas filtros) {
        LOG.infof("@getListadoCotizacionesDevueltasFiltros, Consulta a MONGO, para obtener cotizaciones devueltas");

        MongoDatabase db = mongoClient.getDatabase("datosLinea");
        MongoCollection<Document> lineasMsg = db.getCollection("CotizacionMsg");

        List<Bson> queries = new ArrayList<>();

        columna = getColumna(columna);
        LOG.debugf("La columna es: " + columna);

        Bson sort = sort(descending(columna));
        if (orden.matches("1")) {
            sort = sort(ascending(columna));
        }

        Bson limit = limit(cantidad);
        Bson skip = skip((page - 1) * cantidad);


        Bson lookup = new Document("$lookup",
                new Document("from", "LineaMsg").
                        append("localField", "contexto._id").
                        append("foreignField", "_id.cotizacionId").
                        append("as", "lineas")
        );

        Bson project = new Document("$project",
                new Document("_id", new Document("ejecutivo", "$meta.dataauthor").
                        append("referencia", "$contexto.referencia").
                        append("idCotizacion", "$contexto._id").
                        append("estadoCotizacion", "$contexto.estado").
                        append("avance", "$contexto.avance").
                        append("idTiendaCreacion", "$data.idTiendaCreacion").
                        append("idTiendaDespacho", "$data.idTiendaDespacho").
                        append("idZonaTiendaCreacion", "$data.idZonaTiendaCreacion").
                        append("idZonaTiendaDespacho", "$data.idZonaTiendaDespacho").
                        append("subclienteConsecutivo", "$data.clienteSubcliente.subclienteConsecutivo").
                        append("subclienteNombre", "$data.clienteSubcliente.subclienteNombre").
                        append("clienteNumeroDoc", "$data.clienteSubcliente.clienteNumeroDoc").
                        append("clienteNombre", "$data.clienteSubcliente.clienteNombre").
                        append("remisionada", "$contexto.remisionada").
                        append("creacion", "$meta.time").
                        append("vigenciaDesdeFecha", "$data.vigenciaDesdeFecha").
                        append("vigenciaHastaFecha", "$data.vigenciaHastaFecha").

                        // Falta agregar la categoría de cliente, Juan se encarga de agregarlo en
                        // clienteSubcliente de CotizacionMsg
                        // append("categoriaCliente", "$data.clienteSubcliente.categoriaCliente").
                                append("lineas", "$lineas").
                                append("aprobaciones", "$lineas.data.aprobaciones")).
                        append("totalContribucion", new Document("$sum", "$lineas.data.contribucion")).
                        append("totalVenta", new Document("$sum", "$lineas.data.ventaTotal")).
                        append("totalMargen", new Document("$avg", "$lineas.data.margenPorcentaje"))
//                                .append("ultimaLineaAprobacion", new Document("$lineas.data.aprobaciones", new
//                                Document("$slice",-1)))
        );

        Bson matchNullTotalMargen = match(ne("totalMargen", null));
        Bson matchNaNTotalMargen = match(ne("totalMargen", Float.NaN));

        Bson matchNullTotalContribucion = match(ne("totalContribucion", null));
        Bson matchNaNTotalContribucion = match(ne("totalContribucion", Float.NaN));

        Bson matchNullTotalVenta = match(ne("totalVenta", null));
        Bson matchNaNTotalVenta = match(ne("totalVenta", Float.NaN));

        Bson matchEstados = match(or(eq("_id.avance", "_6_APROBACION"), eq("_id.avance", "_7_APROBADO"), eq("_id" +
                        ".avance",
                "_8_RESULTADOS")));
        Bson matchNegacion = match(eq("_id.lineas.data.aprobaciones.estadoLinea", "NEGACION_ANALISTA"));

        Bson matchFlete = match(ne("_id.lineas.data.flete", true));

        queries.add(lookup);
        queries.add(project);
        queries.add(matchNullTotalMargen);
        queries.add(matchNaNTotalMargen);

        queries.add(matchNullTotalContribucion);
        queries.add(matchNaNTotalContribucion);

        queries.add(matchNullTotalVenta);
        queries.add(matchNaNTotalVenta);

        queries.add(matchEstados);
        queries.add(matchNegacion);
        queries.add(matchFlete);


        // Filtros

        if(filtros.getCotizaciones() != null && !filtros.getCotizaciones().isEmpty()){
            queries.add(match(
                    or(
                            in("_id.idCotizacion", filtros.getCotizaciones()),
                            in("_id.referencia", filtros.getCotizaciones())
                    )));
        }
        if(queries.isEmpty()){
            if (filtros.getEjecutivo() != null && !filtros.getEjecutivo().isEmpty()) {
                queries.add(match(in("_id.ejecutivo", filtros.getEjecutivo())));
            }
            if (filtros.getEstado() != null && !filtros.getEstado().isEmpty()) {
                queries.add(match(
                        in("_id.estadoCotizacion", filtros.getEstado()))
                );
            }
            //aqui
            if (filtros.getAvances() != null && !filtros.getAvances().isEmpty()) {
                queries.add(match(
                        in("_id.avance", filtros.getAvances()))
                );
            }
            if (filtros.getTiendas() != null && !filtros.getTiendas().isEmpty()) {
                queries.add(match(
                        or(
                                in("_id.idTiendaCreacion",filtros.getTiendas()),
                                in("_id.idTiendaDespacho", filtros.getTiendas())
                        ))
                );
            }
            if (filtros.getZonas() != null && !filtros.getZonas().isEmpty()) {
                queries.add(match(
                        or(
                                in("_id.idZonaTiendaCreacion",filtros.getZonas()),
                                in("_id.idZonaTiendaDespacho", filtros.getZonas())
                        ))
                );
            }
            if (filtros.getFechaVigencia() != null) {
                queries.add(match(
                        and(
                                gte("_id.vigenciaDesdeFecha", filtros.getFechaVigencia().getFechaInicial()),
                                lte("_id.vigenciaHastaFecha", filtros.getFechaVigencia().getFechaFinal())
                        ))
                );
            }
            if (filtros.getFechaCreacion() != null) {
                queries.add(match(
                        and(
                                gte("_id.creacion", filtros.getFechaCreacion().getFechaInicial()),
                                lte("_id.creacion", filtros.getFechaCreacion().getFechaFinal())
                        ))
                );
            }
            if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals("igual")) {
                queries.add(match(in("totalVenta", filtros.getTotal())));
            }
            if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals(
                    "mayor")) {
                queries.add(match(gte("totalVenta", filtros.getTotal())));
            }
            if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals(
                    "menor")) {
                queries.add(match(lte("totalVenta", filtros.getTotal())));
            }
            if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals(
                    "rango")) {
                queries.add(match(
                        and(
                                gte("totalVenta", filtros.getTotalRango().getRangoTotalMin()),
                                lte("totalVenta", filtros.getTotalRango().getRangoTotalMax())
                        )
                ));
            }
            if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals("igual")) {
                queries.add(match(in("totalContribucion", filtros.getTotalContribucion())));
            }
            if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals(
                    "mayor")) {
                queries.add(match(gte("totalContribucion", filtros.getTotalContribucion())));
            }
            if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals(
                    "menor")) {
                queries.add(match(lte("totalContribucion", filtros.getTotalContribucion())));
            }
            if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals(
                    "rango")) {
                queries.add(match(
                        and(
                                gte("totalContribucion", filtros.getContribucionRango().getContribucionMin()),
                                lte("totalContribucion", filtros.getContribucionRango().getContribucionMax())
                        ))
                );
            }
            if (filtros.getTipoTotalMargen() != null && !filtros.getTipoTotalMargen().isEmpty() && filtros.getTipoTotalMargen().equals(
                    "rango")) {
                queries.add(match(
                        and(
                                gte("totalMargen", filtros.getMargenRango().getMargenMin()),
                                lte("totalMargen", filtros.getMargenRango().getMargenMax())
                        ))
                );
            }
        }

        queries.add(sort);
        queries.add(skip);
        queries.add(limit);


        // Data
        List<Document> results = lineasMsg.aggregate(queries).into(new ArrayList<>());

        // Contador de documentos
        // se elimina limit
        queries.remove(queries.size() - 1);
        // se elimina skip
        queries.remove(queries.size() - 1);
        // se elimina sort
        queries.remove(queries.size() - 1);

        List<Document> resultsContador = lineasMsg.aggregate(queries).into(new ArrayList<>());
        long cantidadDocumentos = resultsContador.size();

        return ReporteContribucionMsg.builder().
                data(new ArrayList<>(results)).
                meta(
                        MetaReporteMsg.builder().
                                cantidadDocumentos(cantidadDocumentos)
                                .build()).
                build();
    }
}
