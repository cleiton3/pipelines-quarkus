package co.sodimac.vedigital.reportes.service.reportes;

import co.sodimac.vedigital.reportes.domain.domains.MetaReporteMsg;

import co.sodimac.vedigital.reportes.domain.contribucion.FiltrosContribucion;
import co.sodimac.vedigital.reportes.domain.contribucion.ReporteContribucionMsg;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;

import io.quarkus.cache.CacheKey;
import io.quarkus.cache.CacheResult;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Sorts.*;
import static com.mongodb.client.model.Filters.*;

@ApplicationScoped
public class ReporteContribucionService {

    private static final Logger LOG = Logger.getLogger(ReporteContribucionService.class);

    @Inject
    MongoClient mongoClient;

    private MongoCollection getCollectionReporteContribucion() {
        LOG.infof("@getCollectionCotizacion, Conexion a la base de datos ReporteContribucion");
        return mongoClient.getDatabase("datosLinea").getCollection("ReporteContribucion");
    }

    public void actualizacionVistas(){
        getCollectionReporteContribucion().drop();
        mongoClient.getDatabase("datosLinea").createView("ReporteContribucion","CotizacionMsg", Arrays.asList(
                new Document("$lookup",
                        new Document("from","LineaMsg").
                                append("localField","contexto._id").
                                append("foreignField","_id.cotizacionId").
                                append("as","lineas")
                ),
                new Document("$project",
                        new Document("_id", new Document("ejecutivo","$meta.dataauthor").
                                append("avance","$contexto.avance").
                                append("tiendaCreacion","$data.idTiendaCreacion").
                                append("tiendaDespacho","$data.idTiendaDespacho").
                                append("zonaCreacion","$data.idZonaTiendaCreacion").
                                append("fechaCreacion","$meta.time").
                                append("vigenciaDesdeFecha","$data.vigenciaDesdeFecha").
                                append("vigenciaHastaFecha","$data.vigenciaHastaFecha").
                                append("zonaDespacho","$data.idZonaTiendaDespacho")).
                                append("totalContribucion", new Document("$sum", "$lineas.data.contribucion")).
                                append("totalVenta", new Document("$sum", "$lineas.data.ventaTotal")).
                                append("totalMargen", new Document("$avg", "$lineas.data.margenPorcentaje"))
                ),
                match(ne("totalMargen", null)),
                match(ne("totalMargen", Float.NaN)),
                match(ne("totalContribucion", null)),
                match(ne("totalContribucion", Float.NaN)),
                match(ne("totalVenta", null)),
                match(ne("totalVenta", Float.NaN)),
                new Document("$group",
                        new Document("_id", new Document("ejecutivo","$_id.ejecutivo").
                                append("avance","$_id.avance").
                                append("tiendaCreacion","$_id.tiendaCreacion").
                                append("tiendaDespacho","$_id.tiendaDespacho").
                                append("zonaCreacion","$_id.zonaCreacion").
                                append("fechaCreacion","$_id.fechaCreacion").
                                append("vigenciaDesdeFecha","$_id.vigenciaDesdeFecha").
                                append("vigenciaHastaFecha","$_id.vigenciaHastaFecha").
                                append("zonaDespacho","$_id.zonaDespacho")).
                                append("numeroCotizaciones", new Document("$sum", 1)).
                                append("totalContribucion", new Document("$sum", "$totalContribucion")).
                                append("totalVenta", new Document("$sum", "$totalVenta")).
                                append("totalMargen", new Document("$avg", "$totalMargen"))
                )
        ));
    }

    // Listados
    private String getColumna(String columna) {
        switch (columna) {
            case "ejecutivo":
                return "_id.ejecutivo";
            case "avance":
                return "_id.avance";
            case "tiendaCreacion":
                return "_id.tiendaCreacion";
            case "tiendaDespacho":
                return "_id.tiendaDespacho";
            case "zonaCreacion":
                return "_id.zonaCreacion";
            case "fechaCreacion":
                return "_id.fechaCreacion";
            case "vigenciaDesdeFecha":
                return "_id.vigenciaDesdeFecha";
            case "vigenciaHastaFecha":
                return "_id.vigenciaHastaFecha";
            case "zonaDespacho":
                return "_id.zonaDespacho";
            case "numeroCotizaciones":
                return "numeroCotizaciones";
            case "totalContribucion":
                return "totalContribucion";
            case "totalVenta":
                return "totalVenta";
            case "totalMargen":
                return "totalMargen";
            default:
                return "totalContribucion";
        }
    }


    /**
     * Obtiene el reporte de contribución y margen
     *
     * @param page
     * @param cantidad
     * @param orden
     * @param columna
     * @return
     */
    @CacheResult(cacheName = "reporte-contribucion-cache")
    public ReporteContribucionMsg getListadoContribucion(@CacheKey int page, @CacheKey int cantidad,
                                                         @CacheKey String orden,
                                                         @CacheKey String columna) {
        LOG.infof("@getListadoTopSku, Consulta a MONGO, para obtener top sku");

        columna = getColumna(columna);
        LOG.debugf("getListadoContribucion La columna es: %s", columna);

        Bson direction = descending(columna);
        if (orden.matches("1")) {
            direction = ascending(columna);
        }

        // Aqui crea la vista
        actualizacionVistas();

        var collection = getCollectionReporteContribucion();
        AggregateIterable<Document> output = collection.
                aggregate(Arrays.asList(
                        sort(orderBy(direction)),
                        skip((page - 1) * cantidad),
                        limit(cantidad)
                ));

        ArrayList<Document> listaContribucion = output.into(new ArrayList<>());
        long cantidadDocumentos = collection.countDocuments();

        LOG.infof("Lista respuesta: %s", listaContribucion);
        LOG.infof("Cantidad lista respuesta: %s", cantidadDocumentos);

        return ReporteContribucionMsg.builder().
                data(listaContribucion).
                meta(
                        MetaReporteMsg.builder().
                                cantidadDocumentos(cantidadDocumentos)
                                .build()).
                build();
    }

    /**
     * Devuelve listado de contribución y margen, según los filtros, si no se ingresa un filtro, devuelve la lista
     * completa
     *
     * @param page
     * @param cantidad
     * @param orden
     * @param columna
     * @param filtros
     * @return
     */
    @CacheResult(cacheName = "reporte-top-cache")
    public ReporteContribucionMsg getListadoContribucionFiltros(@CacheKey int page, @CacheKey int cantidad,
                                                                @CacheKey String orden,
                                                                @CacheKey String columna,
                                                                @CacheKey FiltrosContribucion filtros) {
        LOG.infof("@getListadoTopSkuFiltros, Consulta a MONGO, para obtener top sku");

        var collection = getCollectionReporteContribucion();
        List<Bson> queries = new ArrayList<>();
        List<Bson> stepsAggregate = new ArrayList<>();

        columna = getColumna(columna);

        Bson direction = descending(columna);
        if (orden.matches("1")) {
            direction = ascending(columna);
        }

        if (filtros.getEjecutivo() != null && !filtros.getEjecutivo().isEmpty()) {
            queries.add(in("_id.ejecutivo", filtros.getEjecutivo()));
        }
        if (filtros.getAvance() != null && !filtros.getAvance().isEmpty()) {
            queries.add(
                    in("_id.avance", filtros.getAvance())
            );
        }
        if (filtros.getFechaCreacion() != null) {
            queries.add(
                    and(
                            gte("_id.fechaCreacion", filtros.getFechaCreacion().getFechaInicial()),
                            lte("_id.fechaCreacion", filtros.getFechaCreacion().getFechaFinal())
                    )
            );
        }
        if (filtros.getFechaVigencia() != null) {
            queries.add(
                    and(
                            gte("_id.vigenciaDesdeFecha", filtros.getFechaVigencia().getFechaInicial()),
                            lte("_id.vigenciaHastaFecha", filtros.getFechaVigencia().getFechaFinal())
                    )
            );
        }
        if (filtros.getZonas() != null && !filtros.getZonas().isEmpty()) {
            queries.add(
                    or(
                            in("_id.zonaCreacion",filtros.getZonas()),
                            in("_id.zonaDespacho", filtros.getZonas())
                    )
            );
        }

        if (filtros.getTiendas() != null && !filtros.getTiendas().isEmpty()) {
            queries.add(
                    or(
                            in("_id.tiendaCreacion",filtros.getTiendas()),
                            in("_id.tiendaDespacho", filtros.getTiendas())
                    )
            );
        }

        if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals("igual")) {
            queries.add(in("totalVenta", filtros.getTotal()));
        }
        if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals(
                "mayor")) {
            queries.add(gte("totalVenta", filtros.getTotal()));
        }
        if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals(
                "menor")) {
            queries.add(lte("totalVenta", filtros.getTotal()));
        }
        if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals(
                "rango")) {
            queries.add(
                    and(
                            gte("totalVenta", filtros.getTotalRango().getRangoTotalMin()),
                            lte("totalVenta", filtros.getTotalRango().getRangoTotalMax())
                    )
            );
        }
        if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals("igual")) {
            queries.add(in("totalContribucion", filtros.getTotalContribucion()));
        }
        if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals(
                "mayor")) {
            queries.add(gte("totalContribucion", filtros.getTotalContribucion()));
        }
        if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals(
                "menor")) {
            queries.add(lte("totalContribucion", filtros.getTotalContribucion()));
        }
        if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals(
                "rango")) {
            queries.add(
                    and(
                            gte("totalContribucion", filtros.getContribucionRango().getContribucionMin()),
                            lte("totalContribucion", filtros.getContribucionRango().getContribucionMax())
                    )
            );
        }
        if (filtros.getTipoTotalMargen() != null && !filtros.getTipoTotalMargen().isEmpty() && filtros.getTipoTotalMargen().equals(
                "rango")) {
            queries.add(
                    and(
                            gte("totalMargen", filtros.getMargenRango().getMargenMin()),
                            lte("totalMargen", filtros.getMargenRango().getMargenMax())
                    )
            );
        }
        if (!queries.isEmpty()) {
            stepsAggregate.add(match(and(queries)));
        }

        //Cuenta la cantidad de documentos
        stepsAggregate.add(count());
        AggregateIterable<Document> countAggregateIterable = collection.aggregate(stepsAggregate);

        var countList = countAggregateIterable.into(new ArrayList<>());
        var cantidadDocumentos = 0;
        if (countList != null && !countList.isEmpty()) {
            cantidadDocumentos = (Integer) countList.get(0).get("count");
        }

        //Consulta los documentos que cumplen con el filtro
        stepsAggregate.remove(stepsAggregate.size() - 1);
        stepsAggregate.add(sort(orderBy(direction)));
        stepsAggregate.add(skip((page - 1) * cantidad));
        stepsAggregate.add(limit(cantidad));
        AggregateIterable<Document> output = collection.aggregate(stepsAggregate);

        var listaTop = output.into(new ArrayList<>());

        LOG.infof("Lista respuesta: %s", listaTop);
        LOG.infof("Cantidad lista respuesta: %s", cantidadDocumentos);
        return ReporteContribucionMsg.builder().
                data(listaTop).
                meta(
                        MetaReporteMsg.builder().
                                cantidadDocumentos(cantidadDocumentos)
                                .build()).
                build();
    }
}