package co.sodimac.vedigital.reportes.service.dashboards.ejecutivo;

import co.sodimac.vedigital.reportes.domain.domainsDasboards.MetaDashboardMsg;
import co.sodimac.vedigital.reportes.domain.domainsDasboards.ejecutivo.ResDashboardsMsg;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Sorts.descending;
import static com.mongodb.client.model.Sorts.orderBy;

@ApplicationScoped
public class TopEjecutivosService {

    private static final Logger LOG = Logger.getLogger(TopEjecutivosService.class);
    @Inject
    MongoClient mongoClient;

    private MongoCollection getCollectionTopEjecutivosTienda() {
        LOG.infof("@getCollectionTopEjecutivosTienda, Conexion a la base de datos DashTopEjecutivosTienda");
        return mongoClient.getDatabase("datosLinea").getCollection("DashTopEjecutivosTienda");
    }

    public void actualizacionVistas(){
        getCollectionTopEjecutivosTienda().drop();
        mongoClient.getDatabase("datosLinea").createView("DashTopEjecutivosTienda","CotizacionMsg", Arrays.asList(
                new Document("$group",
                        new Document("_id", new Document("ejecutivo", "$meta.dataauthor").
                                append("tienda", "$data.idTiendaCreacion")).
                                append("cantidad", new Document("$sum", 1))
                )
        ));
    }


    public ResDashboardsMsg getTopEjecutivos(String tienda) {
        LOG.infof("@getTotalCotizacionesService, Consulta a MONGO, para obtener TopEjecutivosTienda");

        actualizacionVistas();

        var collection = getCollectionTopEjecutivosTienda();

        String columna = "cantidad";
        Integer cantidad = 10;


        Bson direction = descending(columna);

        List<Bson> queries = new ArrayList<>();
        List<Bson> stepsAggregate = new ArrayList<>();

        if (tienda != null && !tienda.equals("")) {
            queries.add(in("_id.tienda", tienda.toString()));
        }

        if (!queries.isEmpty()) {
            stepsAggregate.add(match(and(queries)));
        }

        //Cuenta la cantidad de documentos
        stepsAggregate.add(count());
        AggregateIterable<Document> countAggregateIterable = collection.aggregate(stepsAggregate);

        var countList = countAggregateIterable.into(new ArrayList<>());
        var cantidadDocumentos = 0;
        if (countList != null && !countList.isEmpty()) {
            cantidadDocumentos = (Integer) countList.get(0).get("count");
        }

        //Consulta los documentos que cumplen con el filtro
        stepsAggregate.remove(stepsAggregate.size() - 1);
        stepsAggregate.add(sort(orderBy(direction)));
        stepsAggregate.add(limit(cantidad));
        AggregateIterable<Document> output = collection.aggregate(stepsAggregate);

        var listaTop = output.into(new ArrayList<>());
        LOG.infof("Lista respuesta: %s", listaTop);
        LOG.infof("Cantidad lista respuesta: %s", cantidadDocumentos);

        return ResDashboardsMsg.builder().
                data(listaTop).
                meta(
                        MetaDashboardMsg.builder().
                                cantidadDocumentos(cantidadDocumentos)
                                .build()).
                build();
    }


}
