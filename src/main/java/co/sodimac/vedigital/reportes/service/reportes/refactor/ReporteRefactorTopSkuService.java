package co.sodimac.vedigital.reportes.service.reportes.refactor;

import co.sodimac.vedigital.reportes.domain.domains.MetaReporteMsg;
import co.sodimac.vedigital.reportes.domain.topsku.FiltrosTopSku;
import co.sodimac.vedigital.reportes.domain.topsku.ReporteTopSkuMsg;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import io.quarkus.cache.CacheKey;
import io.quarkus.cache.CacheResult;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;

import static com.mongodb.client.model.Accumulators.*;
import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Aggregates.project;
import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Projections.include;
import static com.mongodb.client.model.Sorts.*;

import com.mongodb.client.MongoDatabase;

import java.util.List;


import static com.mongodb.client.model.Projections.*;
import static com.mongodb.client.model.Sorts.descending;

@ApplicationScoped
public class ReporteRefactorTopSkuService {

    private static final Logger LOG = Logger.getLogger(ReporteRefactorTopSkuService.class);

    @Inject
    MongoClient mongoClient;

    private String getColumna(String columna) {
        switch (columna) {
            case "sku":
                return "producto._id.sku";
            case "descripcion":
                return "producto._id.descripcion";
            case "proveedor":
                return "producto._id.proveedor";
            case "departamento":
                return "producto._id.jerarquia.departmentName";
            case "familia":
                return "producto._id.jerarquia.familyName";
            case "subfamilia":
                return "producto._id.jerarquia.subfamilyName";
            case "grupo":
                return "producto._id.jerarquia.groupName";
            case "conjunto":
                return "producto._id.jerarquia.conjunctName";
            case "tienda":
                return "producto._id.tienda";
            case "totalVenta":
                return "totalVenta";
            case "totalMargen":
                return "totalMargen";
            case "totalContribucion":
                return "totalContribucion";
            case "cantidad":
                return "cantidad";
            default:
                return "cantidad";
        }
    }

    /**
     * Obtiene el reporte de top sku
     *
     * @param page
     * @param cantidad
     * @param orden
     * @param columna
     * @return
     */
    @CacheResult(cacheName = "reporte-top-cache")
    public ReporteTopSkuMsg getListadoTopSku(@CacheKey int page, @CacheKey int cantidad, @CacheKey String orden,
                                             @CacheKey String columna) {
        LOG.infof("@getCotizacionesPendientes, Consulta a MONGO, para obtener top sku ");

        MongoDatabase db = mongoClient.getDatabase("datosLinea");
        MongoCollection<Document> lineasMsg = db.getCollection("LineaMsg");

        columna = getColumna(columna);
        LOG.debugf("La columna es: " + columna);

        Bson sort = sort(descending(columna));
        if (orden.matches("1")) {
            sort = sort(ascending(columna));
        }
        Bson limit = limit(cantidad);
        Bson skip = skip((page - 1) * cantidad);


        Bson matchNotTotalMargen = match(ne("totalMargen", 0));
        Bson matchNullTotalMargen = match(ne("totalMargen", null));
        Bson matchNaNTotalMargen = match(ne("totalMargen", Float.NaN));

        Bson matchNotTotalContribucion = match(ne("totalContribucion", 0));
        Bson matchNullTotalContribucion = match(ne("totalContribucion", null));
        Bson matchNaNTotalContribucion = match(ne("totalContribucion", Float.NaN));


        Bson matchNotTotalVenta = match(ne("totalVenta", 0));
        Bson matchNullTotalVenta = match(ne("totalVenta", null));
        Bson matchNaNTotalVenta = match(ne("totalVenta", Float.NaN));

        Bson matchNotFlete = match(ne("producto._id.flete", true));

        Bson group = group(new Document("_id", new Document("sku", "$data.sku").
                        append("descripcion", "$data.descripcion").
                        append("proveedor", "$data.proveedor").
                        append("proveedores", "$data.proveedores").
                        append("jerarquia", "$data.jerarquia").
                        append("tienda", "$data.tiendaDespacho").
//                        append("fecha", "$meta.time").
                        append("flete", "$data.esFlete")
                )
                , sum("totalVenta", "$data.ventaTotal")
                , sum("totalContribucion", "$data.contribucion")
                , avg("totalMargen", "$data.margenPorcentaje")
                , sum("cantidad", new Document("$sum", 1)));

        Bson project = project(fields(excludeId(), computed("producto", "$_id"),
                include("cantidad", "totalVenta", "totalContribucion", "totalMargen")));

        List<Document> results =
                lineasMsg.aggregate(Arrays.asList(
                        group,
                        matchNotTotalMargen, matchNullTotalMargen, matchNaNTotalMargen,
                        matchNotTotalContribucion, matchNullTotalContribucion, matchNaNTotalContribucion,
                        matchNotTotalVenta, matchNullTotalVenta, matchNaNTotalVenta,
                        matchNotFlete
                        , project
                        , sort, skip, limit
                )).into(new ArrayList<>());

        List<Document> cantidadDocumentosTopSku = lineasMsg.aggregate(Arrays.asList(
                group, project,
                matchNotTotalMargen, matchNullTotalMargen, matchNaNTotalMargen,
                matchNotTotalContribucion, matchNullTotalContribucion, matchNaNTotalContribucion,
                matchNotTotalVenta, matchNullTotalVenta, matchNaNTotalVenta, matchNotFlete)).into(new ArrayList<>());

        long cantidadDocumentos = cantidadDocumentosTopSku.size();

        return ReporteTopSkuMsg.builder().
                data(new ArrayList<>(results)).
                meta(
                        MetaReporteMsg.builder().
                                cantidadDocumentos(cantidadDocumentos)
                                .build()).
                build();
    }


    /**
     * Devuelve listado de top sku, según los filtros, si no se ingresa un filtro, devuelve la lista completa
     *
     * @param page
     * @param cantidad
     * @param orden
     * @param columna
     * @param filtros
     * @return
     */
    @CacheResult(cacheName = "reporte-top-filtros-cache")
    public ReporteTopSkuMsg getListadoTopSkuFiltros(@CacheKey int page, @CacheKey int cantidad,
                                                    @CacheKey String orden, @CacheKey String columna,
                                                    @CacheKey FiltrosTopSku filtros) {
        LOG.infof("@getListadoTopSkuFiltros, Consulta a MONGO, para obtener top sku");

        MongoDatabase db = mongoClient.getDatabase("datosLinea");
        MongoCollection<Document> lineasMsg = db.getCollection("LineaMsg");

        List<Bson> queries = new ArrayList<>();

        columna = getColumna(columna);
        LOG.debugf("La columna es: " + columna);

        Bson sort = sort(descending(columna));
        if (orden.matches("1")) {
            sort = sort(ascending(columna));
        }

        Bson limit = limit(cantidad);
        Bson skip = skip((page - 1) * cantidad);


        Bson matchNotTotalMargen = match(ne("totalMargen", 0));
        Bson matchNullTotalMargen = match(ne("totalMargen", null));
        Bson matchNaNTotalMargen = match(ne("totalMargen", Float.NaN));

        Bson matchNotTotalContribucion = match(ne("totalContribucion", 0));
        Bson matchNullTotalContribucion = match(ne("totalContribucion", null));
        Bson matchNaNTotalContribucion = match(ne("totalContribucion", Float.NaN));


        Bson matchNotTotalVenta = match(ne("totalVenta", 0));
        Bson matchNullTotalVenta = match(ne("totalVenta", null));
        Bson matchNaNTotalVenta = match(ne("totalVenta", Float.NaN));

        Bson matchNotFlete = match(ne("producto._id.flete", true));

        Bson group = group(new Document("_id", new Document("sku", "$data.sku").
                        append("descripcion", "$data.descripcion").
                        append("proveedor", "$data.proveedor").
                        append("proveedores", "$data.proveedores").
                        append("jerarquia", "$data.jerarquia").
                        append("tienda", "$data.tiendaDespacho").
//                        append("fecha", "$meta.time").
                        append("flete", "$data.esFlete")
                )
                , sum("totalVenta", "$data.ventaTotal")
                , sum("totalContribucion", "$data.contribucion")
                , avg("totalMargen", "$data.margenPorcentaje")
                , sum("cantidad", new Document("$sum", 1)));

        Bson project = project(fields(excludeId(), computed("producto", "$_id"),
                include("cantidad", "totalVenta", "totalContribucion", "totalMargen")));

        queries.add(group);

        queries.add(matchNotTotalMargen);
        queries.add(matchNullTotalMargen);
        queries.add(matchNaNTotalMargen);

        queries.add(matchNotTotalContribucion);
        queries.add(matchNullTotalContribucion);
        queries.add(matchNaNTotalContribucion);

        queries.add(matchNotTotalVenta);
        queries.add(matchNullTotalVenta);
        queries.add(matchNaNTotalVenta);

        queries.add(matchNotFlete);

        queries.add(project);


        // Filtros

        if (filtros.getTiendas() != null && !filtros.getTiendas().isEmpty()) {
            queries.add(match(in("producto._id.tienda", filtros.getTiendas())));
        }
        if (filtros.getFamilias() != null && !filtros.getFamilias().isEmpty()) {
            queries.add(
                    match(in("producto._id.jerarquia.familia", filtros.getFamilias()))
            );
        }
//            if (filtros.getFecha() != null) {
//                queries.add(match(
//                        and(
//                                gte("producto._id.fecha", filtros.getFecha().getFechaInicial()),
//                                lte("producto._id.fecha", filtros.getFecha().getFechaFinal())
//                        )
//                ));
//            }
        if (filtros.getProductos() != null && !filtros.getProductos().isEmpty()) {
            queries.add(match(in("producto._id.sku", filtros.getProductos())));
        }

        if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals(
                "igual")) {
            queries.add(match(in("totalVenta", filtros.getTotal())));
        }
        if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals(
                "mayor")) {
            queries.add(match(gte("totalVenta", filtros.getTotal())));
        }
        if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals(
                "menor")) {
            queries.add(match(lte("totalVenta", filtros.getTotal())));
        }
        if (filtros.getTipoTotal() != null && !filtros.getTipoTotal().isEmpty() && filtros.getTipoTotal().equals(
                "rango")) {
            queries.add(match(
                    and(
                            gte("totalVenta", filtros.getTotalRango().getRangoTotalMin()),
                            lte("totalVenta", filtros.getTotalRango().getRangoTotalMax())
                    ))
            );
        }
        if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals("igual")) {
            queries.add(match(in("totalContribucion", filtros.getTotalContribucion())));
        }
        if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals(
                "mayor")) {
            queries.add(match(gte("totalContribucion", filtros.getTotalContribucion())));
        }
        if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals(
                "menor")) {
            queries.add(match(lte("totalContribucion", filtros.getTotalContribucion())));
        }
        if (filtros.getTipoTotalContribucion() != null && !filtros.getTipoTotalContribucion().isEmpty() && filtros.getTipoTotalContribucion().equals(
                "rango")) {
            queries.add(match(
                    and(
                            gte("totalContribucion", filtros.getContribucionRango().getContribucionMin()),
                            lte("totalContribucion", filtros.getContribucionRango().getContribucionMax())
                    ))
            );
        }
        if (filtros.getTipoTotalMargen() != null && !filtros.getTipoTotalMargen().isEmpty() && filtros.getTipoTotalMargen().equals(
                "rango")) {
            queries.add(match(
                    and(
                            gte("totalMargen", filtros.getMargenRango().getMargenMin()),
                            lte("totalMargen", filtros.getMargenRango().getMargenMax())
                    ))
            );
        }
        if (filtros.getProveedores() != null && !filtros.getProveedores().isEmpty()) {
            queries.add(match(
                    in("producto._id.proveedor", filtros.getProveedores()))
            );
        }

        queries.add(sort);
        queries.add(skip);
        queries.add(limit);

        // Data
        List<Document> results = lineasMsg.aggregate(queries).into(new ArrayList<>());

        // Contador de documentos
        // se elimina limit
        queries.remove(queries.size() - 1);
        // se elimina skip
        queries.remove(queries.size() - 1);
        // se elimina sort
        queries.remove(queries.size() - 1);

        List<Document> resultsContador = lineasMsg.aggregate(queries).into(new ArrayList<>());
        long cantidadDocumentos = resultsContador.size();

        return ReporteTopSkuMsg.builder().
                data(new ArrayList<>(results)).
                meta(
                        MetaReporteMsg.builder().
                                cantidadDocumentos(cantidadDocumentos)
                                .build()).
                build();
    }
}
