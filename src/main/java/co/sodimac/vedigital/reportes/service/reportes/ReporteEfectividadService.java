package co.sodimac.vedigital.reportes.service.reportes;

import co.sodimac.vedigital.reportes.domain.domains.MetaReporteMsg;
import co.sodimac.vedigital.reportes.domain.efectividad.RespuestaEfectividad;
import co.sodimac.vedigital.reportes.domain.efectividad.FiltrosEfectividad;
import co.sodimac.vedigital.reportes.domain.efectividad.ReporteEfectividadMsg;
import io.agroal.api.AgroalDataSource;
import io.quarkus.agroal.DataSource;
import io.quarkus.cache.CacheKey;
import io.quarkus.cache.CacheResult;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@ApplicationScoped
public class ReporteEfectividadService {

    private static final String CONSULTA_NOMBRE_CLIENTE = "SELECT C.NOMBRE FROM SODIMAC_C1_CLIENTE C WHERE CONCAT(C" +
            ".IDENTIFICACION, C.CONS_SUBC) = ?";

    private static final String CONSULTA_EJECUTIVO_NOMBRE = "SELECT CEDULA, NOMBRES, APELLIDOS FROM EMPLEADO WHERE " +
            "CEDULA = ?";

    private static final String CONSULTA_EFECTIVIDAD_CONTADOR = "SELECT COUNT(*) AS TOTAL FROM (SELECT " +
            "DISTINCT Q" +
            ".STOREID AS CODIGO_TIENDA,\n" +
            "Q.CREATIONDATE,\n" +
            "TIE.HOMOLOGACION_RPA AS DESCRIPCION_TIENDA,\n" +
            "Q.NUMBER_ NUMERO_COTIZACION,\n" +
            "N.NUMBER_ AS NUMERO_NEGOCIACION,\n" +
            "O.STICKERCODE AS STICKER_NOTA_PEDIDO,\n" +
            "E.NOMBRE AS ESTADO_NP,\n" +
            "DECODE(DET.NUMEROOC, 0, OCPR.ID_ORDEN_COMPRA, DET.NUMEROOC) AS OC,\n" +
            "TRIM(EOC.PMG_STAT_NAME) AS ESTADO_OC,\n" +
            "TDEV.FECHA_INICIO AS FECHA_DEVOLUCION,\n" +
            "DDEV.NUMERO_DEVOLUCION AS NUMERO_DEVOLUCION,\n" +
            "O.NUMBER_ CONSECUTIVO_NP,\n" +
            "DT.DATA1 FACTURA,\n" +
            "Q.ZONEID AS ZONAID,\n" +
            "Q.CREATEDBY AS IDEJECUTIVO,\n" +
            "Q.CUSTOMERCODE AS CLIENTESUBCLIENTE\n" +
            "FROM   FIDELIZACION2.QUOTATION Q\n" +
            "LEFT   JOIN FIDELIZACION2.NEGOTIATION N\n" +
            "ON     N.QUOTATIONID = Q.ID\n" +
            "LEFT   JOIN UNIPROD.SODIMAC_C1_TIENDAS_SFC@PROD.HOMECENTER.COM.CO TIE\n" +
            "ON     TIE.CODIGO_TIENDA = Q.STOREID\n" +
            "LEFT   JOIN FIDELIZACION2.ORDER_ O\n" +
            "ON     O.QUOTATIONID = Q.ID\n" +
            "LEFT   JOIN FIDELIZACION2.COSTO CO\n" +
            "ON     CO.IDCOTIZACION = Q. ID\n" +
            "AND CO.STICKERNP = SUBSTR(O.STICKERCODE, 1, 12)\n" +
            "LEFT   JOIN FIDELIZACION2.DETALLECOSTO DET\n" +
            "ON     CO.ID = DET.IDCOSTO\n" +
            "LEFT   JOIN SAPS.NPEDIDO NP\n" +
            "ON     NP.STICKER = SUBSTR(O.STICKERCODE, 1, 12)\n" +
            "AND NP.ID_ALMACEN = O.STOREID\n" +
            "LEFT   JOIN SAPS.ESTADO E\n" +
            "ON     E.ID_ESTADO = NP.ID_ESTADO\n" +
            "LEFT   JOIN SAPS.ORDEN_COMPRA OCPR\n" +
            "ON     (OCPR.STICKER = NP.STICKER AND OCPR.ID_ALMACEN = NP.ID_ALMACEN)\n" +
            "LEFT   JOIN UNIPROD.PMGHDREE@PROD.HOMECENTER.COM.CO POC\n" +
            "ON     ((POC.PMG_PO_NUMBER = DET.NUMEROOC) OR (POC.PMG_PO_NUMBER = OCPR.ID_ORDEN_COMPRA))\n" +
            "LEFT   JOIN UNIPROD.PMGSTSCD@PROD.HOMECENTER.COM.CO EOC\n" +
            "ON     EOC.PMG_STAT_CODE = POC.PMG_STAT_CODE\n" +
            "LEFT   JOIN DEVOLUCIONES.DETALLE_TIRILLA DTR\n" +
            "ON     DTR.STICKER_NP = NP.STICKER\n" +
            "LEFT   JOIN DEVOLUCIONES.DETALLE_DEVOLUCION DDEV\n" +
            "ON     DDEV.NUMERO_FISCAL = DTR.NUMERO_FISCAL\n" +
            "AND DTR.PRD_LVL_NUMBER = DET.IDPRODUCTO\n" +
            "LEFT   JOIN DEVOLUCIONES.TIEMPO_DEVOLUCION TDEV\n" +
            "ON     TDEV.NUMERO_DEVOLUCION = DDEV.NUMERO_DEVOLUCION\n" +
            "AND TDEV.TIPO_PROCESO = 'PROCESAR_DEVOLUCION'\n" +
            "LEFT JOIN POS_ARS.TPOS_USER_DATA@PROD.HOMECENTER.COM.CO DT\n" +
            "ON (DT.TYPE = '5520150909' AND DT.STORE = NP.ID_ALMACEN_PAGO AND NP.TERMINAL_PAGO = DT.TERMINAL AND NP" +
            ".TRANSACCION = DT.TRANSNUM AND DT.DAY = NP.FECHA_PAGO)\n" +
            "WHERE TRUNC(Q.CREATIONDATE) BETWEEN TO_DATE(?, 'DD/MM/YY') AND TO_DATE(?, 'DD/MM/YY') \n" +
            "ORDER BY Q.CREATIONDATE DESC)";

    private static final String CONSULTA_EFECTIVIDAD_CONTADOR_FILTROS = "SELECT COUNT(*) AS TOTAL FROM (SELECT " +
            "DISTINCT Q.STOREID AS CODIGO_TIENDA,\n" +
            "Q.CREATIONDATE AS FECHA_CREACION,\n" +
            "TIE.HOMOLOGACION_RPA AS DESCRIPCION_TIENDA,\n" +
            "Q.NUMBER_ NUMERO_COTIZACION,\n" +
            "N.NUMBER_ AS NUMERO_NEGOCIACION,\n" +
            "O.STICKERCODE AS STICKER_NOTA_PEDIDO,\n" +
            "E.NOMBRE AS ESTADO_NP,\n" +
            "DECODE(DET.NUMEROOC, 0, OCPR.ID_ORDEN_COMPRA, DET.NUMEROOC) AS OC,\n" +
            "TRIM(EOC.PMG_STAT_NAME) AS ESTADO_OC,\n" +
            "TDEV.FECHA_INICIO AS FECHA_DEVOLUCION,\n" +
            "DDEV.NUMERO_DEVOLUCION AS NUMERO_DEVOLUCION,\n" +
            "O.NUMBER_ CONSECUTIVO_NP,\n" +
            "DT.DATA1 FACTURA,\n" +
            "Q.ZONEID AS ZONAID,\n" +
            "Q.CREATEDBY AS IDEJECUTIVO,\n" +
            "Q.CUSTOMERCODE AS CLIENTESUBCLIENTE\n" +
            "FROM   FIDELIZACION2.QUOTATION Q\n" +
            "LEFT   JOIN FIDELIZACION2.NEGOTIATION N\n" +
            "ON     N.QUOTATIONID = Q.ID\n" +
            "LEFT   JOIN UNIPROD.SODIMAC_C1_TIENDAS_SFC@PROD.HOMECENTER.COM.CO TIE\n" +
            "ON     TIE.CODIGO_TIENDA = Q.STOREID\n" +
            "LEFT   JOIN FIDELIZACION2.ORDER_ O\n" +
            "ON     O.QUOTATIONID = Q.ID\n" +
            "LEFT   JOIN FIDELIZACION2.COSTO CO\n" +
            "ON     CO.IDCOTIZACION = Q. ID\n" +
            "AND CO.STICKERNP = SUBSTR(O.STICKERCODE, 1, 12)\n" +
            "LEFT   JOIN FIDELIZACION2.DETALLECOSTO DET\n" +
            "ON     CO.ID = DET.IDCOSTO\n" +
            "LEFT   JOIN SAPS.NPEDIDO NP\n" +
            "ON     NP.STICKER = SUBSTR(O.STICKERCODE, 1, 12)\n" +
            "AND NP.ID_ALMACEN = O.STOREID\n" +
            "LEFT   JOIN SAPS.ESTADO E\n" +
            "ON     E.ID_ESTADO = NP.ID_ESTADO\n" +
            "LEFT   JOIN SAPS.ORDEN_COMPRA OCPR\n" +
            "ON     (OCPR.STICKER = NP.STICKER AND OCPR.ID_ALMACEN = NP.ID_ALMACEN)\n" +
            "LEFT   JOIN UNIPROD.PMGHDREE@PROD.HOMECENTER.COM.CO POC\n" +
            "ON     ((POC.PMG_PO_NUMBER = DET.NUMEROOC) OR (POC.PMG_PO_NUMBER = OCPR.ID_ORDEN_COMPRA))\n" +
            "LEFT   JOIN UNIPROD.PMGSTSCD@PROD.HOMECENTER.COM.CO EOC\n" +
            "ON     EOC.PMG_STAT_CODE = POC.PMG_STAT_CODE\n" +
            "LEFT   JOIN DEVOLUCIONES.DETALLE_TIRILLA DTR\n" +
            "ON     DTR.STICKER_NP = NP.STICKER\n" +
            "LEFT   JOIN DEVOLUCIONES.DETALLE_DEVOLUCION DDEV\n" +
            "ON     DDEV.NUMERO_FISCAL = DTR.NUMERO_FISCAL\n" +
            "AND DTR.PRD_LVL_NUMBER = DET.IDPRODUCTO\n" +
            "LEFT   JOIN DEVOLUCIONES.TIEMPO_DEVOLUCION TDEV\n" +
            "ON     TDEV.NUMERO_DEVOLUCION = DDEV.NUMERO_DEVOLUCION\n" +
            "AND TDEV.TIPO_PROCESO = 'PROCESAR_DEVOLUCION'\n" +
            "LEFT JOIN POS_ARS.TPOS_USER_DATA@PROD.HOMECENTER.COM.CO DT\n" +
            "ON (DT.TYPE = '5520150909' AND DT.STORE = NP.ID_ALMACEN_PAGO AND NP.TERMINAL_PAGO = DT.TERMINAL AND NP\n" +
            ".TRANSACCION = DT.TRANSNUM AND DT.DAY = NP.FECHA_PAGO)\n" +
            "WHERE TRUNC(Q.CREATIONDATE) BETWEEN TO_DATE(?, 'DD/MM/YY') AND TO_DATE(?, 'DD/MM/YY') AND ";

    private static final String CONSULTA_EFECTIVIDAD = "SELECT * FROM (SELECT CLIENTESUBCLIENTE, IDEJECUTIVO, ZONAID," +
            "CODIGO_TIENDA, " +
            "FECHA_CREACION, " +
            "DESCRIPCION_TIENDA, NUMERO_COTIZACION, NUMERO_NEGOCIACION, STICKER_NOTA_PEDIDO, ESTADO_NP, OC, " +
            "ESTADO_OC, FECHA_DEVOLUCION, NUMERO_DEVOLUCION, CONSECUTIVO_NP, FACTURA, ROWNUM RNUM FROM\n" +
            "(SELECT DISTINCT Q.STOREID AS CODIGO_TIENDA,\n" +
            "Q.CREATIONDATE AS FECHA_CREACION,\n" +
            "TIE.HOMOLOGACION_RPA AS DESCRIPCION_TIENDA,\n" +
            "Q.NUMBER_ NUMERO_COTIZACION,\n" +
            "N.NUMBER_ AS NUMERO_NEGOCIACION,\n" +
            "O.STICKERCODE AS STICKER_NOTA_PEDIDO,\n" +
            "E.NOMBRE AS ESTADO_NP,\n" +
            "DECODE(DET.NUMEROOC, 0, OCPR.ID_ORDEN_COMPRA, DET.NUMEROOC) AS OC,\n" +
            "TRIM(EOC.PMG_STAT_NAME) AS ESTADO_OC,\n" +
            "TDEV.FECHA_INICIO AS FECHA_DEVOLUCION,\n" +
            "DDEV.NUMERO_DEVOLUCION AS NUMERO_DEVOLUCION,\n" +
            "O.NUMBER_ CONSECUTIVO_NP,\n" +
            "DT.DATA1 FACTURA,\n" +
            "Q.ZONEID AS ZONAID,\n" +
            "Q.CREATEDBY AS IDEJECUTIVO,\n" +
            "Q.CUSTOMERCODE AS CLIENTESUBCLIENTE\n" +
            "FROM   FIDELIZACION2.QUOTATION Q\n" +
            "LEFT   JOIN FIDELIZACION2.NEGOTIATION N\n" +
            "ON     N.QUOTATIONID = Q.ID\n" +
            "LEFT   JOIN UNIPROD.SODIMAC_C1_TIENDAS_SFC@PROD.HOMECENTER.COM.CO TIE\n" +
            "ON     TIE.CODIGO_TIENDA = Q.STOREID\n" +
            "LEFT   JOIN FIDELIZACION2.ORDER_ O\n" +
            "ON     O.QUOTATIONID = Q.ID\n" +
            "LEFT   JOIN FIDELIZACION2.COSTO CO\n" +
            "ON     CO.IDCOTIZACION = Q. ID\n" +
            "AND CO.STICKERNP = SUBSTR(O.STICKERCODE, 1, 12)\n" +
            "LEFT   JOIN FIDELIZACION2.DETALLECOSTO DET\n" +
            "ON     CO.ID = DET.IDCOSTO\n" +
            "LEFT   JOIN SAPS.NPEDIDO NP\n" +
            "ON     NP.STICKER = SUBSTR(O.STICKERCODE, 1, 12)\n" +
            "AND NP.ID_ALMACEN = O.STOREID\n" +
            "LEFT   JOIN SAPS.ESTADO E\n" +
            "ON     E.ID_ESTADO = NP.ID_ESTADO\n" +
            "LEFT   JOIN SAPS.ORDEN_COMPRA OCPR\n" +
            "ON     (OCPR.STICKER = NP.STICKER AND OCPR.ID_ALMACEN = NP.ID_ALMACEN)\n" +
            "LEFT   JOIN UNIPROD.PMGHDREE@PROD.HOMECENTER.COM.CO POC\n" +
            "ON     ((POC.PMG_PO_NUMBER = DET.NUMEROOC) OR (POC.PMG_PO_NUMBER = OCPR.ID_ORDEN_COMPRA))\n" +
            "LEFT   JOIN UNIPROD.PMGSTSCD@PROD.HOMECENTER.COM.CO EOC\n" +
            "ON     EOC.PMG_STAT_CODE = POC.PMG_STAT_CODE\n" +
            "LEFT   JOIN DEVOLUCIONES.DETALLE_TIRILLA DTR\n" +
            "ON     DTR.STICKER_NP = NP.STICKER\n" +
            "LEFT   JOIN DEVOLUCIONES.DETALLE_DEVOLUCION DDEV\n" +
            "ON     DDEV.NUMERO_FISCAL = DTR.NUMERO_FISCAL\n" +
            "AND DTR.PRD_LVL_NUMBER = DET.IDPRODUCTO\n" +
            "LEFT   JOIN DEVOLUCIONES.TIEMPO_DEVOLUCION TDEV\n" +
            "ON     TDEV.NUMERO_DEVOLUCION = DDEV.NUMERO_DEVOLUCION\n" +
            "AND TDEV.TIPO_PROCESO = 'PROCESAR_DEVOLUCION'\n" +
            "LEFT JOIN POS_ARS.TPOS_USER_DATA@PROD.HOMECENTER.COM.CO DT\n" +
            "ON (DT.TYPE = '5520150909' AND DT.STORE = NP.ID_ALMACEN_PAGO AND NP.TERMINAL_PAGO = DT.TERMINAL AND NP" +
            ".TRANSACCION = DT.TRANSNUM AND DT.DAY = NP.FECHA_PAGO)\n" +
            "WHERE TRUNC(Q.CREATIONDATE) BETWEEN TO_DATE(?, 'DD/MM/YY') AND TO_DATE(?, 'DD/MM/YY') " +
            "ORDER BY Q.CREATIONDATE DESC)\n" +
            "WHERE ROWNUM <= ?) WHERE RNUM >= ?";

    private static final String CONSULTA_EFECTIVIDAD_FILTRO_BASE = "SELECT * FROM (SELECT CLIENTESUBCLIENTE, " +
            "IDEJECUTIVO, " +
            "ZONAID, " +
            "CODIGO_TIENDA, " +
            "FECHA_CREACION, DESCRIPCION_TIENDA, NUMERO_COTIZACION, NUMERO_NEGOCIACION, STICKER_NOTA_PEDIDO, " +
            "ESTADO_NP, OC, ESTADO_OC, FECHA_DEVOLUCION, NUMERO_DEVOLUCION, CONSECUTIVO_NP, FACTURA, ROWNUM RNUM " +
            "FROM\n" +
            "(SELECT DISTINCT Q.STOREID AS CODIGO_TIENDA,\n" +
            "Q.CREATIONDATE AS FECHA_CREACION,\n" +
            "TIE.HOMOLOGACION_RPA AS DESCRIPCION_TIENDA,\n" +
            "Q.NUMBER_ NUMERO_COTIZACION,\n" +
            "N.NUMBER_ AS NUMERO_NEGOCIACION,\n" +
            "O.STICKERCODE AS STICKER_NOTA_PEDIDO,\n" +
            "E.NOMBRE AS ESTADO_NP,\n" +
            "DECODE(DET.NUMEROOC, 0, OCPR.ID_ORDEN_COMPRA, DET.NUMEROOC) AS OC,\n" +
            "TRIM(EOC.PMG_STAT_NAME) AS ESTADO_OC,\n" +
            "TDEV.FECHA_INICIO AS FECHA_DEVOLUCION,\n" +
            "DDEV.NUMERO_DEVOLUCION AS NUMERO_DEVOLUCION,\n" +
            "O.NUMBER_ CONSECUTIVO_NP,\n" +
            "DT.DATA1 FACTURA,\n" +
            "Q.ZONEID AS ZONAID,\n" +
            "Q.CREATEDBY AS IDEJECUTIVO,\n" +
            "Q.CUSTOMERCODE AS CLIENTESUBCLIENTE\n" +
            "FROM   FIDELIZACION2.QUOTATION Q\n" +
            "LEFT   JOIN FIDELIZACION2.NEGOTIATION N\n" +
            "ON     N.QUOTATIONID = Q.ID\n" +
            "LEFT   JOIN UNIPROD.SODIMAC_C1_TIENDAS_SFC@PROD.HOMECENTER.COM.CO TIE\n" +
            "ON     TIE.CODIGO_TIENDA = Q.STOREID\n" +
            "LEFT   JOIN FIDELIZACION2.ORDER_ O\n" +
            "ON     O.QUOTATIONID = Q.ID\n" +
            "LEFT   JOIN FIDELIZACION2.COSTO CO\n" +
            "ON     CO.IDCOTIZACION = Q. ID\n" +
            "AND CO.STICKERNP = SUBSTR(O.STICKERCODE, 1, 12)\n" +
            "LEFT   JOIN FIDELIZACION2.DETALLECOSTO DET\n" +
            "ON     CO.ID = DET.IDCOSTO\n" +
            "LEFT   JOIN SAPS.NPEDIDO NP\n" +
            "ON     NP.STICKER = SUBSTR(O.STICKERCODE, 1, 12)\n" +
            "AND NP.ID_ALMACEN = O.STOREID\n" +
            "LEFT   JOIN SAPS.ESTADO E\n" +
            "ON     E.ID_ESTADO = NP.ID_ESTADO\n" +
            "LEFT   JOIN SAPS.ORDEN_COMPRA OCPR\n" +
            "ON     (OCPR.STICKER = NP.STICKER AND OCPR.ID_ALMACEN = NP.ID_ALMACEN)\n" +
            "LEFT   JOIN UNIPROD.PMGHDREE@PROD.HOMECENTER.COM.CO POC\n" +
            "ON     ((POC.PMG_PO_NUMBER = DET.NUMEROOC) OR (POC.PMG_PO_NUMBER = OCPR.ID_ORDEN_COMPRA))\n" +
            "LEFT   JOIN UNIPROD.PMGSTSCD@PROD.HOMECENTER.COM.CO EOC\n" +
            "ON     EOC.PMG_STAT_CODE = POC.PMG_STAT_CODE\n" +
            "LEFT   JOIN DEVOLUCIONES.DETALLE_TIRILLA DTR\n" +
            "ON     DTR.STICKER_NP = NP.STICKER\n" +
            "LEFT   JOIN DEVOLUCIONES.DETALLE_DEVOLUCION DDEV\n" +
            "ON     DDEV.NUMERO_FISCAL = DTR.NUMERO_FISCAL\n" +
            "AND DTR.PRD_LVL_NUMBER = DET.IDPRODUCTO\n" +
            "LEFT   JOIN DEVOLUCIONES.TIEMPO_DEVOLUCION TDEV\n" +
            "ON     TDEV.NUMERO_DEVOLUCION = DDEV.NUMERO_DEVOLUCION\n" +
            "AND TDEV.TIPO_PROCESO = 'PROCESAR_DEVOLUCION'\n" +
            "LEFT JOIN POS_ARS.TPOS_USER_DATA@PROD.HOMECENTER.COM.CO DT\n" +
            "ON (DT.TYPE = '5520150909' AND DT.STORE = NP.ID_ALMACEN_PAGO AND NP.TERMINAL_PAGO = DT.TERMINAL AND NP" +
            ".TRANSACCION = DT.TRANSNUM AND DT.DAY = NP.FECHA_PAGO)\n" +
            "WHERE TRUNC(Q.CREATIONDATE) BETWEEN TO_DATE(?, 'DD/MM/YY') AND TO_DATE(?, 'DD/MM/YY') AND ";


    @Inject
    @DataSource("uniprod")
    AgroalDataSource uniprodClient;

    @Inject
    @DataSource("saps")
    AgroalDataSource sapsClient;

    @Inject
    @DataSource("fidelizacion")
    AgroalDataSource fidelizacionClient;

    private static final Logger LOG = Logger.getLogger(ReporteEfectividadService.class);

    public ReporteEfectividadMsg getReporteEfectividad(Integer rInicial, Integer rFinal) throws SQLException {

        List<RespuestaEfectividad> listaEfectividad = consultaEfectividad(rInicial, rFinal);
        Integer cantidad = consultaContadorEfectividad();
        LOG.debugf("@getReporteEfectividad Termino reporte de efectividad satisfactoriamente");

        return ReporteEfectividadMsg.builder().
                data(listaEfectividad).
                meta(
                        MetaReporteMsg.builder().
                                cantidadDocumentos(cantidad)
                                .build()).
                build();
    }

    public ReporteEfectividadMsg getReporteEfectividadFiltros(Integer rInicial, Integer rFinal,
                                                              FiltrosEfectividad filtros) throws SQLException {

        List<RespuestaEfectividad> listaEfectividad = consultaEfectividadFiltros(rInicial, rFinal, filtros);
        Integer cantidad = consultaContadorEfectividadFiltros(filtros);
        LOG.debugf("@getReporteEfectividadFiltros Termino reporte de efectividad filtros satisfactoriamente");

        return ReporteEfectividadMsg.builder().
                data(listaEfectividad).
                meta(
                        MetaReporteMsg.builder().
                                cantidadDocumentos(cantidad)
                                .build()).
                build();
    }

    @CacheResult(cacheName = "efectividad-cache")
    public List<RespuestaEfectividad> consultaEfectividad(@CacheKey Integer rInicial, @CacheKey Integer rFinal) throws SQLException {
        LOG.debugf("@consultaEfectividad inicia consultaEfectividad.");
        List<RespuestaEfectividad> respuestaEfectividad = new ArrayList<>();

        try (Connection connection = fidelizacionClient.getConnection();
             PreparedStatement ps = connection.prepareStatement(CONSULTA_EFECTIVIDAD)) {

            Calendar cal = Calendar.getInstance();
            int anio = cal.get(Calendar.YEAR);
            Date fecha = new Date();
            int mes = fecha.getMonth() + 1;

            int anioInicial = anio - 1;

            int diaFinal = cal.get(Calendar.DATE);

            String fechaInicial = diaFinal + "/" + mes + "/" + anioInicial;
            String fechaFinal = diaFinal + "/" + mes + "/" + anio;

            LOG.debugf("@consultaEfectividad fechaInicial: " + fechaInicial);
            LOG.debugf("@consultaEfectividad fechaFinal: " + fechaFinal);

            ps.setString(1, fechaInicial);
            ps.setString(2, fechaFinal);
            ps.setInt(3, rFinal);
            ps.setInt(4, rInicial);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                RespuestaEfectividad miRespuesta =
                        RespuestaEfectividad.builder()
                                .clienteNombre(consultaCliente(rs.getString("CLIENTESUBCLIENTE")))
                                .zona(rs.getString("ZONAID"))
                                .ejecutivoId(rs.getString("IDEJECUTIVO"))
                                .ejecutivoNombre(consultaEjecutivo(rs.getString("IDEJECUTIVO")))
                                .codigoTienda(rs.getString("CODIGO_TIENDA"))
                                .fechaCreacion(rs.getString("FECHA_CREACION"))
                                .descripcionTienda(rs.getString("DESCRIPCION_TIENDA"))
                                .numeroCotizacion(rs.getString("NUMERO_COTIZACION"))
                                .numeroNegociacion(rs.getString("NUMERO_NEGOCIACION"))
                                .stickerNotaPedido(rs.getString("STICKER_NOTA_PEDIDO"))
                                .estadoNotaPedido(rs.getString("ESTADO_NP"))
                                .oc(rs.getString("OC"))
                                .estadoOc(rs.getString("ESTADO_OC"))
                                .fechaDevolucion(rs.getDate("FECHA_DEVOLUCION"))
                                .numeroDevolucion(rs.getString("NUMERO_DEVOLUCION"))
                                .consecutivoNP(rs.getString("CONSECUTIVO_NP"))
                                .factura(rs.getString("FACTURA"))
                                .clienteNitConsecutivo(rs.getString("CLIENTESUBCLIENTE"))
                                .build();
                respuestaEfectividad.add(miRespuesta);
            }
        }
        LOG.debugf("@consultaEfectividad termino consultaEfectividad.");
        return respuestaEfectividad;
    }

    @CacheResult(cacheName = "efectividad-contador-cache")
    public Integer consultaContadorEfectividad() throws SQLException {
        LOG.debugf("@consultaContadorEfectividad inicia consultaContadorEfectividad.");
        Integer total = 0;
        try (Connection connection = fidelizacionClient.getConnection();
             PreparedStatement ps = connection.prepareStatement(CONSULTA_EFECTIVIDAD_CONTADOR)) {
            Calendar cal = Calendar.getInstance();
            int anio = cal.get(Calendar.YEAR);
            Date fecha = new Date();
            int mes = fecha.getMonth() + 1;

            int anioInicial = anio - 1;

            int diaFinal = cal.get(Calendar.DATE);

            String fechaInicial = diaFinal + "/" + mes + "/" + anioInicial;
            String fechaFinal = diaFinal + "/" + mes + "/" + anio;

            LOG.debugf("@consultaContadorEfectividad fechaInicial: " + fechaInicial);
            LOG.debugf("@consultaContadorEfectividad fechaFinal: " + fechaFinal);

            ps.setString(1, fechaInicial);
            ps.setString(2, fechaFinal);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                total = (rs.getInt("TOTAL"));
            }
        }
        LOG.debugf("@consultaContadorEfectividad termino consultaContadorEfectividad.");
        return total;
    }

    @CacheResult(cacheName = "efectividad-cliente")
    public String consultaCliente(@CacheKey String codigoCliente) throws SQLException {
        String clienteRespuesta = "";
        String clientebuscar = "";
        boolean buscarCliente = true;

        if (codigoCliente != null || codigoCliente != "") {
            LOG.debugf("@consultaCliente - OK búsqueda cliente: El ID del cliente es: \n" + codigoCliente);
            clientebuscar = codigoCliente;
        } else {
            buscarCliente = false;
            clienteRespuesta = "";
            LOG.debugf("@consultaCliente - Error búsqueda cliente: El ID del cliente es: \n" + codigoCliente);
        }

        try (Connection connection = uniprodClient.getConnection();
             PreparedStatement ps = connection.prepareStatement(CONSULTA_NOMBRE_CLIENTE)) {
            ps.setString(1, clientebuscar);
            ResultSet rs = ps.executeQuery();
            if (rs.next() && buscarCliente) {
                clienteRespuesta = rs.getString("NOMBRE");
            }
        }
        LOG.debugf("@consultaCliente - El nombre del cliente es: " + clienteRespuesta);
        return clienteRespuesta;
    }

    @CacheResult(cacheName = "efectividad-ejecutivo")
    public String  consultaEjecutivo(@CacheKey String codigoEjecutivo) throws SQLException{
        LOG.infof("@consultaEjecutivo, ejecutivo: %s", codigoEjecutivo);
        Integer codigo = 0;
        try {
            codigo = Integer.parseInt(codigoEjecutivo);
        }
        catch (NumberFormatException e)
        {
            return codigoEjecutivo;
        }
        String ejecutivo = "";

        try (Connection connection = sapsClient.getConnection();
             PreparedStatement ps = connection.prepareStatement(CONSULTA_EJECUTIVO_NOMBRE)) {
            ps.setInt(1, codigo);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                ejecutivo = rs.getString("NOMBRES") + " " + rs.getString("APELLIDOS");
            }
        }
        return ejecutivo;
    }

    @CacheResult(cacheName = "efectividad-filtros-cache")
    public List<RespuestaEfectividad> consultaEfectividadFiltros(@CacheKey Integer rInicial, @CacheKey Integer rFinal,
                                                                 @CacheKey FiltrosEfectividad filtros) throws SQLException {
        LOG.debugf("@consultaEfectividadFiltros inicia consultaEfectividadFiltros.");
        List<RespuestaEfectividad> respuestaEfectividad = new ArrayList<>();

        String CONSULTA_FILTRO = CONSULTA_EFECTIVIDAD;

        if (filtros.getTiendas() != null && filtros.getTiendas().size() >= 1 ||
                filtros.getZonas() != null && filtros.getZonas().size() >= 1 ||
                filtros.getCotizaciones() != null && filtros.getCotizaciones().size() >= 1 ||
                filtros.getStickers() != null && filtros.getStickers().size() >= 1 ||
                filtros.getNegociaciones() != null && filtros.getNegociaciones().size() >= 1 ||
                filtros.getEstadosNP() != null && filtros.getEstadosNP().size() >= 1 ||
                filtros.getOc() != null && filtros.getOc().size() >= 1 ||
                filtros.getEstadosOC() != null && filtros.getEstadosOC().size() >= 1 ||
                filtros.getNumDevoluciones() != null && filtros.getNumDevoluciones().size() >= 1 ||
                filtros.getConsecutivosNP() != null && filtros.getConsecutivosNP().size() >= 1 ||
                filtros.getFacturas() != null && filtros.getFacturas().size() >= 1 ||
                filtros.getEjecutivos() != null && filtros.getEjecutivos().size() >= 1 ||
                filtros.getClientes() != null && filtros.getClientes().size() >= 1) {
            CONSULTA_FILTRO = "";
            String filtroTiendas = null;
            String filtroZonas = null;
            String filtroCotizaciones = null;
            String filtroStickers = null;
            String filtroNegociaciones = null;
            String filtroEstadosNP = null;
            String filtroOC = null;
            String filtroEstadosOC = null;
            String filtroDevoluciones = null;
            String filtroConsecutivosNP = null;
            String filtroFacturas = null;
            String filtroEjecutivos = null;
            String filtrosClientes = null;

            if (filtros.getTiendas() != null && filtros.getTiendas().size() >= 1) {
                String cadenaTiendas = "";
                for (Integer tienda : filtros.getTiendas()) {
                    LOG.debugf("@consultaEfectividadFiltros - La tiendaId es: " + tienda);
                    cadenaTiendas += tienda.toString() + ",";
                    LOG.debugf("@consultaEfectividadFiltros - La cadenaTiendas es: " + cadenaTiendas);
                }
                String tiendas =
                        "(" + cadenaTiendas.subSequence(0, Math.max(0, cadenaTiendas.length() - 1)).toString() + ")";
                LOG.debugf("@consultaEfectividadFiltros - Final cadenaTiendas es: " + tiendas);
                filtroTiendas = "Q.STOREID IN " + tiendas;
                CONSULTA_FILTRO += CONSULTA_EFECTIVIDAD_FILTRO_BASE + filtroTiendas;
            }
            if (filtros.getZonas() != null && filtros.getZonas().size() >= 1) {
                String cadenaZonas = "";
                for (Integer zona : filtros.getZonas()) {
                    LOG.debugf("@consultaEfectividadFiltros - La zona es: " + zona);
                    cadenaZonas += zona.toString() + ",";
                    LOG.debugf("@consultaEfectividadFiltros - La cadenaTiendas es: " + cadenaZonas);
                }
                String zonas = "(" + cadenaZonas.subSequence(0, Math.max(0, cadenaZonas.length() - 1)).toString() + ")";
                LOG.debugf("@consultaEfectividadFiltros - Final cadenaZonas es: " + zonas);
                filtroZonas = "Q.ZONEID IN " + zonas;
                if (filtroTiendas != null) {
                    CONSULTA_FILTRO = CONSULTA_FILTRO + " AND " + filtroZonas;
                } else {
                    CONSULTA_FILTRO += CONSULTA_EFECTIVIDAD_FILTRO_BASE + filtroZonas;
                }
            }
            if (filtros.getCotizaciones() != null && filtros.getCotizaciones().size() >= 1) {
                String cadenaCotizaciones = "";
                for (Integer cotizacion : filtros.getCotizaciones()) {
                    LOG.debugf("@consultaEfectividadFiltros - La cotizacion es: " + cotizacion);
                    cadenaCotizaciones += cotizacion.toString() + ",";
                    LOG.debugf("@consultaEfectividadFiltros - La cadenaCotizaciones es: " + cadenaCotizaciones);
                }
                String cotizaciones = "(" + cadenaCotizaciones.subSequence(0, Math.max(0,
                        cadenaCotizaciones.length() - 1)).toString() + ")";
                LOG.debugf("@consultaEfectividadFiltros - Final cadenaCotizaciones es: " + cotizaciones);
                filtroCotizaciones = "Q.NUMBER_ IN " + cotizaciones;
                if (filtroTiendas != null || filtroZonas != null) {
                    CONSULTA_FILTRO = CONSULTA_FILTRO + " AND " + filtroCotizaciones;
                } else {
                    CONSULTA_FILTRO += CONSULTA_EFECTIVIDAD_FILTRO_BASE + filtroCotizaciones;
                }
            }
            if (filtros.getStickers() != null && filtros.getStickers().size() >= 1) {
                String cadenaStikers = "";
                for (String sticker : filtros.getStickers()) {
                    LOG.debugf("@consultaEfectividadFiltros - El sticker es: " + sticker);
                    cadenaStikers += "'" + sticker.toString() + "'" + ",";
                    LOG.debugf("@consultaEfectividadFiltros - La cadenaStickers es: " + cadenaStikers);
                }
                String stickers =
                        "(" + cadenaStikers.subSequence(0, Math.max(0, cadenaStikers.length() - 1)).toString() + ")";
                LOG.debugf("@consultaEfectividadFiltros - Final cadenaStickers es: " + stickers);
                filtroStickers = "O.STICKERCODE IN " + stickers;
                if (filtroTiendas != null || filtroZonas != null || filtroCotizaciones != null) {
                    CONSULTA_FILTRO = CONSULTA_FILTRO + " AND " + filtroStickers;
                } else {
                    CONSULTA_FILTRO += CONSULTA_EFECTIVIDAD_FILTRO_BASE + filtroStickers;
                }
            }
            if (filtros.getNegociaciones() != null && filtros.getNegociaciones().size() >= 1) {
                String cadenaNeg = "";
                for (String neg : filtros.getNegociaciones()) {
                    LOG.debugf("@consultaEfectividadFiltros - La negociación es: " + neg);
                    cadenaNeg += "'" + neg.toString() + "'" + ",";
                    LOG.debugf("@consultaEfectividadFiltros - La cadenaNegociación es: " + cadenaNeg);
                }
                String negociaciones =
                        "(" + cadenaNeg.subSequence(0, Math.max(0, cadenaNeg.length() - 1)).toString() + ")";
                LOG.debugf("@consultaEfectividadFiltros - Final cadenaNegociaciones es: " + negociaciones);
                filtroNegociaciones = "N.NUMBER_ IN " + negociaciones;
                if (filtroTiendas != null || filtroZonas != null || filtroCotizaciones != null || filtroStickers != null) {
                    CONSULTA_FILTRO = CONSULTA_FILTRO + " AND " + filtroNegociaciones;
                } else {
                    CONSULTA_FILTRO += CONSULTA_EFECTIVIDAD_FILTRO_BASE + filtroNegociaciones;
                }
            }
            if (filtros.getEstadosNP() != null && filtros.getEstadosNP().size() >= 1) {
                String cadenaEstadosNP = "";
                for (String estadoNP : filtros.getEstadosNP()) {
                    LOG.debugf("@consultaEfectividadFiltros - El estado es: " + estadoNP);
                    cadenaEstadosNP += "'" + estadoNP.toString() + "'" + ",";
                    LOG.debugf("@consultaEfectividadFiltros - La cadenaEstadosNp es: " + cadenaEstadosNP);
                }
                String estadosNPresult =
                        "(" + cadenaEstadosNP.subSequence(0, Math.max(0, cadenaEstadosNP.length() - 1)).toString() +
                                ")";
                LOG.debugf("@consultaEfectividadFiltros - Final cadenaNegociaciones es: " + estadosNPresult);
                filtroEstadosNP = "E.NOMBRE IN " + estadosNPresult;
                if (filtroTiendas != null || filtroZonas != null || filtroCotizaciones != null || filtroStickers != null || filtroNegociaciones != null) {
                    CONSULTA_FILTRO = CONSULTA_FILTRO + " AND " + filtroEstadosNP;
                } else {
                    CONSULTA_FILTRO += CONSULTA_EFECTIVIDAD_FILTRO_BASE + filtroEstadosNP;
                }
            }
            if (filtros.getOc() != null && filtros.getOc().size() >= 1) {
                String cadenaOC = "";
                for (String oc : filtros.getOc()) {
                    LOG.debugf("@consultaEfectividadFiltros - El oc es: " + oc);
                    cadenaOC += "'" + oc.toString() + "'" + ",";
                    LOG.debugf("@consultaEfectividadFiltros - La cadenaOC es: " + cadenaOC);
                }
                String OCresult =
                        "(" + cadenaOC.subSequence(0, Math.max(0, cadenaOC.length() - 1)).toString() + ")";
                LOG.debugf("@consultaEfectividadFiltros - Final cadenaOC es: " + OCresult);
                filtroOC = "DECODE(DET.NUMEROOC, 0, OCPR.ID_ORDEN_COMPRA, DET.NUMEROOC) IN " + OCresult;
                if (filtroTiendas != null || filtroZonas != null || filtroCotizaciones != null || filtroStickers != null || filtroNegociaciones != null || filtroEstadosNP != null) {
                    CONSULTA_FILTRO = CONSULTA_FILTRO + " AND " + filtroOC;
                } else {
                    CONSULTA_FILTRO += CONSULTA_EFECTIVIDAD_FILTRO_BASE + filtroOC;
                }
            }
            if (filtros.getEstadosOC() != null && filtros.getEstadosOC().size() >= 1) {
                String cadenaEstadosOC = "";
                for (String estadoOC : filtros.getEstadosOC()) {
                    LOG.debugf("@consultaEfectividadFiltros - El estado oc es: " + estadoOC);
                    cadenaEstadosOC += "'" + estadoOC.toString() + "'" + ",";
                    LOG.debugf("@consultaEfectividadFiltros - La cadenaEstadosOC es: " + cadenaEstadosOC);
                }
                String estadosOCresult =
                        "(" + cadenaEstadosOC.subSequence(0, Math.max(0, cadenaEstadosOC.length() - 1)).toString() +
                                ")";
                LOG.debugf("@consultaEfectividadFiltros - Final cadenaEstadosOC es: " + estadosOCresult);
                filtroEstadosOC = " TRIM(EOC.PMG_STAT_NAME) IN " + estadosOCresult;
                if (filtroTiendas != null || filtroZonas != null || filtroCotizaciones != null || filtroStickers != null || filtroNegociaciones != null || filtroEstadosNP != null || filtroOC != null) {
                    CONSULTA_FILTRO = CONSULTA_FILTRO + " AND " + filtroEstadosOC;
                } else {
                    CONSULTA_FILTRO += CONSULTA_EFECTIVIDAD_FILTRO_BASE + filtroEstadosOC;
                }
            }
            if (filtros.getNumDevoluciones() != null && filtros.getNumDevoluciones().size() >= 1) {
                String cadenaNumDevoluciones = "";
                for (String devolucion : filtros.getNumDevoluciones()) {
                    LOG.debugf("@consultaEfectividadFiltros - La devolución es: " + devolucion);
                    cadenaNumDevoluciones += "'" + devolucion.toString() + "'" + ",";
                    LOG.debugf("@consultaEfectividadFiltros - La cadena devolución es: " + cadenaNumDevoluciones);
                }
                String devolucionesNumResult =
                        "(" + cadenaNumDevoluciones.subSequence(0, Math.max(0, cadenaNumDevoluciones.length() - 1)).toString() + ")";
                LOG.debugf("@consultaEfectividadFiltros - Final cadena Devoluciones es: " + devolucionesNumResult);
                filtroDevoluciones = "DDEV.NUMERO_DEVOLUCION IN " + devolucionesNumResult;
                if (filtroTiendas != null || filtroZonas != null || filtroCotizaciones != null || filtroStickers != null || filtroNegociaciones != null || filtroEstadosNP != null || filtroOC != null || filtroEstadosOC != null) {
                    CONSULTA_FILTRO = CONSULTA_FILTRO + " AND " + filtroDevoluciones;
                } else {
                    CONSULTA_FILTRO += CONSULTA_EFECTIVIDAD_FILTRO_BASE + filtroDevoluciones;
                }
            }
            if (filtros.getConsecutivosNP() != null && filtros.getConsecutivosNP().size() >= 1) {
                String cadenaConsecutivosNP = "";
                for (String conNp : filtros.getConsecutivosNP()) {
                    LOG.debugf("@consultaEfectividadFiltros - El consecutivo np es: " + conNp);
                    cadenaConsecutivosNP += "'" + conNp.toString() + "'" + ",";
                    LOG.debugf("@consultaEfectividadFiltros - La cadena consecutivos NP es: " + cadenaConsecutivosNP);
                }
                String consecutivosNPResult =
                        "(" + cadenaConsecutivosNP.subSequence(0, Math.max(0, cadenaConsecutivosNP.length() - 1)).toString() + ")";
                LOG.debugf("@consultaEfectividadFiltros - Final cadena consecutivos NP es: " + consecutivosNPResult);
                filtroConsecutivosNP = "O.NUMBER_ IN " + consecutivosNPResult;
                if (filtroTiendas != null || filtroZonas != null || filtroCotizaciones != null || filtroStickers != null || filtroNegociaciones != null || filtroEstadosNP != null || filtroOC != null || filtroEstadosOC != null) {
                    CONSULTA_FILTRO = CONSULTA_FILTRO + " AND " + filtroConsecutivosNP;
                } else {
                    CONSULTA_FILTRO += CONSULTA_EFECTIVIDAD_FILTRO_BASE + filtroConsecutivosNP;
                }
            }
            if (filtros.getFacturas() != null && filtros.getFacturas().size() >= 1) {
                String cadenaFacturas = "";
                for (String fac : filtros.getFacturas()) {
                    LOG.debugf("@consultaEfectividadFiltros - La factura es: " + fac);
                    cadenaFacturas += "'" + fac.toString() + "'" + ",";
                    LOG.debugf("@consultaEfectividadFiltros - La cadena factura es: " + cadenaFacturas);
                }
                String consecutivosFacturasResult =
                        "(" + cadenaFacturas.subSequence(0, Math.max(0, cadenaFacturas.length() - 1)).toString() + ")";
                LOG.debugf("@consultaEfectividadFiltros - Final cadena facturas es: " + consecutivosFacturasResult);
                filtroFacturas = "DT.DATA1 IN " + consecutivosFacturasResult;
                if (filtroTiendas != null || filtroZonas != null || filtroCotizaciones != null || filtroStickers != null || filtroNegociaciones != null || filtroEstadosNP != null || filtroOC != null || filtroEstadosOC != null || filtroConsecutivosNP != null) {
                    CONSULTA_FILTRO = CONSULTA_FILTRO + " AND " + filtroFacturas;
                } else {
                    CONSULTA_FILTRO += CONSULTA_EFECTIVIDAD_FILTRO_BASE + filtroFacturas;
                }
            }
            if (filtros.getFacturas() != null && filtros.getFacturas().size() >= 1) {
                String cadenaFacturas = "";
                for (String fac : filtros.getFacturas()) {
                    LOG.debugf("@consultaEfectividadFiltros - La factura es: " + fac);
                    cadenaFacturas += "'" + fac.toString() + "'" + ",";
                    LOG.debugf("@consultaEfectividadFiltros - La cadena factura es: " + cadenaFacturas);
                }
                String consecutivosFacturasResult =
                        "(" + cadenaFacturas.subSequence(0, Math.max(0, cadenaFacturas.length() - 1)).toString() + ")";
                LOG.debugf("@consultaEfectividadFiltros - Final cadena facturas es: " + consecutivosFacturasResult);
                filtroFacturas = "DT.DATA1 IN " + consecutivosFacturasResult;
                if (filtroTiendas != null || filtroZonas != null || filtroCotizaciones != null || filtroStickers != null || filtroNegociaciones != null || filtroEstadosNP != null || filtroOC != null || filtroEstadosOC != null || filtroConsecutivosNP != null) {
                    CONSULTA_FILTRO = CONSULTA_FILTRO + " AND " + filtroFacturas;
                } else {
                    CONSULTA_FILTRO += CONSULTA_EFECTIVIDAD_FILTRO_BASE + filtroFacturas;
                }
            }
            if (filtros.getEjecutivos() != null && filtros.getEjecutivos().size() >= 1) {
                String cadenaEjecutivos = "";
                for (String eje : filtros.getEjecutivos()) {
                    LOG.debugf("@consultaEfectividadFiltros - El ejecutivo es: " + eje);
                    cadenaEjecutivos += "'" + eje.toString() + "'" + ",";
                    LOG.debugf("@consultaEfectividadFiltros - La cadena ejecutivos es: " + cadenaEjecutivos);
                }
                String consecutivosEjecutivosResult =
                        "(" + cadenaEjecutivos.subSequence(0, Math.max(0, cadenaEjecutivos.length() - 1)).toString() + ")";
                LOG.debugf("@consultaEfectividadFiltros - Final cadena ejecutivos es: " + consecutivosEjecutivosResult);
                filtroEjecutivos = "Q.CREATEDBY IN " + consecutivosEjecutivosResult;
                if (filtroTiendas != null || filtroZonas != null || filtroCotizaciones != null || filtroStickers != null || filtroNegociaciones != null || filtroEstadosNP != null || filtroOC != null || filtroEstadosOC != null || filtroConsecutivosNP != null || filtroFacturas != null) {
                    CONSULTA_FILTRO = CONSULTA_FILTRO + " AND " + filtroEjecutivos;
                } else {
                    CONSULTA_FILTRO += CONSULTA_EFECTIVIDAD_FILTRO_BASE + filtroEjecutivos;
                }
            }
            if (filtros.getClientes() != null && filtros.getClientes().size() >= 1) {
                String cadenaClientes = "";
                for (String cliente : filtros.getClientes()) {
                    LOG.debugf("@consultaEfectividadFiltros - El cliente es: " + cliente);
                    cadenaClientes += "'" + cliente.toString() + "'" + ",";
                    LOG.debugf("@consultaEfectividadFiltros - La cadena clientes es: " + cadenaClientes);
                }
                String consecutivosClientesResult =
                        "(" + cadenaClientes.subSequence(0, Math.max(0, cadenaClientes.length() - 1)).toString() + ")";
                LOG.debugf("@consultaEfectividadFiltros - Final cadena clientes es: " + consecutivosClientesResult);
                filtrosClientes = "Q.CUSTOMERCODE IN " + consecutivosClientesResult;
                if (filtroTiendas != null || filtroZonas != null || filtroCotizaciones != null || filtroStickers != null || filtroNegociaciones != null || filtroEstadosNP != null || filtroOC != null || filtroEstadosOC != null || filtroConsecutivosNP != null || filtroFacturas != null || filtroEjecutivos != null) {
                    CONSULTA_FILTRO = CONSULTA_FILTRO + " AND " + filtrosClientes;
                } else {
                    CONSULTA_FILTRO += CONSULTA_EFECTIVIDAD_FILTRO_BASE + filtrosClientes;
                }
            }
            CONSULTA_FILTRO += " ORDER BY Q.CREATIONDATE DESC)" +
                    "WHERE ROWNUM <= ?) WHERE RNUM >= ?";
        }else {
            LOG.debugf("@consultaEfectividadFiltros - NO se entro por algun filtro, quiere decir que tiene " +
                    "por" +
                    " " +
                    "lo menos un filtro.");
        }


        LOG.debugf("@consultaEfectividadFiltros - LA CONSULTA SQL CON FILTROS ES: " + CONSULTA_FILTRO);

        try (Connection connection = fidelizacionClient.getConnection();
             PreparedStatement ps = connection.prepareStatement(CONSULTA_FILTRO)) {

            Calendar cal = Calendar.getInstance();
            int anio = cal.get(Calendar.YEAR);
            Date fecha = new Date();
            int mes = fecha.getMonth() + 1;

            int anioInicial = anio - 1;

            int diaFinal = cal.get(Calendar.DATE);

            String fechaInicial = diaFinal + "/" + mes + "/" + anioInicial;
            String fechaFinal = diaFinal + "/" + mes + "/" + anio;

            if(filtros.getFechaCreacion()!=null){
                ps.setString(1, filtros.getFechaCreacion().getFechaInicial());
                ps.setString(2, filtros.getFechaCreacion().getFechaFinal());
                LOG.debugf("@consultaEfectividadFiltros - Se realizá consulta con filtro de fecha de creación " +
                        "personalizado.");
                LOG.debugf("@consultaEfectividadFiltros - fechaInicial: " + filtros.getFechaCreacion().getFechaInicial());
                LOG.debugf("@consultaEfectividadFiltros - fechaFinal: " + filtros.getFechaCreacion().getFechaFinal());
            }else{
                ps.setString(1, fechaInicial);
                ps.setString(2, fechaFinal);
                LOG.debugf("@consultaEfectividadFiltros - NO Se realizá consulta con filtro de fecha de creación " +
                        "personalizado.");
                LOG.debugf("@consultaEfectividadFiltros - fechaInicial: " + fechaInicial);
                LOG.debugf("@consultaEfectividadFiltros - fechaFinal: " + fechaFinal);
            }
            ps.setInt(3, rFinal);
            ps.setInt(4, rInicial);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                RespuestaEfectividad miRespuesta =
                        RespuestaEfectividad.builder()
                                .ejecutivoId(rs.getString("IDEJECUTIVO"))
                                .ejecutivoNombre(consultaEjecutivo(rs.getString("IDEJECUTIVO")))
                                .clienteNitConsecutivo(rs.getString("CLIENTESUBCLIENTE"))
                                .zona(rs.getString("ZONAID"))
                                .codigoTienda(rs.getString("CODIGO_TIENDA"))
                                .fechaCreacion(rs.getString("FECHA_CREACION"))
                                .descripcionTienda(rs.getString("DESCRIPCION_TIENDA"))
                                .numeroCotizacion(rs.getString("NUMERO_COTIZACION"))
                                .numeroNegociacion(rs.getString("NUMERO_NEGOCIACION"))
                                .stickerNotaPedido(rs.getString("STICKER_NOTA_PEDIDO"))
                                .estadoNotaPedido(rs.getString("ESTADO_NP"))
                                .oc(rs.getString("OC"))
                                .estadoOc(rs.getString("ESTADO_OC"))
                                .fechaDevolucion(rs.getDate("FECHA_DEVOLUCION"))
                                .numeroDevolucion(rs.getString("NUMERO_DEVOLUCION"))
                                .consecutivoNP(rs.getString("CONSECUTIVO_NP"))
                                .factura(rs.getString("FACTURA"))
                                .clienteNombre(consultaCliente(rs.getString("CLIENTESUBCLIENTE")))
                                .build();
                respuestaEfectividad.add(miRespuesta);
            }
        }
        LOG.debugf("@consultaEfectividadFiltros -  termino consultaEfectividadFiltros.");
        return respuestaEfectividad;
    }

    @CacheResult(cacheName = "efectividad-filtros-contador-cache")
    public Integer consultaContadorEfectividadFiltros(@CacheKey FiltrosEfectividad filtros) throws SQLException {
        LOG.debugf("@consultaContadorEfectividadFiltros inicia consultaContadorEfectividadFiltros.");

        String CONSULTA_CONTADOR_FIL = CONSULTA_EFECTIVIDAD_CONTADOR;

        if (filtros.getTiendas() != null && filtros.getTiendas().size() >= 1 ||
                filtros.getZonas() != null && filtros.getZonas().size() >= 1 ||
                filtros.getCotizaciones() != null && filtros.getCotizaciones().size() >= 1 ||
                filtros.getStickers() != null && filtros.getStickers().size() >= 1 ||
                filtros.getNegociaciones() != null && filtros.getNegociaciones().size() >= 1 ||
                filtros.getEstadosNP() != null && filtros.getEstadosNP().size() >= 1 ||
                filtros.getOc() != null && filtros.getOc().size() >= 1 ||
                filtros.getEstadosOC() != null && filtros.getEstadosOC().size() >= 1 ||
                filtros.getNumDevoluciones() != null && filtros.getNumDevoluciones().size() >= 1 ||
                filtros.getConsecutivosNP() != null && filtros.getConsecutivosNP().size() >= 1 ||
                filtros.getFacturas() != null && filtros.getFacturas().size() >= 1 ||
                filtros.getEjecutivos() != null && filtros.getEjecutivos().size() >= 1 ||
                filtros.getClientes() != null && filtros.getClientes().size() >= 1) {
            LOG.debugf("@consultaContadorEfectividadFiltros - Se entro por algun filtro, quiere decir que tiene por " +
                    "lo menos un filtro.");
            CONSULTA_CONTADOR_FIL = "";
            String filtroTiendas = null;
            String filtroZonas = null;
            String filtroCotizaciones = null;
            String filtroStickers = null;
            String filtroNegociaciones = null;
            String filtroEstadosNP = null;
            String filtroOC = null;
            String filtroEstadosOC = null;
            String filtroDevoluciones = null;
            String filtroConsecutivosNP = null;
            String filtroFacturas = null;
            String filtroEjecutivos = null;
            String filtrosClientes = null;

            if (filtros.getTiendas() != null && filtros.getTiendas().size() >= 1) {
                String cadenaTiendas = "";
                for (Integer tienda : filtros.getTiendas()) {
                    LOG.debugf("@consultaContadorEfectividadFiltros - La tiendaId es: " + tienda);
                    cadenaTiendas += tienda.toString() + ",";
                    LOG.debugf("@consultaContadorEfectividadFiltros - La cadenaTiendas es: " + cadenaTiendas);
                }
                String tiendas =
                        "(" + cadenaTiendas.subSequence(0, Math.max(0, cadenaTiendas.length() - 1)).toString() + ")";
                LOG.debugf("@consultaContadorEfectividadFiltros - Final cadenaTiendas es: " + tiendas);
                filtroTiendas = "Q.STOREID IN " + tiendas;
                CONSULTA_CONTADOR_FIL += CONSULTA_EFECTIVIDAD_CONTADOR_FILTROS + filtroTiendas;
            }
            if (filtros.getZonas() != null && filtros.getZonas().size() >= 1) {
                String cadenaZonas = "";
                for (Integer zona : filtros.getZonas()) {
                    LOG.debugf("@consultaContadorEfectividadFiltros - La zona es: " + zona);
                    cadenaZonas += zona.toString() + ",";
                    LOG.debugf("@consultaContadorEfectividadFiltros - La cadenaTiendas es: " + cadenaZonas);
                }
                String zonas = "(" + cadenaZonas.subSequence(0, Math.max(0, cadenaZonas.length() - 1)).toString() + ")";
                LOG.debugf("@consultaContadorEfectividadFiltros - Final cadenaZonas es: " + zonas);
                filtroZonas = "Q.ZONEID IN " + zonas;
                if (filtroTiendas != null) {
                    CONSULTA_CONTADOR_FIL = CONSULTA_CONTADOR_FIL + " AND " + filtroZonas;
                } else {
                    CONSULTA_CONTADOR_FIL += CONSULTA_EFECTIVIDAD_CONTADOR_FILTROS + filtroZonas;
                }
            }
            if (filtros.getCotizaciones() != null && filtros.getCotizaciones().size() >= 1) {
                String cadenaCotizaciones = "";
                for (Integer cotizacion : filtros.getCotizaciones()) {
                    LOG.debugf("@consultaContadorEfectividadFiltros - La cotizacion es: " + cotizacion);
                    cadenaCotizaciones += cotizacion.toString() + ",";
                    LOG.debugf("@consultaContadorEfectividadFiltros - La cadenaCotizaciones es: " + cadenaCotizaciones);
                }
                String cotizaciones = "(" + cadenaCotizaciones.subSequence(0, Math.max(0,
                        cadenaCotizaciones.length() - 1)).toString() + ")";
                LOG.debugf("@consultaContadorEfectividadFiltros - Final cadenaCotizaciones es: " + cotizaciones);
                filtroCotizaciones = "Q.NUMBER_ IN " + cotizaciones;
                if (filtroTiendas != null || filtroZonas != null) {
                    CONSULTA_CONTADOR_FIL = CONSULTA_CONTADOR_FIL + " AND " + filtroCotizaciones;
                } else {
                    CONSULTA_CONTADOR_FIL += CONSULTA_EFECTIVIDAD_CONTADOR_FILTROS + filtroCotizaciones;
                }
            }
            if (filtros.getStickers() != null && filtros.getStickers().size() >= 1) {
                String cadenaStikers = "";
                for (String sticker : filtros.getStickers()) {
                    LOG.debugf("@consultaContadorEfectividadFiltros - El sticker es: " + sticker);
                    cadenaStikers += "'" + sticker.toString() + "'" + ",";
                    LOG.debugf("@consultaContadorEfectividadFiltros - La cadenaStickers es: " + cadenaStikers);
                }
                String stickers =
                        "(" + cadenaStikers.subSequence(0, Math.max(0, cadenaStikers.length() - 1)).toString() + ")";
                LOG.debugf("@consultaContadorEfectividadFiltros - Final cadenaStickers es: " + stickers);
                filtroStickers = "O.STICKERCODE IN " + stickers;
                if (filtroTiendas != null || filtroZonas != null || filtroCotizaciones != null) {
                    CONSULTA_CONTADOR_FIL = CONSULTA_CONTADOR_FIL + " AND " + filtroStickers;
                } else {
                    CONSULTA_CONTADOR_FIL += CONSULTA_EFECTIVIDAD_CONTADOR_FILTROS + filtroStickers;
                }
            }
            if (filtros.getNegociaciones() != null && filtros.getNegociaciones().size() >= 1) {
                String cadenaNeg = "";
                for (String neg : filtros.getNegociaciones()) {
                    LOG.debugf("@consultaContadorEfectividadFiltros - La negociación es: " + neg);
                    cadenaNeg += "'" + neg.toString() + "'" + ",";
                    LOG.debugf("@consultaContadorEfectividadFiltros - La cadenaNegociación es: " + cadenaNeg);
                }
                String negociaciones =
                        "(" + cadenaNeg.subSequence(0, Math.max(0, cadenaNeg.length() - 1)).toString() + ")";
                LOG.debugf("@consultaContadorEfectividadFiltros - Final cadenaNegociaciones es: " + negociaciones);
                filtroNegociaciones = "N.NUMBER_ IN " + negociaciones;
                if (filtroTiendas != null || filtroZonas != null || filtroCotizaciones != null || filtroStickers != null) {
                    CONSULTA_CONTADOR_FIL = CONSULTA_CONTADOR_FIL + " AND " + filtroNegociaciones;
                } else {
                    CONSULTA_CONTADOR_FIL += CONSULTA_EFECTIVIDAD_CONTADOR_FILTROS + filtroNegociaciones;
                }
            }
            if (filtros.getEstadosNP() != null && filtros.getEstadosNP().size() >= 1) {
                String cadenaEstadosNP = "";
                for (String estadoNP : filtros.getEstadosNP()) {
                    LOG.debugf("@consultaContadorEfectividadFiltros - El estado es: " + estadoNP);
                    cadenaEstadosNP += "'" + estadoNP.toString() + "'" + ",";
                    LOG.debugf("@consultaContadorEfectividadFiltros - La cadenaEstadosNp es: " + cadenaEstadosNP);
                }
                String estadosNPresult =
                        "(" + cadenaEstadosNP.subSequence(0, Math.max(0, cadenaEstadosNP.length() - 1)).toString() +
                                ")";
                LOG.debugf("@consultaContadorEfectividadFiltros - Final cadenaNegociaciones es: " + estadosNPresult);
                filtroEstadosNP = "E.NOMBRE IN " + estadosNPresult;
                if (filtroTiendas != null || filtroZonas != null || filtroCotizaciones != null || filtroStickers != null || filtroNegociaciones != null) {
                    CONSULTA_CONTADOR_FIL = CONSULTA_CONTADOR_FIL + " AND " + filtroEstadosNP;
                } else {
                    CONSULTA_CONTADOR_FIL += CONSULTA_EFECTIVIDAD_CONTADOR_FILTROS + filtroEstadosNP;
                }
            }
            if (filtros.getOc() != null && filtros.getOc().size() >= 1) {
                String cadenaOC = "";
                for (String oc : filtros.getOc()) {
                    LOG.debugf("@consultaContadorEfectividadFiltros - El oc es: " + oc);
                    cadenaOC += "'" + oc.toString() + "'" + ",";
                    LOG.debugf("@consultaContadorEfectividadFiltros - La cadenaOC es: " + cadenaOC);
                }
                String OCresult =
                        "(" + cadenaOC.subSequence(0, Math.max(0, cadenaOC.length() - 1)).toString() + ")";
                LOG.debugf("@consultaContadorEfectividadFiltros - Final cadenaOC es: " + OCresult);
                filtroOC = "DECODE(DET.NUMEROOC, 0, OCPR.ID_ORDEN_COMPRA, DET.NUMEROOC) IN " + OCresult;
                if (filtroTiendas != null || filtroZonas != null || filtroCotizaciones != null || filtroStickers != null || filtroNegociaciones != null || filtroEstadosNP != null) {
                    CONSULTA_CONTADOR_FIL = CONSULTA_CONTADOR_FIL + " AND " + filtroOC;
                } else {
                    CONSULTA_CONTADOR_FIL += CONSULTA_EFECTIVIDAD_CONTADOR_FILTROS + filtroOC;
                }
            }
            if (filtros.getEstadosOC() != null && filtros.getEstadosOC().size() >= 1) {
                String cadenaEstadosOC = "";
                for (String estadoOC : filtros.getEstadosOC()) {
                    LOG.debugf("@consultaContadorEfectividadFiltros - El estado oc es: " + estadoOC);
                    cadenaEstadosOC += "'" + estadoOC.toString() + "'" + ",";
                    LOG.debugf("@consultaContadorEfectividadFiltros - La cadenaEstadosOC es: " + cadenaEstadosOC);
                }
                String estadosOCresult =
                        "(" + cadenaEstadosOC.subSequence(0, Math.max(0, cadenaEstadosOC.length() - 1)).toString() +
                                ")";
                LOG.debugf("@consultaContadorEfectividadFiltros - Final cadenaEstadosOC es: " + estadosOCresult);
                filtroEstadosOC = " TRIM(EOC.PMG_STAT_NAME) IN " + estadosOCresult;
                if (filtroTiendas != null || filtroZonas != null || filtroCotizaciones != null || filtroStickers != null || filtroNegociaciones != null || filtroEstadosNP != null || filtroOC != null) {
                    CONSULTA_CONTADOR_FIL = CONSULTA_CONTADOR_FIL + " AND " + filtroEstadosOC;
                } else {
                    CONSULTA_CONTADOR_FIL += CONSULTA_EFECTIVIDAD_CONTADOR_FILTROS + filtroEstadosOC;
                }
            }
            if (filtros.getNumDevoluciones() != null && filtros.getNumDevoluciones().size() >= 1) {
                String cadenaNumDevoluciones = "";
                for (String devolucion : filtros.getNumDevoluciones()) {
                    LOG.debugf("@consultaContadorEfectividadFiltros - La devolución es: " + devolucion);
                    cadenaNumDevoluciones += "'" + devolucion.toString() + "'" + ",";
                    LOG.debugf("@consultaContadorEfectividadFiltros - La cadena devolución es: " + cadenaNumDevoluciones);
                }
                String devolucionesNumResult =
                        "(" + cadenaNumDevoluciones.subSequence(0, Math.max(0, cadenaNumDevoluciones.length() - 1)).toString() + ")";
                LOG.debugf("@consultaContadorEfectividadFiltros - Final cadena Devoluciones es: " + devolucionesNumResult);
                filtroDevoluciones = "DDEV.NUMERO_DEVOLUCION IN " + devolucionesNumResult;
                if (filtroTiendas != null || filtroZonas != null || filtroCotizaciones != null || filtroStickers != null || filtroNegociaciones != null || filtroEstadosNP != null || filtroOC != null || filtroEstadosOC != null) {
                    CONSULTA_CONTADOR_FIL = CONSULTA_CONTADOR_FIL + " AND " + filtroDevoluciones;
                } else {
                    CONSULTA_CONTADOR_FIL += CONSULTA_EFECTIVIDAD_CONTADOR_FILTROS + filtroDevoluciones;
                }
            }
            if (filtros.getConsecutivosNP() != null && filtros.getConsecutivosNP().size() >= 1) {
                String cadenaConsecutivosNP = "";
                for (String conNp : filtros.getConsecutivosNP()) {
                    LOG.debugf("@consultaContadorEfectividadFiltros - El consecutivo np es: " + conNp);
                    cadenaConsecutivosNP += "'" + conNp.toString() + "'" + ",";
                    LOG.debugf("@consultaContadorEfectividadFiltros - La cadena consecutivos NP es: " + cadenaConsecutivosNP);
                }
                String consecutivosNPResult =
                        "(" + cadenaConsecutivosNP.subSequence(0, Math.max(0, cadenaConsecutivosNP.length() - 1)).toString() + ")";
                LOG.debugf("@consultaContadorEfectividadFiltros - Final cadena consecutivos NP es: " + consecutivosNPResult);
                filtroConsecutivosNP = "O.NUMBER_ IN " + consecutivosNPResult;
                if (filtroTiendas != null || filtroZonas != null || filtroCotizaciones != null || filtroStickers != null || filtroNegociaciones != null || filtroEstadosNP != null || filtroOC != null || filtroEstadosOC != null) {
                    CONSULTA_CONTADOR_FIL = CONSULTA_CONTADOR_FIL + " AND " + filtroConsecutivosNP;
                } else {
                    CONSULTA_CONTADOR_FIL += CONSULTA_EFECTIVIDAD_CONTADOR_FILTROS + filtroConsecutivosNP;
                }
            }
            if (filtros.getFacturas() != null && filtros.getFacturas().size() >= 1) {
                String cadenaFacturas = "";
                for (String fac : filtros.getFacturas()) {
                    LOG.debugf("@consultaContadorEfectividadFiltros - La factura es: " + fac);
                    cadenaFacturas += "'" + fac.toString() + "'" + ",";
                    LOG.debugf("@consultaContadorEfectividadFiltros - La cadena factura es: " + cadenaFacturas);
                }
                String consecutivosFacturasResult =
                        "(" + cadenaFacturas.subSequence(0, Math.max(0, cadenaFacturas.length() - 1)).toString() + ")";
                LOG.debugf("@consultaContadorEfectividadFiltros - Final cadena facturas es: " + consecutivosFacturasResult);
                filtroFacturas = "DT.DATA1 IN " + consecutivosFacturasResult;
                if (filtroTiendas != null || filtroZonas != null || filtroCotizaciones != null || filtroStickers != null || filtroNegociaciones != null || filtroEstadosNP != null || filtroOC != null || filtroEstadosOC != null || filtroConsecutivosNP != null) {
                    CONSULTA_CONTADOR_FIL = CONSULTA_CONTADOR_FIL + " AND " + filtroFacturas;
                } else {
                    CONSULTA_CONTADOR_FIL += CONSULTA_EFECTIVIDAD_CONTADOR_FILTROS + filtroFacturas;
                }
            }
            if (filtros.getFacturas() != null && filtros.getFacturas().size() >= 1) {
                String cadenaFacturas = "";
                for (String fac : filtros.getFacturas()) {
                    LOG.debugf("@consultaContadorEfectividadFiltros - La factura es: " + fac);
                    cadenaFacturas += "'" + fac.toString() + "'" + ",";
                    LOG.debugf("@consultaContadorEfectividadFiltros - La cadena factura es: " + cadenaFacturas);
                }
                String consecutivosFacturasResult =
                        "(" + cadenaFacturas.subSequence(0, Math.max(0, cadenaFacturas.length() - 1)).toString() + ")";
                LOG.debugf("@consultaContadorEfectividadFiltros - Final cadena facturas es: " + consecutivosFacturasResult);
                filtroFacturas = "DT.DATA1 IN " + consecutivosFacturasResult;
                if (filtroTiendas != null || filtroZonas != null || filtroCotizaciones != null || filtroStickers != null || filtroNegociaciones != null || filtroEstadosNP != null || filtroOC != null || filtroEstadosOC != null || filtroConsecutivosNP != null) {
                    CONSULTA_CONTADOR_FIL = CONSULTA_CONTADOR_FIL + " AND " + filtroFacturas;
                } else {
                    CONSULTA_CONTADOR_FIL += CONSULTA_EFECTIVIDAD_CONTADOR_FILTROS + filtroFacturas;
                }
            }
            if (filtros.getEjecutivos() != null && filtros.getEjecutivos().size() >= 1) {
                String cadenaEjecutivos = "";
                for (String eje : filtros.getEjecutivos()) {
                    LOG.debugf("@consultaContadorEfectividadFiltros - El ejecutivo es: " + eje);
                    cadenaEjecutivos += "'" + eje.toString() + "'" + ",";
                    LOG.debugf("@consultaContadorEfectividadFiltros - La cadena ejecutivos es: " + cadenaEjecutivos);
                }
                String consecutivosEjecutivosResult =
                        "(" + cadenaEjecutivos.subSequence(0, Math.max(0, cadenaEjecutivos.length() - 1)).toString() + ")";
                LOG.debugf("@consultaContadorEfectividadFiltros - Final cadena ejecutivos es: " + consecutivosEjecutivosResult);
                filtroEjecutivos = "Q.CREATEDBY IN " + consecutivosEjecutivosResult;
                if (filtroTiendas != null || filtroZonas != null || filtroCotizaciones != null || filtroStickers != null || filtroNegociaciones != null || filtroEstadosNP != null || filtroOC != null || filtroEstadosOC != null || filtroConsecutivosNP != null || filtroFacturas != null) {
                    CONSULTA_CONTADOR_FIL = CONSULTA_CONTADOR_FIL + " AND " + filtroEjecutivos;
                } else {
                    CONSULTA_CONTADOR_FIL += CONSULTA_EFECTIVIDAD_CONTADOR_FILTROS + filtroEjecutivos;
                }
            }
            if (filtros.getClientes() != null && filtros.getClientes().size() >= 1) {
                String cadenaClientes = "";
                for (String cliente : filtros.getClientes()) {
                    LOG.debugf("@consultaEfectividadFiltros - El cliente es: " + cliente);
                    cadenaClientes += "'" + cliente.toString() + "'" + ",";
                    LOG.debugf("@consultaEfectividadFiltros - La cadena clientes es: " + cadenaClientes);
                }
                String consecutivosClientesResult =
                        "(" + cadenaClientes.subSequence(0, Math.max(0, cadenaClientes.length() - 1)).toString() + ")";
                LOG.debugf("@consultaEfectividadFiltros - Final cadena clientes es: " + consecutivosClientesResult);
                filtrosClientes = "Q.CUSTOMERCODE IN " + consecutivosClientesResult;
                if (filtroTiendas != null || filtroZonas != null || filtroCotizaciones != null || filtroStickers != null || filtroNegociaciones != null || filtroEstadosNP != null || filtroOC != null || filtroEstadosOC != null || filtroConsecutivosNP != null || filtroFacturas != null || filtroEjecutivos != null) {
                    CONSULTA_CONTADOR_FIL = CONSULTA_CONTADOR_FIL + " AND " + filtrosClientes;
                } else {
                    CONSULTA_CONTADOR_FIL += CONSULTA_EFECTIVIDAD_CONTADOR_FILTROS + filtrosClientes;
                }
            }
            CONSULTA_CONTADOR_FIL += ")";
        } else {
            LOG.debugf("@consultaContadorEfectividadFiltros - NO se entro por algun filtro, quiere decir que tiene " +
                    "por" +
                    " " +
                    "lo menos un filtro.");
        }

        LOG.debugf("@consultaContadorEfectividadFiltros - LA CONSULTA SQL CONTADOR CON FILTROS ES: " + CONSULTA_CONTADOR_FIL+"\n");
        Integer total = 0;
        try (Connection connection = fidelizacionClient.getConnection();
             PreparedStatement ps = connection.prepareStatement(CONSULTA_CONTADOR_FIL)) {

            Calendar cal = Calendar.getInstance();
            int anio = cal.get(Calendar.YEAR);
            Date fecha = new Date();
            int mes = fecha.getMonth() + 1;

            int anioInicial = anio - 1;

            int diaFinal = cal.get(Calendar.DATE);

            String fechaInicial = diaFinal + "/" + mes + "/" + anioInicial;
            String fechaFinal = diaFinal + "/" + mes + "/" + anio;

            LOG.debugf("@consultaContadorEfectividadFiltros - fechaInicial: " + fechaInicial);
            LOG.debugf("@consultaContadorEfectividadFiltros - fechaFinal: " + fechaFinal);

            if(filtros.getFechaCreacion()!=null){
                ps.setString(1, filtros.getFechaCreacion().getFechaInicial());
                ps.setString(2, filtros.getFechaCreacion().getFechaFinal());
                LOG.debugf("@consultaEfectividadFiltros - Se realizá consulta con filtro de fecha de creación " +
                        "personalizado.");
                LOG.debugf("@consultaEfectividadFiltros - fechaInicial: " + filtros.getFechaCreacion().getFechaInicial());
                LOG.debugf("@consultaEfectividadFiltros - fechaFinal: " + filtros.getFechaCreacion().getFechaFinal());
            }else{
                ps.setString(1, fechaInicial);
                ps.setString(2, fechaFinal);
                LOG.debugf("@consultaEfectividadFiltros  - NO Se realizá consulta con filtro de fecha de creación " +
                        "personalizado.");
                LOG.debugf("@consultaEfectividadFiltros - fechaInicial: " + fechaInicial);
                LOG.debugf("@consultaEfectividadFiltros - fechaFinal: " + fechaFinal);
            }
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                total = (rs.getInt("TOTAL"));
            }
            LOG.debugf("@consultaContadorEfectividadFiltros termino consultaContadorEfectividadFiltros.");
            return total;
        }
    }
}
