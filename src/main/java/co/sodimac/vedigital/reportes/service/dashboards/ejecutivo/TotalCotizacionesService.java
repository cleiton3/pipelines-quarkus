package co.sodimac.vedigital.reportes.service.dashboards.ejecutivo;

import co.sodimac.vedigital.reportes.domain.domainsDasboards.MetaDashboardMsg;
import co.sodimac.vedigital.reportes.domain.domainsDasboards.ejecutivo.Filtros.FiltroTotalCotizaciones;
import co.sodimac.vedigital.reportes.domain.domainsDasboards.ejecutivo.TotalCotizaionesMsg;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;

import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.ne;

@ApplicationScoped
public class TotalCotizacionesService {

    private static final Logger LOG = Logger.getLogger(TotalCotizacionesService.class);

    @Inject
    MongoClient mongoClient;

    private MongoCollection getCollectionDashboardTotalCotizaciones() {
        LOG.infof("@getCollectionDashboardTotalCotizaciones, Conexion a la base de datos DashEjecTotalCotizaciones");
        return mongoClient.getDatabase("datosLinea").getCollection("DashEjecTotalCotizaciones");
    }

    public void actualizacionVistas(FiltroTotalCotizaciones filtro){
        getCollectionDashboardTotalCotizaciones().drop();
        mongoClient.getDatabase("datosLinea").createView("DashEjecTotalCotizaciones","CotizacionMsg", Arrays.asList(
                new Document("$group",
                        new Document("_id", new Document("ejecutivo", "$meta.dataauthor").
                                append("avance", "$contexto.avance")).
                                append("cantidad", new Document("$sum", 1))
                ),
                match(ne("_id.avance", null)),
                match(ne("_id.avance", "")),
                match(eq("_id.ejecutivo", filtro.getEjecutivo()))
        ));
    }


    public TotalCotizaionesMsg getTotalCotizacionesService(FiltroTotalCotizaciones filtro) {
        LOG.infof("@getTotalCotizacionesService, Consulta a MONGO, para obtener DashEjecTotalCotizaciones");

        // Aqui crea la vista
        actualizacionVistas(filtro);

        var collection = getCollectionDashboardTotalCotizaciones();
        AggregateIterable<Document> output = collection.aggregate(Arrays.asList());

        var lista = output.into(new ArrayList<>());

        long cantidadDocumentos = collection.countDocuments();
        LOG.infof("Lista respuesta: %s", lista);
        LOG.infof("Cantidad lista respuesta: %s", cantidadDocumentos);

        return TotalCotizaionesMsg.builder().
                data(lista).
                meta(
                        MetaDashboardMsg.builder().
                                cantidadDocumentos(cantidadDocumentos)
                                .build()).
                build();
    }
}
