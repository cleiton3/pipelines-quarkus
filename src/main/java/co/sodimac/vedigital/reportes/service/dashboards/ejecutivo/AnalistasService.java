package co.sodimac.vedigital.reportes.service.dashboards.ejecutivo;

import co.sodimac.vedigital.reportes.domain.domainsDasboards.MetaDashboardMsg;
import co.sodimac.vedigital.reportes.domain.domainsDasboards.ejecutivo.ResDashboardsMsg;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.mongodb.client.model.Aggregates.count;
import static com.mongodb.client.model.Aggregates.sort;
import static com.mongodb.client.model.Sorts.descending;
import static com.mongodb.client.model.Sorts.orderBy;

@ApplicationScoped
public class AnalistasService {

    private static final Logger LOG = Logger.getLogger(AnalistasService.class);
    @Inject
    MongoClient mongoClient;

    private MongoCollection getCollectionAnalistas() {
        LOG.infof("@getCollectionAnalistas, Conexion a la base de datos DashAnalistas");
        return mongoClient.getDatabase("datosLinea").getCollection("DashAnalistas");
    }

    public void actualizacionVistas(){
        getCollectionAnalistas().drop();
        mongoClient.getDatabase("datosLinea").createView("DashAnalistas","LineaMsg", Arrays.asList(
                new Document("$group",
                        new Document("_id", new Document("idAnalistas", "$data.aprobaciones.revisionesAnalista.idAnalista").
                                append("analistas", "$data.aprobaciones.revisionesAnalista.analista"))
                )
        ));
    }


    public ResDashboardsMsg getAnalistas() {
        LOG.infof("@getAnalistas, Consulta a MONGO, para obtener Analistas");

        actualizacionVistas();

        var collection = getCollectionAnalistas();

        String columna = "analista";

        Bson direction = descending(columna);

        List<Bson> stepsAggregate = new ArrayList<>();

        stepsAggregate.add(count());
        AggregateIterable<Document> countAggregateIterable = collection.aggregate(stepsAggregate);

        var countList = countAggregateIterable.into(new ArrayList<>());
        var cantidadDocumentos = 0;
        if (countList != null && !countList.isEmpty()) {
            cantidadDocumentos = (Integer) countList.get(0).get("count");
        }

        stepsAggregate.remove(stepsAggregate.size() - 1);
        stepsAggregate.add(sort(orderBy(direction)));
        AggregateIterable<Document> output = collection.aggregate(stepsAggregate);

        var listaTop = output.into(new ArrayList<>());
        LOG.infof("Lista respuesta: %s", listaTop);
        LOG.infof("Cantidad lista respuesta: %s", cantidadDocumentos);

        return ResDashboardsMsg.builder().
                data(listaTop).
                meta(
                        MetaDashboardMsg.builder().
                                cantidadDocumentos(cantidadDocumentos)
                                .build()).
                build();
    }


}
