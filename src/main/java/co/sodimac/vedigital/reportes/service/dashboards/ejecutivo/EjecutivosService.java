package co.sodimac.vedigital.reportes.service.dashboards.ejecutivo;

import co.sodimac.vedigital.reportes.domain.domainsDasboards.MetaDashboardMsg;
import co.sodimac.vedigital.reportes.domain.domainsDasboards.ejecutivo.ResDashboardsMsg;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.in;
import static com.mongodb.client.model.Sorts.descending;
import static com.mongodb.client.model.Sorts.orderBy;

@ApplicationScoped
public class EjecutivosService {

    private static final Logger LOG = Logger.getLogger(EjecutivosService.class);
    @Inject
    MongoClient mongoClient;

    private MongoCollection getCollectionEjecutivos() {
        LOG.infof("@getCollectionEjecutivos, Conexion a la base de datos DashEjecutivos");
        return mongoClient.getDatabase("datosLinea").getCollection("DashEjecutivos");
    }

    public void actualizacionVistas(){
        getCollectionEjecutivos().drop();
        mongoClient.getDatabase("datosLinea").createView("DashEjecutivos","CotizacionMsg", Arrays.asList(
                new Document("$group",
                        new Document("_id", new Document("ejecutivo", "$meta.dataauthor"))
                )
        ));
    }


    public ResDashboardsMsg getEjecutivos() {
        LOG.infof("@getEjecutivos, Consulta a MONGO, para obtener Ejecutivos");

        actualizacionVistas();

        var collection = getCollectionEjecutivos();

        String columna = "ejecutivo";

        Bson direction = descending(columna);

        List<Bson> stepsAggregate = new ArrayList<>();

        stepsAggregate.add(count());
        AggregateIterable<Document> countAggregateIterable = collection.aggregate(stepsAggregate);

        var countList = countAggregateIterable.into(new ArrayList<>());
        var cantidadDocumentos = 0;
        if (countList != null && !countList.isEmpty()) {
            cantidadDocumentos = (Integer) countList.get(0).get("count");
        }

        stepsAggregate.remove(stepsAggregate.size() - 1);
        stepsAggregate.add(sort(orderBy(direction)));
        AggregateIterable<Document> output = collection.aggregate(stepsAggregate);

        var listaTop = output.into(new ArrayList<>());
        LOG.infof("Lista respuesta: %s", listaTop);
        LOG.infof("Cantidad lista respuesta: %s", cantidadDocumentos);

        return ResDashboardsMsg.builder().
                data(listaTop).
                meta(
                        MetaDashboardMsg.builder().
                                cantidadDocumentos(cantidadDocumentos)
                                .build()).
                build();
    }


}
