package co.sodimac.vedigital.reportes.service.dashboards.ejecutivo;

import co.sodimac.vedigital.reportes.domain.domainsDasboards.MetaDashboardMsg;
import co.sodimac.vedigital.reportes.domain.domainsDasboards.ejecutivo.EstadosCotizacionesMsg;
import co.sodimac.vedigital.reportes.domain.domainsDasboards.ejecutivo.Filtros.FiltroEstadosCotizaciones;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.jboss.logging.Logger;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.ne;

@ApplicationScoped
public class EstadosCotizacionesService {
    private static final Logger LOG = Logger.getLogger(EstadosCotizacionesService.class);

    @Inject
    MongoClient mongoClient;

    private MongoCollection getCollectionDashboardEstadosCotizaciones() {
        LOG.infof("@getCollectionDashboardEstadosCotizaciones, Conexion a la base de datos DashEjecEstadCotizaciones");
        return mongoClient.getDatabase("datosLinea").getCollection("DashEjecEstadCotizaciones");
    }

    public void actualizacionVistas(FiltroEstadosCotizaciones filtro) {
        getCollectionDashboardEstadosCotizaciones().drop();
        int anio = 0;
        int mes = 0;

        if (filtro.isFechaActual()) {
            Calendar cal = Calendar.getInstance();
            anio = cal.get(Calendar.YEAR);
            Date fecha = new Date();
            mes = fecha.getMonth() + 1;
        } else {
            anio = filtro.getAnioSeleccionado();
            mes = filtro.getMesSeleccionado();
        }

        LOG.infof("El año a filtrar es: " + anio);
        LOG.infof("El mes a filtrar es: " + mes);

        mongoClient.getDatabase("datosLinea").createView("DashEjecEstadCotizaciones", "CotizacionMsg", Arrays.asList(
                new Document("$group",
                        new Document("_id", new Document("ejecutivo", "$meta.dataauthor").
                                append("avance", "$contexto.avance").
                                append("anioActual", new Document("$year", "$meta.time")).
                                append("mesActual", new Document("$month", "$meta.time"))).
                                append("cantidad", new Document("$sum", 1))
                ),
                match(eq("_id.anioActual", anio)),
                match(eq("_id.mesActual", mes)),
                match(ne("_id.avance", null)),
                match(ne("_id.avance", "")),
                match(eq("_id.ejecutivo", filtro.getEjecutivo()))
        ));
    }

    public EstadosCotizacionesMsg getEstadosCotizacionesService(FiltroEstadosCotizaciones filtro) {
        LOG.infof("@getTotalCotizacionesService, Consulta a MONGO, para obtener DashEjecTotalCotizaciones");

        // Aqui crea la vista
        actualizacionVistas(filtro);

        var collection = getCollectionDashboardEstadosCotizaciones();
        AggregateIterable<Document> output = collection.aggregate(Arrays.asList());

        var lista = output.into(new ArrayList<>());

        long cantidadDocumentos = collection.countDocuments();
        LOG.infof("Lista respuesta: %s", lista);
        LOG.infof("Cantidad lista respuesta: %s", cantidadDocumentos);

        return EstadosCotizacionesMsg.builder().
                data(lista).
                meta(
                        MetaDashboardMsg.builder().
                                cantidadDocumentos(cantidadDocumentos)
                                .build()).
                build();
    }
}
